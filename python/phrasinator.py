#!/bin/env python

## @file
## @brief Phrasinator is a Python application that extracts phrases from text, where phrases are sequence of tokens whose POS tags are of the form <tt>(ADJ*)(NN+)</tt>.
## This file implements the \p Phrasinator class and also a script to \a phrasinate files.
## @details
## @verbatim
##usage: phrasinator.py [-h] [-i input [input ...]] [-o output] [-O]
##                      [-V vocabulary_file] [--input-enc encoding]
##                      [--output-enc encoding] [--tokenized] [-p] [-l] [-s sep]
##                      [-t [pos_sep]] [-c [str]]
##
##Run Phrasinator on raw text. Includes sentence splitting (using Splitta),
##tokenizing and POS tagging (using Stanford NLP). GZip files are handled
##automatically.
##
##optional arguments:
##  -h, --help            show this help message and exit
##
##  Input/output options
##
##  -i input [input ...], --input input [input ...]
##                        List of input files/directories to Phrasinate.
##                        (defaults to STDIN). You can read list of filenames
##                        from a file by prepending it with '@'.
##  -o output, --output output
##                        Location to save output. When there is one input file,
##                        output is filename. When there are multiple inputs,
##                        output should be a folder. Default: output is STDOUT
##                        if --input is a single file, '.phrases' will be
##                        appended to the filenames if there are multiple files
##                        on --input.
##  -O, --no-output       Don't generate output files.
##  -V vocabulary_file, --vocab vocabulary_file
##                        Save all extracted phrases into a vocabulary file.
##  --input-enc encoding  Encoding of input text (default: utf-8).
##  --output-enc encoding
##                        Encoding for output text (default: utf-8).
##  --tokenized           Input is already tokenized, i.e each line is a
##                        sentence and each token is separated by spaces.
##
##  Phrasinator options
##
##  -p, --phrases-only    Keep only phrases and ignore the unigrams. Note that
##                        there can still be unigram "phrases"
##  -l, --lemma           Replace tokens with lemma.
##  -s sep, --phrase-separator sep
##                        Separator character between tokens in a phrase
##                        (default: '_').
##  -t [pos_sep], --tags [pos_sep]
##                        Include POS tags with the tokens. You can specify the
##                        POS separator here (defaults: '/' if enabled).
##  -c [str], --comments [str]
##                        Treat lines that start with <str> as comments and just
##                        pass them straight through to the output (default: '#'
##                        if enabled).
## @endverbatim

import argparse
import codecs
import collections
import gzip
import imp
import itertools
import operator
import os
import re
import sys

try:
  from corenlpwrapper import StanfordCoreNLP  # wont work now but some day
except ImportError:
  (_file, _filename, _data) = imp.find_module('corenlpwrapper', [os.path.join(os.path.dirname(os.path.realpath(__file__)), '..', 'lib')])
  ## corenlpwrapper module object
  corenlpwrapper = imp.load_module('corenlpwrapper', _file, _filename, _data)
  from corenlpwrapper import StanfordCoreNLP
#end try


## @brief Implementation of \p Phrasinator.
class Phrasinator(object):

  ## @brief Private regular expression that defines what constitutes a "phrase".
  _re_phrase_tags = re.compile(ur'(?P<phrase>J*N+)')

  ## @brief Mapping of Penn Treebank tags to (single character versions of) Universal POS tags.
  _ptb_tag_map = {  # R -> ADV, J -> ADJ, V -> VERB, N -> NOUN
      'JJ': 'J',
      'JJR': 'J',
      'JJS': 'J',
      'MD': 'V',
      'NN': 'N',
      'NNP': 'N',
      'NNPS': 'N',
      'NNS': 'N',
      'NP': 'N',
      'RB': 'R',
      'RBR': 'R',
      'RBS': 'R',
      'VB': 'V',
      'VBD': 'V',
      'VBG': 'V',
      'VBN': 'V',
      'VBP': 'V',
      'VBZ': 'V',
      'VP': 'V',
      'WRB': 'R'
  }

  ## @brief Constructor.
  ## @param lemma whether to return lemmatized tokens
  ## @param phrase_separator separator character between words in a phrase, i.e hello_world
  ## @param token_separator if set, it means that the text is already tokenized and tokens are separated by it
  ## @param sentence_separator if set, it means sentences are separated by this
  ## @param pos_model the Stanford CoreNLP model to use for POS tagging
  def __init__(self, lemma=False, phrase_separator=u'_', token_separator=None, sentence_separator=None, pos_model=u'english-left3words-distsim.tagger'):
    ## Whether to return lemmatized tokens. This is set in the constructor.
    self._lemma = lemma

    ## separator character between words in a phrase, i.e \a hello_world.  This is set in the constructor.
    self._phrase_separator = phrase_separator

    ## If not \p None, text is assumed to be sentence splitted (using \p sentence_separator) already. This is set in the constructor.
    self._sentence_separator = sentence_separator

    ## Private \p StanfordCoreNLP object instance. This is created during construction.
    self._corenlp = StanfordCoreNLP(ssplit=sentence_separator is None, tokenize=token_separator is None, lemma=lemma, pos=pos_model)
  #end def

  ## @brief Phrasinate a chunk of text.
  ## @details Depending on the settings defined during construction (see \p __init__), this method \a phrasinates a chunk of text and returns a list of list of words/phrases and POS tags.
  ## @note POS tags of phrases share the same \p Phrasinator._phrase_separator character defined during construction.
  ## @param text text to phrasinate
  ## @return a list of sentences, where each sentence is a list of <i>(is_phrase, word or phrase, pos tags)</i> tuples.
  def phrasinate_text(self, text):
    sents = self._corenlp.annotate(text, sentence_separator=self._sentence_separator)

    return map(self._extract_phrases, sents)
  #end def

  ## Extract phrases from a POS tagged string.
  ## @param sent a string returned by \p StanfordCoreNLP with words and tags
  ## @return a list of <i>(is_phrase, word or phrase, pos tags)</i> tuples.
  def _extract_phrases(self, sent):
    if not sent: return []

    wt_pairs = map(lambda word_tag: tuple(word_tag.rsplit(u'/', 1)), sent.split(u' '))
    tag_string = u''.join(map(lambda (word, tag): self._ptb_tag_map.get(tag, '.'), wt_pairs))

    start = 0
    phrase_sent = []
    for m in self._re_phrase_tags.finditer(tag_string):
      phrase_sent += map(lambda (w, t): (False, w, t), wt_pairs[start:m.start()])
      phrase = self._phrase_separator.join(map(operator.itemgetter(0), wt_pairs[m.start():m.end()]))
      phrase_tag = self._phrase_separator.join(map(operator.itemgetter(1), wt_pairs[m.start():m.end()]))

      phrase_sent.append((True, phrase, phrase_tag))
      start = m.end()
    #end for
    phrase_sent += map(lambda (w, t): (False, w, t), wt_pairs[start:])

    return phrase_sent
  #end def
#end class

## @cond


def process_paragraph(phrasinator, output_f, text):
  global A, V_freq
  sents = phrasinator.phrasinate_text(text)

  if A.phrases_only: sents = map(lambda sent: filter(operator.itemgetter(0), sent), sents)

  doc_phrases = set()
  for sent in sents:
    if A.vocab:
      for (is_phrase, word, tag) in sent:
        if not is_phrase: continue
        V_freq[word] += 1
        doc_phrases.add(word)
      #end for
    #end if

    if A.tags: print >>output_f, u' '.join(map(lambda (_, w, t): w + A.tags + t, sent))
    else: print >>output_f, u' '.join(map(operator.itemgetter(1), sent))
  #end for

  return doc_phrases
#end def

if __name__ == '__main__':
  parser = argparse.ArgumentParser(description='Run Phrasinator on raw text. Includes sentence splitting (using Splitta), tokenizing and POS tagging (using Stanford NLP). GZip files are handled automatically.', fromfile_prefix_chars='@')

  group = parser.add_argument_group(description='Input/output options')
  group.add_argument('-i', '--input', type=str, default=[], required=False, nargs='+', metavar='input', help='List of input files/directories to Phrasinate. (defaults to STDIN). You can read list of filenames from a file by prepending it with \'@\'.')
  group.add_argument('-o', '--output', type=str, metavar='output', default=None, help='Location to save output. When there is one input file, output is filename. When there are multiple inputs, output should be a folder. Default: output is STDOUT if --input is a single file, \'.phrases\' will be appended to the filenames if there are multiple files on --input.')
  group.add_argument('-O', '--no-output', action='store_true', help='Don\'t generate output files.')
  group.add_argument('-V', '--vocab', type=argparse.FileType('w'), metavar='vocabulary_file', default=None, help='Save all extracted phrases into a vocabulary file.')
  group.add_argument('--input-enc', type=str, default='utf-8', metavar='encoding', help='Encoding of input text (default: utf-8).')
  group.add_argument('--output-enc', type=str, default='utf-8', metavar='encoding', help='Encoding for output text (default: utf-8).')
  group.add_argument('--tokenized', action='store_true', help='Input is already tokenized, i.e each line is a sentence and each token is separated by spaces.')

  group = parser.add_argument_group(description='Phrasinator options')
  group.add_argument('-p', '--phrases-only', action='store_true', help='Keep only phrases and ignore the unigrams. Note that there can still be unigram \"phrases\"')
  group.add_argument('-l', '--lemma', action='store_true', help='Replace tokens with lemma.')
  group.add_argument('-s', '--phrase-separator', type=str, default='_', metavar='sep', help='Separator character between tokens in a phrase (default: \'_\').')
  group.add_argument('-t', '--tags', type=str, default=None, nargs='?', const='/', metavar='pos_sep', help='Include POS tags with the tokens. You can specify the POS separator here (defaults: \'/\' if enabled).')
  group.add_argument('-c', '--comments', type=str, default=None, const='#', nargs='?', metavar='str', help='Treat lines that start with <str> as comments and just pass them straight through to the output (default: \'#\' if enabled).')
  A = parser.parse_args()

  sys.stdin = codecs.getreader(A.input_enc)(sys.stdin)
  sys.stdout = codecs.getwriter(A.output_enc)(sys.stdout)
  sys.stderr = codecs.getwriter('utf-8')(sys.stderr)

  input_filenames = []
  for arg in A.input:
    if os.path.isfile(arg): input_filenames.append(arg)
    elif os.path.isdir(arg): input_filenames += map(lambda fname: os.path.join(arg, fname), sorted(os.listdir(arg)))
  #end for
  if A.no_output: output_filenames = [None] * len(input_filenames)
  elif A.output is None: output_filenames = [] if len(input_filenames) <= 1 else map(lambda fname: fname + '.phrases', input_filenames)
  elif len(input_filenames) == 1: output_filenames = [A.output]
  elif len(input_filenames) > 1 and os.path.isdir(A.output): output_filenames = map(lambda fname: os.path.join(A.output, os.path.basename(fname)), input_filenames)
  else: parser.error('Invalid input/output arguments.')

  input_files = [sys.stdin] if len(input_filenames) == 0 else itertools.imap(lambda fname: codecs.open(fname, 'r', A.input_enc), input_filenames)
  output_files = [sys.stdout] if len(output_filenames) == 0 else itertools.imap(lambda fname: codecs.open(fname, 'w', A.output_enc) if fname else None, output_filenames)
  if A.vocab:
    V_freq = collections.Counter()
    V_docfreq = collections.Counter()
  #end if

  phrasinator = Phrasinator(lemma=A.lemma, phrase_separator=A.phrase_separator, token_separator=' ' if A.tokenized else None, sentence_separator=u'\n' if A.tokenized else None)

  autogz = lambda f: gzip.GzipFile(fileobj=f) if f.name.endswith('.gz') else f
  for i, (input_f, output_f) in enumerate(itertools.izip(input_files, output_files), start=1):
    cur_paragraph = []
    if len(input_filenames) > 0: print >>sys.stderr, u'Processing {} of {}: {} -> {}'.format(i, len(input_filenames), input_f.name, output_f.name)

    for line in autogz(input_f):
      line = line.strip(' \r\n')
      doc_phrases = set()

      if line and not (A.comments and line.startswith(A.comments)):
        cur_paragraph.append(line)
      else:
        text = u' '.join(cur_paragraph)
        doc_phrases |= process_paragraph(phrasinator, output_f, text)

        if A.comments and line.startswith(A.comments):
          print >>output_f, line
          cur_paragraph = []
        else: cur_paragraph = [line]
      #end if
    #end for
    text = u' '.join(cur_paragraph)
    doc_phrases |= process_paragraph(phrasinator, output_f, text)

    if A.vocab:
      for phrase in doc_phrases: V_docfreq[phrase] += 1
    #end if
  #end for

  if A.vocab:
    for (w, c) in V_freq.most_common(): print >>A.vocab, u'{}\t{}\t{}'.format(w, c, V_docfreq[w]).encode('utf-8')
  #end if
#end if
## @endcond
