/**
 * @file
 * @brief Unit test code for the math header library \p ycutils/math.h.
 * @cond
 */

#include <ctime>

#include <array>
#include <iostream>
#include <iomanip>
#include <vector>

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE FastMathTest
#include <boost/test/unit_test.hpp>

#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_real_distribution.hpp>
#include <boost/random/variate_generator.hpp>
#include <boost/timer/timer.hpp>

#define YCUTILS_GLOBAL_sigmoid_table

#include <ycutils/collections.h>
#include <ycutils/fastmath.h>

using std::vector;

BOOST_AUTO_TEST_SUITE(fastmath_test_suite);

BOOST_AUTO_TEST_CASE(fastexp)
{
  std::cout << "haha";
  using ycutils::fastexp;
  using real_variate_generator = boost::random::variate_generator<boost::random::mt19937, boost::random::uniform_real_distribution<double> >;

  static constexpr auto rel_tolerance = 1e-12;
  static constexpr auto N = 500000u;
  static constexpr auto D = 20u;

  boost::random::mt19937 rng(static_cast<unsigned int>(std::time(0)));
  real_variate_generator unif_real_dist(rng, boost::random::uniform_real_distribution<double>(-20, 20));

  // double arr[N][D], ans[N][D];
  std::array<std::array<double, D>, N> arr, ans;
  std::vector<std::vector<double> > vec(N);

  for (auto i = 0u; i < N; ++i)
  {
    vec[i].resize(D);
    for (auto j = 0u; j < D; ++j)
    {
      ans[i][j] = unif_real_dist();
      vec[i][j] = ans[i][j];
      arr[i][j] = ans[i][j];
    }
  }

  boost::timer::cpu_timer timer_;
  for (auto i = 0u; i < N; ++i)
    for (auto j = 0u; j < D; ++j)
      ans[i][j] = std::exp(ans[i][j]);
  timer_.stop();
  const auto stdexp_elapsed(timer_.elapsed());
  BOOST_TEST_MESSAGE("Exponentials (using std) on " << N << "x" << D << " random real numbers took " << timer_.format(5, "%t") << " seconds.");

  timer_.start();
  for (auto i = 0u; i < N; ++i)
    fastexp(&vec[i][0], D);
  timer_.stop();
  BOOST_TEST_MESSAGE("Exponentials (using fastexp with vector<double> in-place) on " << N << "x" << D << " random real numbers took " << timer_.format(5, "%t") << " seconds (" << static_cast<double>((stdexp_elapsed.system + stdexp_elapsed.user)) / (timer_.elapsed().system + timer_.elapsed().user) << "x).");

  timer_.start();
  for (auto i = 0u; i < N; ++i)
    fastexp(arr[i].data(), D);
  timer_.stop();
  BOOST_TEST_MESSAGE("Exponentials (using fastexp with arrays) on " << N << "x" << D << " random real numbers took " << timer_.format(5, "%t") << " seconds (" << static_cast<double>((stdexp_elapsed.system + stdexp_elapsed.user)) / (timer_.elapsed().system + timer_.elapsed().user) << "x).");

  for (auto i = 0u; i < N; i += 10)
    for (auto j = 0u; j < D; ++j)
    {
      BOOST_REQUIRE_CLOSE(vec[i][j], ans[i][j], rel_tolerance);
      BOOST_REQUIRE_CLOSE(arr[i][j], ans[i][j], rel_tolerance);
    }
}

BOOST_AUTO_TEST_CASE(fastlog)
{
  using ycutils::fastlog;
  using real_variate_generator = boost::random::variate_generator<boost::random::mt19937, boost::random::uniform_real_distribution<double> >;

  static constexpr auto rel_tolerance = 1e-12;
  static constexpr auto N = 500000u;
  static constexpr auto D = 20u;

  boost::random::mt19937 rng(static_cast<unsigned int>(std::time(0)));
  real_variate_generator unif_real_dist(rng, boost::random::uniform_real_distribution<double>(0, 20));

  double arr[N][D], ans[N][D];
  std::vector<std::vector<double> > vec(N);

  for (auto i = 0u; i < N; ++i)
  {
    vec[i].resize(D);
    for (auto j = 0u; j < D; ++j)
    {
      ans[i][j] = unif_real_dist();
      vec[i][j] = ans[i][j];
      arr[i][j] = ans[i][j];
    }
  }

  boost::timer::cpu_timer timer_;
  for (auto i = 0u; i < N; ++i)
    for (auto j = 0u; j < D; ++j)
      ans[i][j] = std::log(ans[i][j]);
  timer_.stop();
  const auto stdlog_elapsed(timer_.elapsed());
  BOOST_TEST_MESSAGE("Logarithms (using std) on " << N << "x" << D << " random real numbers took " << timer_.format(5, "%t") << " seconds.");

  timer_.start();
  for (auto i = 0u; i < N; ++i)
    fastlog(&vec[i][0], D);
  timer_.stop();
  BOOST_TEST_MESSAGE("Logarithms (using fastlog with vector<double> in-place) on " << N << "x" << D << " random real numbers took " << timer_.format(5, "%t") << " seconds (" << static_cast<double>((stdlog_elapsed.system + stdlog_elapsed.user)) / (timer_.elapsed().system + timer_.elapsed().user) << "x).");

  timer_.start();
  for (auto i = 0u; i < N; ++i)
    fastlog(arr[i], D);
  timer_.stop();
  BOOST_TEST_MESSAGE("Logarithms (using fastlog with arrays) on " << N << "x" << D << " random real numbers took " << timer_.format(5, "%t") << " seconds (" << static_cast<double>((stdlog_elapsed.system + stdlog_elapsed.user)) / (timer_.elapsed().system + timer_.elapsed().user) << "x).");

  for (auto i = 0u; i < N; i += 10)
    for (auto j = 0u; j < D; ++j)
    {
      BOOST_REQUIRE_CLOSE(vec[i][j], ans[i][j], rel_tolerance);
      BOOST_REQUIRE_CLOSE(arr[i][j], ans[i][j], rel_tolerance);
    }
}

BOOST_AUTO_TEST_CASE(logsigmoid)
{
  using ycutils::logsigmoid;
  using real_variate_generator = boost::random::variate_generator<boost::random::mt19937, boost::random::uniform_real_distribution<double> >;

  static constexpr auto rel_tolerance = 1e-12;
  static constexpr auto N = 100000u;
  static constexpr auto D = 200u;

  boost::random::mt19937 rng(static_cast<unsigned int>(std::time(0)));
  real_variate_generator unif_real_dist(rng, boost::random::uniform_real_distribution<double>(-20, 20));

  double arr[N][D], ans[N][D];
  std::vector<std::vector<double> > vec(N);

  for (auto i = 0u; i < N; ++i)
  {
    vec[i].resize(D);
    for (auto j = 0u; j < D; ++j)
    {
      ans[i][j] = unif_real_dist();
      vec[i][j] = ans[i][j];
      arr[i][j] = ans[i][j];
    }
  }

  boost::timer::cpu_timer timer_;
  for (auto i = 0u; i < N; ++i)
  {
    double orig_vec[D];
    double A = -std::numeric_limits<double>::max();

    for (auto j = 0u; j < D; ++j)
      if (ans[i][j] > A)
        A = ans[i][j];
    for (auto j = 0u; j < D; ++j)
      orig_vec[j] = std::exp(ans[i][j] - A);

    double exp_sum = 0.0;
    for (auto j = 0u; j < D; ++j)
      exp_sum += orig_vec[j];

    const auto log_Z = std::log(exp_sum);

    for (auto j = 0u; j < D; ++j)
      ans[i][j] -= A + log_Z;
  }
  timer_.stop();
  const auto stdmath_elapsed(timer_.elapsed());
  BOOST_TEST_MESSAGE("Log-Sigmoids (using std) on " << N << "x" << D << " random real numbers took " << timer_.format(5, "%t") << " seconds.");

  timer_.start();
  for (auto i = 0u; i < N; ++i)
    logsigmoid(&vec[i][0], D);
  timer_.stop();
  BOOST_TEST_MESSAGE("Log-Sigmoids (using logsigmoid with vector<double> in-place) on " << N << "x" << D << " random real numbers took " << timer_.format(5, "%t") << " seconds (" << static_cast<double>((stdmath_elapsed.system + stdmath_elapsed.user)) / (timer_.elapsed().system + timer_.elapsed().user) << "x).");

  timer_.start();
  for (auto i = 0u; i < N; ++i)
    logsigmoid(arr[i], D);
  timer_.stop();
  BOOST_TEST_MESSAGE("Log-Sigmoids (using logsigmoid with arrays) on " << N << "x" << D << " random real numbers took " << timer_.format(5, "%t") << " seconds (" << static_cast<double>((stdmath_elapsed.system + stdmath_elapsed.user)) / (timer_.elapsed().system + timer_.elapsed().user) << "x).");

  for (auto i = 0u; i < N; i += 10)
    for (auto j = 0u; j < D; ++j)
    {
      BOOST_REQUIRE_CLOSE(vec[i][j], ans[i][j], rel_tolerance);
      BOOST_REQUIRE_CLOSE(arr[i][j], ans[i][j], rel_tolerance);
    }
}

BOOST_AUTO_TEST_CASE(sigmoid)
{
  using ycutils::sigmoid;
  using real_variate_generator = boost::random::variate_generator<boost::random::mt19937, boost::random::uniform_real_distribution<double> >;

  static constexpr auto rel_tolerance = 1e-12;
  static constexpr auto N = 100000u;
  static constexpr auto D = 200u;

  boost::random::mt19937 rng(static_cast<unsigned int>(std::time(0)));
  real_variate_generator unif_real_dist(rng, boost::random::uniform_real_distribution<double>(-15, 15));

  double arr[N][D], ans[N][D];
  std::vector<std::vector<double> > vec(N);

  for (auto i = 0u; i < N; ++i)
  {
    vec[i].resize(D);
    for (auto j = 0u; j < D; ++j)
    {
      ans[i][j] = unif_real_dist();
      vec[i][j] = ans[i][j];
      arr[i][j] = ans[i][j];
    }
  }

  boost::timer::cpu_timer timer_;
  for (auto i = 0u; i < N; ++i)
  {
    double orig_vec[D];
    double A = -std::numeric_limits<double>::max();

    for (auto j = 0u; j < D; ++j)
      if (ans[i][j] > A)
        A = ans[i][j];
    for (auto j = 0u; j < D; ++j)
      orig_vec[j] = std::exp(ans[i][j] - A);

    double exp_sum = 0.0;
    for (auto j = 0u; j < D; ++j)
      exp_sum += orig_vec[j];

    for (auto j = 0u; j < D; ++j)
      ans[i][j] = orig_vec[j] / exp_sum;
  }
  timer_.stop();
  const auto stdmath_elapsed(timer_.elapsed());
  BOOST_TEST_MESSAGE("Sigmoids (using std) on " << N << "x" << D << " random real numbers took " << timer_.format(5, "%t") << " seconds.");

  timer_.start();
  for (auto i = 0u; i < N; ++i)
    sigmoid(&vec[i][0], D);
  timer_.stop();
  BOOST_TEST_MESSAGE("Sigmoids (using sigmoid with vector<double> in-place) on " << N << "x" << D << " random real numbers took " << timer_.format(5, "%t") << " seconds (" << static_cast<double>((stdmath_elapsed.system + stdmath_elapsed.user)) / (timer_.elapsed().system + timer_.elapsed().user) << "x).");

  timer_.start();
  for (auto i = 0u; i < N; ++i)
    sigmoid(arr[i], D);
  timer_.stop();
  BOOST_TEST_MESSAGE("Sigmoids (using sigmoid with arrays) on " << N << "x" << D << " random real numbers took " << timer_.format(5, "%t") << " seconds (" << static_cast<double>((stdmath_elapsed.system + stdmath_elapsed.user)) / (timer_.elapsed().system + timer_.elapsed().user) << "x).");

  for (auto i = 0u; i < N; i += 10)
    for (auto j = 0u; j < D; ++j)
    {
      BOOST_REQUIRE_CLOSE(vec[i][j], ans[i][j], rel_tolerance);
      BOOST_REQUIRE_CLOSE(arr[i][j], ans[i][j], rel_tolerance);
    }
}

BOOST_AUTO_TEST_SUITE_END();
/// @endcond
