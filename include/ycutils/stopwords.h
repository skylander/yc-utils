/**
 * @file
 * @brief Utility functions for managing stopwords.
 * @details This file contains a few lists of stopwords obtained from https://code.google.com/p/stop-words/.
 */
#pragma once
#ifndef YCUTILS_STOPWORDS_H_INCLUDED
#define YCUTILS_STOPWORDS_H_INCLUDED

#include <string>
#include <unordered_set>

#include "ycutils.h"

namespace ycutils
{
  /// Alias for \p unordered_set of stopwords.
  using stopwords_set = std::unordered_set<std::string>;

  /// \p stopwords namespace defines sets of stopwords.
  namespace stopwords
  {
    /// Unicode punctuations (i.e unicode characters with category \p P).
    static const stopwords_set punctuations({"\u0021", "\u0022", "\u0023", "\u0025", "\u0026", "\u0027", "\u0028", "\u0029", "\u002a", "\u002c", "\u002d", "\u002e", "\u002f", "\u003a", "\u003b", "\u003f", "\u0040", "\u005b", "\u005c", "\u005d", "\u005f", "\u007b", "\u007d", "\u00a1", "\u00ab", "\u00b7", "\u00bb", "\u00bf", "\u037e", "\u0387", "\u055a", "\u055b", "\u055c", "\u055d", "\u055e", "\u055f", "\u0589", "\u058a", "\u05be", "\u05c0", "\u05c3", "\u05c6", "\u05f3", "\u05f4", "\u0609", "\u060a", "\u060c", "\u060d", "\u061b", "\u061e", "\u061f", "\u066a", "\u066b", "\u066c", "\u066d", "\u06d4", "\u0700", "\u0701", "\u0702", "\u0703", "\u0704", "\u0705", "\u0706", "\u0707", "\u0708", "\u0709", "\u070a", "\u070b", "\u070c", "\u070d", "\u07f7", "\u07f8", "\u07f9", "\u0830", "\u0831", "\u0832", "\u0833", "\u0834", "\u0835", "\u0836", "\u0837", "\u0838", "\u0839", "\u083a", "\u083b", "\u083c", "\u083d", "\u083e", "\u0964", "\u0965", "\u0970", "\u0df4", "\u0e4f", "\u0e5a", "\u0e5b", "\u0f04", "\u0f05", "\u0f06", "\u0f07", "\u0f08", "\u0f09", "\u0f0a", "\u0f0b", "\u0f0c", "\u0f0d", "\u0f0e", "\u0f0f", "\u0f10", "\u0f11", "\u0f12", "\u0f3a", "\u0f3b", "\u0f3c", "\u0f3d", "\u0f85", "\u0fd0", "\u0fd1", "\u0fd2", "\u0fd3", "\u0fd4", "\u104a", "\u104b", "\u104c", "\u104d", "\u104e", "\u104f", "\u10fb", "\u1361", "\u1362", "\u1363", "\u1364", "\u1365", "\u1366", "\u1367", "\u1368", "\u1400", "\u166d", "\u166e", "\u169b", "\u169c", "\u16eb", "\u16ec", "\u16ed", "\u1735", "\u1736", "\u17d4", "\u17d5", "\u17d6", "\u17d8", "\u17d9", "\u17da", "\u1800", "\u1801", "\u1802", "\u1803", "\u1804", "\u1805", "\u1806", "\u1807", "\u1808", "\u1809", "\u180a", "\u1944", "\u1945", "\u19de", "\u19df", "\u1a1e", "\u1a1f", "\u1aa0", "\u1aa1", "\u1aa2", "\u1aa3", "\u1aa4", "\u1aa5", "\u1aa6", "\u1aa8", "\u1aa9", "\u1aaa", "\u1aab", "\u1aac", "\u1aad", "\u1b5a", "\u1b5b", "\u1b5c", "\u1b5d", "\u1b5e", "\u1b5f", "\u1b60", "\u1c3b", "\u1c3c", "\u1c3d", "\u1c3e", "\u1c3f", "\u1c7e", "\u1c7f", "\u1cd3", "\u2010", "\u2011", "\u2012", "\u2013", "\u2014", "\u2015", "\u2016", "\u2017", "\u2018", "\u2019", "\u201a", "\u201b", "\u201c", "\u201d", "\u201e", "\u201f", "\u2020", "\u2021", "\u2022", "\u2023", "\u2024", "\u2025", "\u2026", "\u2027", "\u2030", "\u2031", "\u2032", "\u2033", "\u2034", "\u2035", "\u2036", "\u2037", "\u2038", "\u2039", "\u203a", "\u203b", "\u203c", "\u203d", "\u203e", "\u203f", "\u2040", "\u2041", "\u2042", "\u2043", "\u2045", "\u2046", "\u2047", "\u2048", "\u2049", "\u204a", "\u204b", "\u204c", "\u204d", "\u204e", "\u204f", "\u2050", "\u2051", "\u2053", "\u2054", "\u2055", "\u2056", "\u2057", "\u2058", "\u2059", "\u205a", "\u205b", "\u205c", "\u205d", "\u205e", "\u207d", "\u207e", "\u208d", "\u208e", "\u2329", "\u232a", "\u2768", "\u2769", "\u276a", "\u276b", "\u276c", "\u276d", "\u276e", "\u276f", "\u2770", "\u2771", "\u2772", "\u2773", "\u2774", "\u2775", "\u27c5", "\u27c6", "\u27e6", "\u27e7", "\u27e8", "\u27e9", "\u27ea", "\u27eb", "\u27ec", "\u27ed", "\u27ee", "\u27ef", "\u2983", "\u2984", "\u2985", "\u2986", "\u2987", "\u2988", "\u2989", "\u298a", "\u298b", "\u298c", "\u298d", "\u298e", "\u298f", "\u2990", "\u2991", "\u2992", "\u2993", "\u2994", "\u2995", "\u2996", "\u2997", "\u2998", "\u29d8", "\u29d9", "\u29da", "\u29db", "\u29fc", "\u29fd", "\u2cf9", "\u2cfa", "\u2cfb", "\u2cfc", "\u2cfe", "\u2cff", "\u2e00", "\u2e01", "\u2e02", "\u2e03", "\u2e04", "\u2e05", "\u2e06", "\u2e07", "\u2e08", "\u2e09", "\u2e0a", "\u2e0b", "\u2e0c", "\u2e0d", "\u2e0e", "\u2e0f", "\u2e10", "\u2e11", "\u2e12", "\u2e13", "\u2e14", "\u2e15", "\u2e16", "\u2e17", "\u2e18", "\u2e19", "\u2e1a", "\u2e1b", "\u2e1c", "\u2e1d", "\u2e1e", "\u2e1f", "\u2e20", "\u2e21", "\u2e22", "\u2e23", "\u2e24", "\u2e25", "\u2e26", "\u2e27", "\u2e28", "\u2e29", "\u2e2a", "\u2e2b", "\u2e2c", "\u2e2d", "\u2e2e", "\u2e30", "\u2e31", "\u3001", "\u3002", "\u3003", "\u3008", "\u3009", "\u300a", "\u300b", "\u300c", "\u300d", "\u300e", "\u300f", "\u3010", "\u3011", "\u3014", "\u3015", "\u3016", "\u3017", "\u3018", "\u3019", "\u301a", "\u301b", "\u301c", "\u301d", "\u301e", "\u301f", "\u3030", "\u303d", "\u30a0", "\u30fb", "\ua4fe", "\ua4ff", "\ua60d", "\ua60e", "\ua60f", "\ua673", "\ua67e", "\ua6f2", "\ua6f3", "\ua6f4", "\ua6f5", "\ua6f6", "\ua6f7", "\ua874", "\ua875", "\ua876", "\ua877", "\ua8ce", "\ua8cf", "\ua8f8", "\ua8f9", "\ua8fa", "\ua92e", "\ua92f", "\ua95f", "\ua9c1", "\ua9c2", "\ua9c3", "\ua9c4", "\ua9c5", "\ua9c6", "\ua9c7", "\ua9c8", "\ua9c9", "\ua9ca", "\ua9cb", "\ua9cc", "\ua9cd", "\ua9de", "\ua9df", "\uaa5c", "\uaa5d", "\uaa5e", "\uaa5f", "\uaade", "\uaadf", "\uabeb", "\ufd3e", "\ufd3f", "\ufe10", "\ufe11", "\ufe12", "\ufe13", "\ufe14", "\ufe15", "\ufe16", "\ufe17", "\ufe18", "\ufe19", "\ufe30", "\ufe31", "\ufe32", "\ufe33", "\ufe34", "\ufe35", "\ufe36", "\ufe37", "\ufe38", "\ufe39", "\ufe3a", "\ufe3b", "\ufe3c", "\ufe3d", "\ufe3e", "\ufe3f", "\ufe40", "\ufe41", "\ufe42", "\ufe43", "\ufe44", "\ufe45", "\ufe46", "\ufe47", "\ufe48", "\ufe49", "\ufe4a", "\ufe4b", "\ufe4c", "\ufe4d", "\ufe4e", "\ufe4f", "\ufe50", "\ufe51", "\ufe52", "\ufe54", "\ufe55", "\ufe56", "\ufe57", "\ufe58", "\ufe59", "\ufe5a", "\ufe5b", "\ufe5c", "\ufe5d", "\ufe5e", "\ufe5f", "\ufe60", "\ufe61", "\ufe63", "\ufe68", "\ufe6a", "\ufe6b", "\uff01", "\uff02", "\uff03", "\uff05", "\uff06", "\uff07", "\uff08", "\uff09", "\uff0a", "\uff0c", "\uff0d", "\uff0e", "\uff0f", "\uff1a", "\uff1b", "\uff1f", "\uff20", "\uff3b", "\uff3c", "\uff3d", "\uff3f", "\uff5b", "\uff5d", "\uff5f", "\uff60", "\uff61", "\uff62", "\uff63", "\uff64", "\uff65"});

    /// A small list of English stopwords
    static const stopwords_set english({"i", "a", "about", "an", "are", "as", "at", "be", "by", "com", "for", "from", "how", "in", "is", "it", "of", "on", "or", "that", "the", "this", "to", "was", "what", "when", "where", "who", "will", "with", "the", "www", "b", "c", "d", "e", "f", "g", "h", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"});

    /// A small list of Chinese stopwords
    static const stopwords_set chinese({"的", "一", "不", "在", "人", "有", "是", "为", "以", "于", "上", "他", "而", "后", "之", "来", "及", "了", "因", "下", "可", "到", "由", "这", "与", "也", "此", "但", "并", "个", "其", "已", "无", "小", "我", "们", "起", "最", "再", "今", "去", "好", "只", "又", "或", "很", "亦", "某", "把", "那", "你", "乃", "它", "吧", "被", "比", "别", "趁", "当", "从", "到", "得", "打", "凡", "儿", "尔", "该", "各", "给", "跟", "和", "何", "还", "即", "几", "既", "看", "据", "距", "靠", "啦", "了", "另", "么", "每", "们", "嘛", "拿", "哪", "那", "您", "凭", "且", "却", "让", "仍", "啥", "如", "若", "使", "谁", "虽", "随", "同", "所", "她", "哇", "嗡", "往", "哪", "些", "向", "沿", "哟", "用", "于", "咱", "则", "怎", "曾", "至", "致", "着", "诸", "自"});

    /// A list of names of predefined stopwords.
    static const std::vector<std::string> predefined({"english", "chinese", "punctuations"});
  }  // namespace stopwords

  /**
   * @brief Read stopwords from path given by \p filename.
   *
   * @details Assumes that there is one stopword per line in file.
   *
   * @param filename file to read stopwords from
   * @param it add stopwords here.
   * @return number of stopwords read from file.
   */
  template <typename OutputIterator>
  inline size_type load_stopwords_file(const std::string& filename, OutputIterator it)
  {
      std::ifstream fin(filename);
      auto count = 0u;

      for(std::string line; std::getline(fin, line); )
        if (!line.empty())
        {
          // auto it = std::inserter(*stopwords, stopwords->end());
          *it = line;
          ++it;
          count++;
        }
      fin.close();

      return count;
  }

  /**
   * @brief Return the predefined \p stopword_set associated with \p name.
   * @param name name of predefined stopword list to use.
   * @return a \p const eference to a set of stopwords.
   * @throw exception when \p name is not found in stopwords::predefined.
   */
  inline const stopwords_set& use_predefined(const std::string& name)
  {
    if (name == "english")
      return ycutils::stopwords::english;
    else if (name == "chinese")
      return ycutils::stopwords::chinese;
    else if (name == "punctuations")
      return ycutils::stopwords::punctuations;

    throw std::logic_error(std::string("'") + name + "' is not a valid predefined stopword list. Possible options are: " + boost::algorithm::join(stopwords::predefined, ", ") + ".");

    return ycutils::stopwords::punctuations;  // to get rid of <tt>control reaches end of non-void function</tt> warning.
  }

  /**
   * @brief Helper function to quickly add stopwords to list \p stopwords.
   * @param filenames sequence of filenames to read stopwords from.
   * @param predefined sequence of names of predefined stopwords lists to use.
   * @param stopwords pointer to stopword container.
   * @param verbose whether to display status updates to \p out.
   * @param out output stream for displaying status messages (defaults to \p std::cerr).
   * @return number of stopwords loaded
   */
  template <typename FilenameList, typename PredefinedList, typename StopwordsList>
  inline size_type init_stopwords(FilenameList filenames, PredefinedList predefined, StopwordsList *stopwords, bool verbose = true, std::ostream& out = std::cerr)
  {
    auto old_size = stopwords->size();
    for (auto& filename : filenames)
    {
      verbose && out << "Reading stopwords from '" << filename << "'...";
      auto count = load_stopwords_file(filename, std::inserter(*stopwords, stopwords->end()));
      verbose && out << count << " stopwords found." << std::endl;
    }

    for (auto& name : predefined)
    {
      const stopwords_set& sw_set = use_predefined(name);
      verbose && out << "Using " << sw_set.size() << " stopwords from pre-defined stopwords list '" << name << "'." << std::endl;
      boost::copy(sw_set, std::inserter(*stopwords, stopwords->end()));
    }

    verbose && !stopwords->empty() && out << "There are " << stopwords->size() << " types in the stopwords list." << std::endl;

    return stopwords->size() - old_size;
  }
}

#endif  // YCUTILS_STOPWORDS_H_INCLUDED
