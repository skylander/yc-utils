/**
 * @file
 * @brief Implementation of the Metropolis-Hastings algorithm.
 */
#ifndef YCUTILS_METROPOLIS_HASTINGS_H_INCLUDED
#define YCUTILS_METROPOLIS_HASTINGS_H_INCLUDED

#include <algorithm>
#include <iostream>
#include <random>
#include <vector>

#include <boost/random/beta_distribution.hpp>
#include <boost/math/special_functions/gamma.hpp>

#include "ycutils.h"
#ifdef USE_EIGEN
#include <Eigen/Dense>
#endif

#include "ycutils/collections.h"
#include "ycutils/math.h"
#include "ycutils/random.h"
#include "ycutils/sampling.h"

namespace ycutils {

/**
 * @brief An implementation of the Metropolis-Hastings algorithm.
 * @tparam State The type of a state variable that will be sampled.
 */
template <typename State>
class metropolis_hastings_sampler
{
public:
  /// Type alias for \p State.
  using state_type = State;

  /// Empty constructor.
  metropolis_hastings_sampler() : _burnin(10), _samples_count(10), _rng(time_seed()), _gen01(_rng, std::uniform_real_distribution<>(0, 1.0)) {}

  /// Constructor with default random number generator seeded using \p ycutils::time_seed(), with burn in of 10 and sample count of 10.
  metropolis_hastings_sampler(const State& zero_state) : _burnin(10), _samples_count(10), _zero_state(zero_state), _rng(time_seed()), _gen01(_rng, std::uniform_real_distribution<>(0, 1.0)) {}

  /// Constructor with default random number generator seeded using \p ycutils::time_seed().
  metropolis_hastings_sampler(const State& zero_state, const size_type burnin, const size_type samples_count) : _burnin(burnin), _samples_count(samples_count), _zero_state(zero_state), _rng(time_seed()), _gen01(_rng, std::uniform_real_distribution<>(0, 1.0)) {}

  /**
   * @brief Constructs a \p metropolis_hastings_sampler with the given parameters.
   * @details See <a href="http://en.wikipedia.org/wiki/Metropolis%E2%80%93Hastings_algorithm#Step-by-step_instructions">Wikipedia article</a> for algorithmic details.
   * @param zero_state A variable representing the \p 0 value of \p State type.
   * @param burnin No. of samples to discard for burnin.
   * @param samples_count No. of samples to collect.
   * @param rng Random number generator.
   */
  metropolis_hastings_sampler(const State& zero_state, const size_type burnin, const size_type samples_count, default_random_engine& rng) : _burnin(burnin), _samples_count(samples_count), _zero_state(zero_state), _rng(rng), _gen01(rng, std::uniform_real_distribution<>(0, 1.0)) {}

  /**
   * @brief Runs the Metropolis-Hastings algorithm.
   * @details Algorithm based on <a href="http://en.wikipedia.org/wiki/Metropolis%E2%80%93Hastings_algorithm#Step-by-step_instructions">Wikipedia article</a>.
   * @param logll_func function computes the log likelihood of a given \p State.
   * @param proposal function proposes a new \p State given the current \p State.
   * @param proposal_logratio computes the log ratio between the proposed \p State and the current \p State.
   * @param initial_state the initial state
   * @param progress function called during every iteration with progress of sampler
   * @return the acceptance ratio
   */
  template <typename LogLLFunction, typename ProposalFunction, typename ProposalLogRatioFunction, typename ProgressFunction>
  double run(LogLLFunction logll_func, ProposalFunction proposal, ProposalLogRatioFunction proposal_logratio, const State& initial_state, ProgressFunction progress)
  {
    _samples.clear();
    _sample_mean = _zero_state;

    State x_cur(initial_state);
    auto x_cur_logll = logll_func(x_cur);
    double accept_ratio = 0;

    for (const auto iter : irange(_burnin + _samples_count))
    {
      const auto x_proposed = proposal(x_cur);
      const auto x_proposed_logll = logll_func(x_proposed);

      const auto log_a1 = x_proposed_logll - x_cur_logll;
      const auto log_a2 = proposal_logratio(x_proposed, x_cur);

      const auto a = (log_a1 + log_a2 > 0.0) ? 1.0 : std::exp(log_a1 + log_a2);

      const auto stop = progress(iter, x_cur, x_proposed, x_cur_logll, x_proposed_logll, log_a2, a);

      if (a >= 1.0 || _gen01() < a)
      {
        x_cur = x_proposed;
        x_cur_logll = x_proposed_logll;
        accept_ratio += 1;
      }

      if (stop) break;

      if (iter >= _burnin)
      {
        _samples.push_back(x_cur);
        _sample_mean += x_cur;
      }
    }

    assert(_samples_count == _samples.size());
    _sample_mean /= _samples_count;

    return accept_ratio / (_burnin + _samples_count);
  }

  /**
   * @brief See \p metropolis_hastings_sampler::run
   * @see metropolis_hastings_sampler::run
   */
  template <typename LogLLFunction, typename ProposalFunction, typename ProposalLogRatioFunction>
  double run(LogLLFunction logll_func, ProposalFunction proposal, ProposalLogRatioFunction proposal_logratio, const State& initial_state)
  {
    auto null_progress = [](index_type iteration, const State& x_cur, const State& x_proposed, double x_cur_logll, double x_proposed_logll, double log_proposal_ratio, double acceptance_probability) { return false; };
    return run(logll_func, proposal, proposal_logratio, initial_state, null_progress);
  }

  /**
   * @brief See \p metropolis_hastings_sampler::run
   * @see metropolis_hastings_sampler::run
   */
  template <typename LogLLFunction, typename ProposalFunction>
  double run(LogLLFunction logll_func, ProposalFunction proposal, const State& initial_state)
  {
    return run(logll_func, proposal, [](const State& x_proposed, const State& x_cur) { return 0.0; }, initial_state);
  }

  /// Returns the sample mean after a sampling run.
  const State& mean() const { assert(!_samples.empty()); return _sample_mean; }

  /// Returns a vector of samples.
  const std::vector<State>& samples() const { return _samples; }

  /// Returns the last sample during the last run.
  const State& last_sample() const { assert(!_samples.empty()); return _samples.back(); }

  /// Set the number of samples to collect.
  void samples_count(size_type count) { _samples_count = count; }

  /// Set the number of burn in samples.
  void burnin(size_type burnin) { _burnin = burnin; }

  /// Set the seed for the random number generator.
  void seed_rng(const double seed) { _rng.seed(seed); }

  /// Set the zero state
  void zero_state(const State& zero) { _zero_state = zero; }

  /// Static function that prints the sampling progress to \p std::cerr.
  static bool debug_progress(index_type iteration, const State& x_cur, const State& x_proposed, double x_cur_logll, double x_proposed_logll, double log_proposal_ratio, double acceptance_probability)
  {
    std::cerr << "iter:" << iteration << " x'=" << x_proposed << " (" << x_proposed_logll << ") x=" << x_cur << " (" << x_cur_logll << ") log(P_ratio)=" << (x_proposed_logll - x_cur_logll) << " log(Q_ratio)=" << log_proposal_ratio << " P(accept)=" << acceptance_probability << std::endl;

    return false;
  }

  /// Symmetric log ratio that always return 0
  static double symmetric_log_ratio(const State& x_proposed, const State& x_cur)
  {
    return 0.0;
  }

protected:
  /// Keeps track of samples.
  std::vector<State> _samples;

  /// Mean of \p _samples.
  State _sample_mean;

  /// No. of burn in samples which will be discarded
  size_type _burnin;

  /// No. of samples we need
  size_type _samples_count;

  /// The "zero" \p State
  State _zero_state;

  /// Our local random number generator
  default_random_engine _rng;

  /// Variate generator for \p U(0,1).
  mersenne_generator<std::uniform_real_distribution<> > _gen01;
};

template <typename State, typename ProposalType>
class adaptive_metropolis_hastings_sampler : public metropolis_hastings_sampler<State>
{
public:
  using metropolis_hastings_sampler<State>::metropolis_hastings_sampler;

  /// Constructs an adaptive MH sampler with the given proposal.
  adaptive_metropolis_hastings_sampler(const State& zero_state, const ProposalType& proposal, const double factor) : metropolis_hastings_sampler<State>(zero_state), _proposal(proposal), _factor(factor) {}

  /// Constructs an adaptive MH sampler with the given proposal.
  adaptive_metropolis_hastings_sampler(const State& zero_state, const size_type burnin, const size_type samples_count, const ProposalType& proposal, const double factor) : metropolis_hastings_sampler<State>(zero_state, burnin, samples_count), _proposal(proposal), _factor(factor) {}

  /**
   * @brief See \p metropolis_hastings_sampler::run
   * @see metropolis_hastings_sampler::run
   */
  template <typename LogLLFunction>
  double run(LogLLFunction logll_func, const State& initial_state)
  {
    auto logratio_func = [&](const State& x_proposed, const State& x_cur) { return _proposal.log_ratio(x_proposed, x_cur); };
    // auto proposal_func = [&](auto x) { return _proposal(x); };
    auto accept = ((metropolis_hastings_sampler<State> *) this)->run(logll_func, _proposal, logratio_func, initial_state);

    if (accept <= 0.15) _proposal.tighten(_factor);
    if (accept >= 0.5) _proposal.loosen(_factor);

    return accept;
  }

  ProposalType& proposal() { return _proposal; }

  void factor(double factor) { _factor = factor; }

  double factor() const { return _factor; }

private:
  ProposalType _proposal;
  double _factor;
};

/**
 * @brief Abstract base class for proposal functions that have a scalar parameter and is tunable.
 * @details It contains \p ycutils::tunable_proposal::tighten and \p ycutils::tunable_proposal::loosen methods for adapting the proposal function's hyperparameters.
 */
template <typename HyperparameterType = double>
class tunable_proposal
{
public:
  /// Make it easier for proposals to be accepted. Increases acceptance rate.
  virtual void tighten(double factor) = 0;

  /// Make it harder for proposals to be accepted. Increases acceptance rate.
  virtual void loosen(double factor) = 0;

  /// Get the tunable hyperparameter.
  virtual const HyperparameterType& hyperparameter() const = 0;
};

/**
 * @brief This is an implementation of the multivariate Gaussian proposal distribution with diagonal covariance.
 * @details It is often used with Metropolis Hastings algorithm.
 * This class can be used directly with \p ycutils::metropolis_hastings_sampler
 * @tparam T the container type for multivariate variable.
 */
 template <typename T = std::vector<double> >
class mvn_proposal : public tunable_proposal<double>
{
public:
  /**
   * @brief Constructs a multivariate Gaussian proposal distribution.
   * @param N No. of dimensions
   * @param stddev Standard deviation of each coordinate (assumes diagonal covariance).
   * @param rng Random number generator.
   */
  mvn_proposal(size_type N, double stddev, default_random_engine& rng) : _N(N), _stddev(stddev), _rng(rng), _normal(0, stddev) {}

  /// Constructor with default random engine.
  mvn_proposal(size_type N, double stddev) : _N(N), _stddev(stddev), _rng(time_seed()), _normal(0, stddev) {}

  /// Constructor with default random engine and 1.0 standard deviation.
  mvn_proposal(size_type N) : _N(N), _stddev(1.0), _rng(time_seed()), _normal(0, 1.0) {}

  void tighten(double factor) { _stddev /= factor; _normal.param(std::normal_distribution<>::param_type(0.0, _stddev)); }

  void loosen(double factor) { _stddev *= factor; _normal.param(std::normal_distribution<>::param_type(0.0, _stddev)); }

  const double& hyperparameter() const { return _stddev; }

  /**
   * @brief Operator overloaded proposal distribution.
   * @param v Current value, which will be used as the mean.
   * @return A proposal value.
   */
  virtual T operator()(const T& v)
  {
    T proposal = v;
    for (const auto i : ycutils::irange(_N))
      proposal[i] += _normal(_rng);

    return proposal;
  }

  /**
   * @brief Returns the log ratio between a \p current and \p proposed value.
   * @param proposed proposed value
   * @param current current value
   * @return the log ratio \f$ \log \frac{p(\text{current} \mid \text{proposed})}{p(\text{proposed} \mid \text{current})} \f$
   */
  virtual double log_ratio(const T& proposed, const T& current) { return 0; }

protected:
  /// No. of dimensions
  size_type _N;

  /// Standard deviation of proposal distribution
  double _stddev;

  /// Our local random number generator
  default_random_engine _rng;

  /// Univariate normal distribution generator.
  std::normal_distribution<> _normal;
};

/**
 * @brief This is an implementation of a multivariate <a href="http://en.wikipedia.org/wiki/Logit-normal_distribution">Logistic Normal</a> distribution with diagonal covariance.
 * @details It is based on \p ycutils::mvn_proposal with logistic transformations.
 * @tparam T the container type for multivariate variable.
 */
template <typename T = std::vector<double> >
class logistic_normal_proposal : public mvn_proposal<T>
{
public:
  using mvn_proposal<T>::mvn_proposal;  // base class constructors

  /**
   * @brief Operator overloaded proposal distribution.
   * @param v Current value, which will be used as the mean.
   * @return A proposal value (which sums up to 1)
   */
  T operator()(const T& v)
  {
    T proposal(v);
    double sum = 0.0;
    const auto log_v_first = std::log(v[0]);
    for (const auto i : ycutils::irange(this->_N))
    {
      proposal[i] = std::exp(std::log(v[i]) - log_v_first + this->_normal(this->_rng));
      sum += proposal[i];
    }

    for (const auto i : ycutils::irange(this->_N))
      proposal[i] /= sum;

    return proposal;
  }

  /**
   * @brief Returns the log ratio between a \p current and \p proposed value.
   * @param proposed proposed value
   * @param current current value
   * @return the log ratio \f$ \log \frac{p(\text{current} \mid \text{proposed})}{p(\text{proposed} \mid \text{current})} \f$
   */
  double log_ratio(const T& proposed, const T& current) { return 0; }
};

#if defined(USE_EIGEN) || defined(DOXYGEN)
/**
 * @brief This is an implementation of a multivariate time series regularized distribution based on <a href="http://www.cs.cmu.edu/~dyogatam/papers/yogatama+etal.emnlp11.pdf">Yogatama et. al. (2011)</a>.
 * @tparam Scalar = double
 */
template <typename Scalar = double>
class time_series_mvn_proposal
{
public:
  /// Name alias for Eigen vector type.
  using Vector = Eigen::Matrix<Scalar, Eigen::Dynamic, 1>;

  /// Name alias for Eigen matrix type.
  using Matrix = Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>;

  /**
   * @brief Create a time series proposal distribution with parameters \f$ \lambda \f$ and \f$ \alpha \f$.
   * @details The definitions of parameters \f$ \lambda \f$ and \f$ \alpha \f$ are same as that in <a href="http://www.cs.cmu.edu/~dyogatam/papers/yogatama+etal.emnlp11.pdf#page=4">Yogatama et. al. (2011) [p. 4 sec 3.3]</a>.
   * @param N No. of dimensions
   * @param lambda Sparsity
   * @param alpha Time series sparsity
   * @param rng Random number engine
   */
  time_series_mvn_proposal(size_type N, double lambda, double alpha, default_random_engine& rng) : _N(N), _rng(rng), _mvn(N) { _setup_mvn(lambda, alpha); }

  /// Constructor using the default random engine.
  time_series_mvn_proposal(size_type N, double lambda, double alpha) : _N(N), _rng(time_seed()), _mvn(N) { _setup_mvn(lambda, alpha); }

  /**
   * @brief Operator overloaded proposal distribution.
   * @param v Current value, which will be used as the mean.
   * @return A proposal value (which sums up to 1)
   */
  Vector operator()(const Vector& v)
  {
    assert(static_cast<size_type>(v.size()) == _N);
    return v + _mvn(_rng);
  }

  /**
   * @brief Returns the log ratio between a \p current and \p proposed value.
   * @param proposed proposed value
   * @param current current value
   * @return the log ratio \f$ \log \frac{p(\text{current} \mid \text{proposed})}{p(\text{proposed} \mid \text{current})} \f$
   */
  double log_ratio(const Vector& proposed, const Vector& current) { return 0; }

protected:
  /// No. of dimensions.
  size_type _N;

  /// Our local random number generator
  default_random_engine _rng;

  /// A \p ycutils::multivariate_normal_distribution with covariance matrix parametrized with our time series parameters.
  multivariate_normal_distribution<Scalar> _mvn;

private:
  /// Build the covariance matrix for our \p ycutils::multivariate_normal_distribution.
  void _setup_mvn(Scalar lambda, Scalar alpha)
  {
    Matrix precision = Matrix::Identity(_N, _N);
    precision.diagonal() = (1 + alpha + alpha) * Vector::Ones(_N);
    precision.diagonal(-1) = (-alpha) * Vector::Ones(_N - 1);
    precision.diagonal(1) = (-alpha) * Vector::Ones(_N - 1);;
    precision(0, 0) -= alpha;
    precision(_N - 1, _N - 1) -= alpha;
    precision *= lambda;

    _mvn.mean(Vector::Zero(_N));
    _mvn.covariance(precision.inverse());
  }
};
#endif

/**
 * @brief This is a simple implementation of the univariate Gaussian proposal distribution with diagonal covariance.
 * @details It is often used with Metropolis Hastings algorithm.
 * This class can be used directly with \p ycutils::metropolis_hastings_sampler
 * @tparam T the container type for univariate variable.
 */
template <typename T = double>
class uvn_proposal : public tunable_proposal<double>
{
public:
  /**
   * @brief Constructs a univariate Gaussian proposal distribution.
   * @param stddev Standard deviation of each coordinate (assumes diagonal covariance).
   * @param rng Random number generator.
   */
  uvn_proposal(double stddev, default_random_engine& rng) : _stddev(stddev), _rng(rng), _normal(0, stddev) {}

  /// Constructor with default random engine.
  uvn_proposal(double stddev) : _stddev(stddev), _rng(time_seed()), _normal(0, stddev) {}

  /// Constructor with default random engine and 1.0 standard deviation.
  uvn_proposal() : _stddev(1.0), _rng(time_seed()), _normal(0, 1.0) {}

  /**
   * @brief Operator overloaded proposal distribution.
   * @param v Current value, which will be used as the mean.
   * @return A proposal value.
   */
  T operator()(const T& v) { return v + _normal(_rng); }

  /**
   * @brief Returns the log ratio between a \p current and \p proposed value.
   * @param proposed proposed value
   * @param current current value
   * @return the log ratio \f$ \log \frac{p(\text{current} \mid \text{proposed})}{p(\text{proposed} \mid \text{current})} \f$
   */
  double log_ratio(const T& proposed, const T& current) { return 0; }

  void tighten(double factor) { _stddev /= factor; _normal.param(std::normal_distribution<>::param_type(0.0, _stddev)); }

  void loosen(double factor) { _stddev *= factor; _normal.param(std::normal_distribution<>::param_type(0.0, _stddev)); }

  const double& hyperparameter() const { return _stddev; }

protected:
  /// Standard deviation of proposal distribution
  double _stddev;

  /// Our local random number generator
  default_random_engine _rng;

  /// Univariate normal distribution generator.
  std::normal_distribution<> _normal;
};

/**
 * @brief This is an implementation of a univariate <a href="https://en.wikipedia.org/wiki/Log-normal_distribution">Log-Normal</a> distribution.
 * @details It is based on \p ycutils::uvn_proposal with exponential transformation. Mean and variance corresponds to \f$\mu\f$ and \f$\sigma^2\f$ used on Wikipedia page.
 * @tparam T the container type for uniivariate variable.
 */
template <typename T = double>
class lognormal_proposal : uvn_proposal<T>
{
public:
  using uvn_proposal<T>::uvn_proposal;  // base class constructors

  /**
   * @brief Operator overloaded proposal distribution.
   * @param v Current value, which will be used as the mean.
   * @return A proposal value.
   */
  T operator()(const T& v) { return std::exp(std::log(v) + this->_normal(this->_rng)); }

  /**
   * @brief Returns the log ratio between a \p current and \p proposed value.
   * @param proposed proposed value
   * @param current current value
   * @return the log ratio \f$ \log \frac{p(\text{current} \mid \text{proposed})}{p(\text{proposed} \mid \text{current})} \f$
   */
  double log_ratio(const T& proposed, const T& current) { return 0; }
};

/**
 * @brief This is a implementation of a Dirichlet proposal distribution with multiplier \p multiplier.
 * @details Given a current vector \f$ x \f$ and a multiplier \f$ \alpha > 0 \f$, we use \f$ \alpha x \f$ as the mean of the proposed Dirichlet multinomial.
 * This class can be used directly with \p ycutils::metropolis_hastings_sampler.
 * @tparam T the container vector type.
 */
template <typename T = std::vector<double> >
class dirichlet_proposal : public tunable_proposal<double>
{
public:
  /**
   * @brief Constructs a Dirichlet proposal distribution.
   * @param N No. of dimensions
   * @param multiplier Multiplier value to use
   * @param rng Random number generator.
   */
  dirichlet_proposal(size_type N, double multiplier, default_random_engine& rng) : _N(N), _multiplier(multiplier), _rng(rng) {}

  /// Constructor with default random engine.
  dirichlet_proposal(size_type N, double multiplier) : _N(N), _multiplier(multiplier), _rng(time_seed()) {}

  /// Constructor with default random engine and multiplier value of 1.
  dirichlet_proposal(size_type N) : _N(N), _multiplier(1.0), _rng(time_seed()) {}

  /**
   * @brief Operator overloaded proposal distribution.
   * @param v Current value.
   * @return A proposal value whose expectation is \f$ \alpha v \f$.
   */
  T operator()(const T& v)
  {
    using ycutils::math::close_to_zero;

    double alpha_sum = 0.0;
    alpha_sum += _multiplier;

    T proposal = v;
    double cum_sum = 0.0;
    for (const auto i : ycutils::irange(_N - 1))
    {
      const auto alpha_i = _multiplier * v[i];
      alpha_sum -= alpha_i;

      boost::random::beta_distribution<> gen_beta(alpha_i, std::max(alpha_sum, close_to_zero));

      proposal[i] = std::max(gen_beta(_rng) * (1.0 - cum_sum), close_to_zero);
      cum_sum += proposal[i];
    }
    proposal[_N - 1] = std::max(1.0 - cum_sum, close_to_zero);
    cum_sum += proposal[_N - 1];
    if (cum_sum != 1.0)
    {
      for (const auto i : ycutils::irange(_N))
      {
        proposal[i] = proposal[i] / cum_sum;
        if (proposal[i] < 0) proposal[i] = 0.0;  // hack
      }
    }

    return proposal;
  }

  void tighten(double factor) { _multiplier *= factor; }

  void loosen(double factor) { _multiplier /= factor; }

  const double& hyperparameter() const { return _multiplier; }

  /**
   * @brief Returns the log ratio between a \p current and \p proposed value.
   * @param proposed proposed value
   * @param current current value
   * @return the log ratio \f$ \log \frac{p(\text{current} \mid \text{proposed})}{p(\text{proposed} \mid \text{current})} \f$
   */
  double log_ratio(const T& proposed, const T& current)
  {
    return _log_ratio(proposed, current, T());
  }

protected:
  /// No. of dimensions
  size_type _N;

  /// Multiplier for proposal distribution
  double _multiplier;

  /// Our local random number generator
  default_random_engine _rng;

private:
  /**
   * @brief Private implementation of \p dirichlet_proposal::log_ratio that works with \p std::vector.
   */
  template <typename U = double>
  double _log_ratio(const std::vector<U>& proposed, const std::vector<U>& current, std::vector<U>)
  {
    const auto N = current.size();

    U log_p_proposed = 0, alpha_cur_sum = 0;
    for (const auto i : ycutils::irange(N))
    {
      const auto a = _multiplier * current[i];
      log_p_proposed += -boost::math::lgamma(a) + (std::log(proposed[i]) * (a - 1.0));
      alpha_cur_sum += a;
    }
    log_p_proposed += boost::math::lgamma(alpha_cur_sum);

    U log_p_current = 0, alpha_proposed_sum = 0;
    for (const auto i : ycutils::irange(N))
    {
      const auto a = _multiplier * proposed[i];
      log_p_current += -boost::math::lgamma(a) + (std::log(current[i]) * (a - 1.0));
      alpha_proposed_sum += a;
    }
    log_p_current += boost::math::lgamma(alpha_proposed_sum);

    return log_p_current - log_p_proposed;
  }

#if defined(USE_EIGEN) || defined(DOXYGEN)
  /**
   * @brief Private implementation of \p dirichlet_proposal::log_ratio that works with \p Eigen::VectorXd.
   */
  double _log_ratio(const Eigen::VectorXd& proposed, const Eigen::VectorXd& current, Eigen::VectorXd)
  {
    const auto alpha_current = _multiplier * current;
    double log_B_alpha_current = alpha_current.unaryExpr([](const double u) { return boost::math::lgamma(u); }).sum() - boost::math::lgamma(alpha_current.sum());
    double log_p_proposed = -log_B_alpha_current + proposed.unaryExpr([](const double u) { return std::log(u); }).dot(alpha_current.unaryExpr([](const double u) { return u - 1.0; }));

    const auto alpha_proposed = _multiplier * proposed;
    double log_B_alpha_proposed = alpha_proposed.unaryExpr([](const double u) { return boost::math::lgamma(u); }).sum() - boost::math::lgamma(alpha_proposed.sum());
    double log_p_current = -log_B_alpha_proposed + current.unaryExpr([](const double u) { return std::log(u); }).dot(alpha_proposed.unaryExpr([](const double u) { return u - 1.0; }));

    return log_p_current - log_p_proposed;
  }
#endif

};

}  // namespace ycutils

#endif  // YCUTILS_METROPOLIS_HASTINGS_H_INCLUDED
