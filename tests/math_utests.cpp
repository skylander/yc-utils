/**
 * @file
 * @brief Unit test code for the math header library \p ycutils/math.h.
 * @cond
 */

#include <ctime>

#include <iostream>
#include <iomanip>

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE MathTest
#include <boost/test/unit_test.hpp>

#include <boost/math/special_functions/expm1.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_real_distribution.hpp>
#include <boost/random/variate_generator.hpp>
#include <boost/timer/timer.hpp>

#define YCUTILS_GLOBAL_sigmoid_table

#include <ycutils/collections.h>
#include <ycutils/math.h>
#include <ycutils/mathtables.h>

BOOST_AUTO_TEST_SUITE(math_test_suite);

BOOST_AUTO_TEST_CASE(log_operations)
{
  using std::log;
  using std::exp;
  using ycutils::log_add;
  using ycutils::log_subtract;

  static constexpr auto rel_tolerance = 0.0001;
  static constexpr auto neg_inf = -std::numeric_limits<double>::infinity();

  BOOST_REQUIRE_CLOSE(exp(log_add(log(5.0), log(7.0))), 12.0, rel_tolerance);
  BOOST_REQUIRE_CLOSE(exp(log_add(log(1e-5), log(1e5))), 1e5 + 1e-5, rel_tolerance);

  #ifdef HONOR_INFINITIES
    BOOST_REQUIRE_CLOSE(exp(log_add(log(0.0), log(1e5))), 1e5, rel_tolerance);
    BOOST_REQUIRE_CLOSE(log_add(log(0), 5.0), 5.0, rel_tolerance);
    BOOST_REQUIRE_CLOSE(log_add(5.0, neg_inf), 5.0, rel_tolerance);
    BOOST_REQUIRE_CLOSE(log_add(neg_inf, 5.0), 5.0, rel_tolerance);
    BOOST_REQUIRE_CLOSE(log_add(5.0, neg_inf), 5.0, rel_tolerance);

    BOOST_REQUIRE_CLOSE(exp(log_subtract(log(1e5), log(0.0))), 1e5, rel_tolerance);
    BOOST_REQUIRE_CLOSE(log_subtract(5.0, neg_inf), 5.0, rel_tolerance);
    BOOST_REQUIRE_EQUAL(log_subtract(5.0, 5.0), neg_inf);
  #else
    BOOST_WARN_MESSAGE(false, "Skipping tests that involves infinities (-ffinite-math-only is enabled?).");
  #endif

  BOOST_REQUIRE_CLOSE(exp(log_subtract(log(7), log(5))), 2, rel_tolerance);
  BOOST_REQUIRE_CLOSE(exp(log_subtract(log(1e5), log(1e-5))), 1e5 - 1e-5, rel_tolerance);
}

BOOST_AUTO_TEST_CASE(math_operations)
{
  static constexpr auto rel_tolerance = 1e-8;

  BOOST_REQUIRE(ycutils::is_close(0.1, 0.1 + 1e-9));
  BOOST_REQUIRE(!ycutils::is_close(0.5, 0.1));

  using fp_type = ycutils::math::default_floating_point_type;
  using real_variate_generator = boost::random::variate_generator<boost::random::mt19937, boost::random::uniform_real_distribution<fp_type> >;

  boost::random::mt19937 rng(static_cast<unsigned int>(std::time(0)));
  real_variate_generator unif_real_dist(rng, boost::random::uniform_real_distribution<fp_type>(-std::numeric_limits<double>::max(), std::numeric_limits<double>::max()));

  for (auto i = 0u; i < 1000; ++i)
  {
    auto a = unif_real_dist();
    auto b = unif_real_dist();
    BOOST_REQUIRE_CLOSE(ycutils::safe_divide(a, b), a / b, rel_tolerance);
  }

  #if defined HONOR_INFINITIES && defined HONOR_NANS
    BOOST_REQUIRE_EQUAL(ycutils::safe_divide(5, 0.0), 0.0);
    BOOST_REQUIRE_EQUAL(ycutils::safe_divide(5, std::numeric_limits<double>::infinity()), 0.0);
    BOOST_REQUIRE_EQUAL(ycutils::safe_divide(5, std::numeric_limits<double>::nan()), 0.0);
    BOOST_REQUIRE_EQUAL(ycutils::safe_divide(5, 0.0), 0.0);
  #else
    BOOST_WARN_MESSAGE(false, "Skipping tests that involves infinities and NaNs (-ffinite-math-only is enabled?).");
  #endif

  std::vector<double> v1({1, 1, 1, 1});
  ycutils::softmax_inplace(v1);
  for (const auto v : v1)
    BOOST_REQUIRE_CLOSE(v, 0.25, 0.0001);

  std::vector<double> v2({-2, -1, 0, 1});
  double v2_exp_sum = 0.0;
  for (const auto v : v2)
    v2_exp_sum += std::exp(v);
  ycutils::softmax_inplace(v2);
  BOOST_REQUIRE_CLOSE(v2[0], std::exp(-2) / v2_exp_sum, 0.0001);
  BOOST_REQUIRE_CLOSE(v2[1], std::exp(-1) / v2_exp_sum, 0.0001);
  BOOST_REQUIRE_CLOSE(v2[2], std::exp(0) / v2_exp_sum, 0.0001);
  BOOST_REQUIRE_CLOSE(v2[3], std::exp(1) / v2_exp_sum, 0.0001);
}

BOOST_AUTO_TEST_CASE(sigmoid_table_correctness)
{
  using fp_type = ycutils::math::default_floating_point_type;
  using real_variate_generator = boost::random::variate_generator<boost::random::mt19937, boost::random::uniform_real_distribution<fp_type> >;

  static constexpr auto rel_tolerance = 0.0001;

  ycutils::sigmoid_table<> sigmoid_table;
  sigmoid_table.initialize();  // initialize with default values

  BOOST_TEST_MESSAGE("Size of default sigmoid_table is about " << (sigmoid_table.table_size() * sizeof(ycutils::sigmoid_table<>::value_type) / 1048576) << "mb.");
  for (auto i = ycutils::sigmoid_table<>::default_min; i <= ycutils::sigmoid_table<>::default_max - ycutils::sigmoid_table<>::default_precision; i += 5.0 * ycutils::sigmoid_table<>::default_precision)
  {
    BOOST_REQUIRE_CLOSE(sigmoid_table(i + (0.5 * ycutils::sigmoid_table<>::default_precision)), std::exp(i) / (1.0 + std::exp(i)), rel_tolerance);
  }
}

BOOST_AUTO_TEST_CASE(sigmoid_table_speed)
{
  using fp_type = ycutils::math::default_floating_point_type;
  using real_variate_generator = boost::random::variate_generator<boost::random::mt19937, boost::random::uniform_real_distribution<fp_type> >;

  static constexpr auto rel_tolerance = 0.00001;

  boost::random::mt19937 rng(static_cast<unsigned int>(std::time(0)));
  real_variate_generator unif_real_dist(rng, boost::random::uniform_real_distribution<fp_type>(ycutils::sigmoid_table<>::default_min, ycutils::sigmoid_table<>::default_max));

  static constexpr auto N = 10000000u;

  std::vector<fp_type> k(N);
  for (auto i = 0u; i < N; ++i)
    k[i] = unif_real_dist();

  fp_type sigmoid_table_sum = 0.0;
  fp_type stdsigmoid = 0.0;

  ycutils::sigmoid_table<> sigmoid_table;
  sigmoid_table.initialize();  // initialize with default values

  boost::timer::cpu_timer timer_;
  for (auto i = 0u; i < N; ++i)
    stdsigmoid += std::exp(k[i]) / (2.0 + std::expm1(k[i]));
  timer_.stop();
  const auto stdexp_elapsed(timer_.elapsed());
  BOOST_TEST_MESSAGE("Sigmoid (using std) on " << N << " random real numbers took " << timer_.format(5, "%t") << " seconds.");

  timer_.start();
  for (auto i = 0u; i < N; ++i)
    sigmoid_table_sum += sigmoid_table(k[i]);
  timer_.stop();
  const auto sigmoid_table_elapsed(timer_.elapsed());
  const double multiplier = static_cast<double>((stdexp_elapsed.system + stdexp_elapsed.user)) / (sigmoid_table_elapsed.system + sigmoid_table_elapsed.user);
  BOOST_TEST_MESSAGE("sigmoid_table on " << N << " random real numbers took " << timer_.format(5, "%t") << " seconds (" << std::setprecision(2) << multiplier << "x of std::exp).");

  BOOST_REQUIRE_CLOSE(stdsigmoid, sigmoid_table_sum, rel_tolerance);
}

BOOST_AUTO_TEST_SUITE_END();
/// @endcond
