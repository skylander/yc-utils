/**
 * @file
 * @brief Suite of C++ functions for working with collections.
 * @details Major helper functions include:
 *   - overloaded \p operator<< capabilities for STL containers (except \p std::array)
 *   - conversion of the above to \p std::string with some confugurable display options
 */
#pragma once
#ifndef YCUTILS_COLLECTIONS_H_INCLUDED
#define YCUTILS_COLLECTIONS_H_INCLUDED

#include <ostream>
#include <sstream>
#include <string>

#include <array>
#include <deque>
#include <forward_list>
#include <functional>
#include <list>
#include <map>
#include <queue>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>

#include <boost/range/irange.hpp>

#include "ycutils.h"

namespace ycutils
{
  namespace collections {
    /// Default delimiter between consecutive elements of collection during printing.
    static const char *print_delimiter = ", ";

    /// Default string to print before printing contents of a collection.
    static const char *print_prefix = "[";

    /// Default string to print after printing contents of a collection.
    static const char *print_suffix = "]";

    /// Default string to print before printing contents of a collection.
    static const char *print_unordered_prefix = "{";

    /// Default string to print after printing contents of a collection.
    static const char *print_unordered_suffix = "}";

    /// Delimiter between \p first and \p second item of a \p std::pair.
    static const char *pair_separator = ":";
  }  // namespace collections

  using std::string;

  /**
   * @brief Helper function to print an iterable object to \p std::ostream.
   * @param os \p std::ostream to write to
   * @param L iterable to print
   * @param prefix print before contents of collection, defaults to \p collections::print_prefix.
   * @param suffix print after contents of collection, defaults to \p collections::print_suffix.
   * @param delimiter print between consecutive elements of collection, defaults to \p collections::print_delimiter.
   */
  template <typename Collection>
  std::ostream& print_collection(std::ostream& os, const Collection& L, const string& prefix = collections::print_prefix, const string& suffix = collections::print_suffix, const string& delimiter = collections::print_delimiter)
  {
    os << prefix;
    if (std::begin(L) == std::end(L))  // empty list
      return os << suffix;

    auto it = L.begin();
    os << *it++;

    for (; it != L.end(); ++it)
      os << delimiter << *it;

    return os << suffix;
  }

  /**
   * @brief Helper function to print elements in \p [first, last) to \p std::ostream.
   * @param os \p std::ostream to write to
   * @param first iterator to start printing from
   * @param last iterator to finish printing at (will not be printed!)
   * @param prefix print before contents of collection, defaults to \p collections::print_prefix.
   * @param suffix print after contents of collection, defaults to \p collections::print_suffix.
   * @param delimiter print between consecutive elements of collection, defaults to \p collections::print_delimiter.
   */
  template <typename Iterator>
  std::ostream& print_collection(std::ostream& os, Iterator first, Iterator last, const string& prefix = collections::print_prefix, const string& suffix = collections::print_suffix, const string& delimiter = collections::print_delimiter)
  {
    os << prefix;
    if (first == last)  // empty list
      return os << suffix;

    auto it = first;
    os << *it++;

    for (; it != last; ++it)
      os << delimiter << *it;

    return os << suffix;
  }

  /**
   * @brief Helper function to convert an iterable object to \p std::string.
   * @param C iterable to print
   * @param prefix print before contents of collection, defaults to \p collections::print_prefix.
   * @param suffix print after contents of collection, defaults to \p collections::print_suffix.
   * @param delimiter print between consecutive elements of collection, defaults to \p collections::print_delimiter.
   * @see ycutils::print_collection
   */
  template <typename Collection>
  string collection_to_string(const Collection& C, const string& prefix = collections::print_prefix, const string& suffix = collections::print_suffix, const string& delimiter = collections::print_delimiter)
  {
    std::ostringstream oss;
    print_collection(oss, C, prefix, suffix, delimiter);

    return oss.str();
  }

  /**
   * @brief Helper function to output items between [first, last) to a \p std::string.
   * @param first iterator to start from
   * @param last last items + 1
   * @param prefix print before contents of collection, defaults to \p collections::print_prefix.
   * @param suffix print after contents of collection, defaults to \p collections::print_suffix.
   * @param delimiter print between consecutive elements of collection, defaults to \p collections::print_delimiter.
   * @see ycutils::print_collection
   */
  template <typename Iterator>
  string collection_to_string(Iterator first, Iterator last, const string& prefix = collections::print_prefix, const string& suffix = collections::print_suffix, const string& delimiter = collections::print_delimiter)
  {
    std::ostringstream oss;
    print_collection(oss, first, last, prefix, suffix, delimiter);

    return oss.str();
  }

  /**
   * @brief Utility function for iterating integers from \p 0 to \p last.
   *
   * @param last the last integer (not inclusive) to iterate to
   * @returns an iterable using \p boost::integer_range
   */
  template <typename Integer = size_type>
  boost::integer_range<Integer> irange(Integer last) { return boost::integer_range<Integer>(0, last); }

  /**
   * @brief Type alias for priority queues, using \p std::vector as the container type.
   */
  template <typename T, typename Container = std::vector<T>, typename CompareFunc = std::less<T> >
  using priority_queue = std::priority_queue<T, Container, CompareFunc>;

  /**
   * @brief A comparator class for pairs.
   * @tparam Pair Pair type.
   * @tparam first=true \p True if we are comparing by \p std::pair::first.
   * @tparam ascending=true \p True if we want ascending order comparisons.
   */
  template <typename Pair, bool first = true, bool ascending = true>
  class pair_comparator
  {
  public:
    /**
     * @brief Overloaded function operator for comparing two pairs.
     * @return \p True if \p lhs &lt; \p rhs (or &gt; if \p ascending is \p false).
     */
    bool operator()(const Pair& lhs, const Pair& rhs) const
    {
      if (ascending)
        return first ? (lhs.first < rhs.first) : (lhs.second < rhs.second);
      return first ? (lhs.first > rhs.first) : (lhs.second > rhs.second);
    }
  };
}  // namespace ycutils

namespace std
{
  /**
   * @brief \p operator<< overload for displaying \p std::pair.
   */
  template <typename First, typename Second>
  ostream& operator<<(ostream& os, const pair<First, Second>& p) { return os << p.first << ycutils::collections::pair_separator << p.second; }

  /**
   * @brief \p operator<< overload for displaying \p std::vector.
   */
  template <typename T>
  ostream& operator<<(ostream& os, const vector<T>& C) { return ycutils::print_collection(os, C); }

  /**
   * @brief \p operator<< overload for displaying \p std::deque.
   */
  template <typename T>
  ostream& operator<<(ostream& os, const deque<T>& C) { return ycutils::print_collection(os, C); }

  /**
   * @brief \p operator<< overload for displaying \p std::forward_list.
   */
  template <typename T>
  ostream& operator<<(ostream& os, const forward_list<T>& C) { return ycutils::print_collection(os, C); }

  /**
   * @brief \p operator<< overload for displaying \p std::list.
   */
  template <typename T>
  ostream& operator<<(ostream& os, const list<T>& C) { return ycutils::print_collection(os, C); }

  /**
   * @brief \p operator<< overload for displaying \p std::map.
   */
  template <typename Key, typename Value>
  ostream& operator<<(ostream& os, const map<Key, Value>& C) { return ycutils::print_collection(os, C, ycutils::collections::print_unordered_prefix, ycutils::collections::print_unordered_suffix); }

  /**
   * @brief \p operator<< overload for displaying \p std::set.
   */
  template <typename Key>
  ostream& operator<<(ostream& os, const set<Key>& C) { return ycutils::print_collection(os, C, ycutils::collections::print_unordered_prefix, ycutils::collections::print_unordered_suffix); }

  /**
   * @brief \p operator<< overload for displaying \p std::unordered_map.
   */
  template <typename Key, typename Value>
  ostream& operator<<(ostream& os, const unordered_map<Key, Value>& C) { return ycutils::print_collection(os, C, ycutils::collections::print_unordered_prefix, ycutils::collections::print_unordered_suffix); }

  /**
   * @brief \p operator<< overload for displaying \p std::unordered_set.
   */
  template <typename Key>
  ostream& operator<<(ostream& os, const unordered_set<Key>& C) { return ycutils::print_collection(os, C, ycutils::collections::print_unordered_prefix, ycutils::collections::print_unordered_suffix); }

}  // namespace std

#endif  // YCUTILS_COLLECTIONS_H_INCLUDED
