/**
 * @file
 * @brief Utility for rapidly shuffling lines from a large file.
 *
 * Implementation is based off the streaming inside-out version of Fisher-Yates shuffling algorithm detailed in <a href="http://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle">http://en.wikipedia.org/wiki/Fisher–Yates_shuffle</a>.
 *
 * @verbatim
Usage: shufflelines < input.txt > output.txt

Shuffle lines given in STDIN and output to STDOUT.

Allowed options:
  -h [ --help ]         produce help message
@endverbatim
@cond
 */
#include <cstdlib>
#include <ctime>

#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include <boost/program_options.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_int_distribution.hpp>
#include <boost/random/variate_generator.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/timer/timer.hpp>

#include <ycutils/ioutils.h>
#include <ycutils/program_options.h>

namespace po = boost::program_options;

using std::cerr;  // usings for iostream
using std::cin;
using std::cout;
using std::endl;

using std::string;  // using for STL types
using std::vector;

void sample_lines(std::istream&, const vector<string>&, const vector<double>&);

int main(int argc, const char **argv)
{
  namespace po = boost::program_options;

  po::options_description app_options("Usage: " + std::string(argv[0]) + " < input.txt > output.txt\n\nShuffle lines given in STDIN and output to STDOUT.\n\nAllowed options");

  vector<string> output_filenames;

  app_options.add_options()
    ("help,h", "produce help message");

  po::variables_map vm;
  vector<double> sampling_rates;

  try
  {
    po::store(po::command_line_parser(argc, argv).options(app_options).run(), vm);
    po::notify(vm);

    if (vm.count("help"))
    {
      cerr << app_options << "\n";
      return 1;
    }
  }
  catch (std::exception& e)
  {
    cerr << e.what() << endl << "Type " << argv[0] << " --help for usage information." << endl;
    return EXIT_FAILURE;
  }

  boost::timer::cpu_timer timer_;
  boost::random::mt19937 rng(static_cast<unsigned int>(std::time(0)));

  vector<string> shuffled_lines;
  shuffled_lines.reserve(ycutils::ioutils::expected_num_lines);
  cerr << "Reading lines from STDIN...";
  for (std::string line; std::getline(cin, line);)
  {
    boost::random::uniform_int_distribution<ycutils::size_type> unif_dist(0, shuffled_lines.size());
    auto j = unif_dist(rng);
    if (j == shuffled_lines.size())
      shuffled_lines.push_back(std::move(line));
    else
    {
      shuffled_lines.push_back(std::move(shuffled_lines[j]));
      shuffled_lines[j] = std::move(line);
    }

    ycutils::report_progress(shuffled_lines.size(), cerr);
  }
  cerr << " done reading and shuffling " << shuffled_lines.size() << " lines (took " << timer_.format(2, "%t") << " seconds)." << endl;

  for (auto line : shuffled_lines)
    cout << line << endl;

  return EXIT_SUCCESS;
}
/// @endcond
