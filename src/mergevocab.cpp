/**
 * @file
 * @brief App for merging multiple vocabulary files created by \ref buildvocab.cpp "buildvocab".
 *
 * @details Merge operations are akin to set operations on the types in the vocabulary. Possible operations are: x (intersection), + (union), - (difference) and / (symmetric difference), which are performed from left to right.
 *
 * @note When two vocabularies are merged, the types of the final vocabulary would be the result of the set operation, while the counts of the remaining tokens are the simple sums from both vocabulary. For difference and symmetric difference operations, the counts of the <em>left hand side</em> term do not change.
 *
 * @verbatim
Usage: mergevocab [options] vocab1 op1 vocab2 op2 vocab3 ... -o merged

Merge vocabulary files according to set operations specified on the command line (op1, op2, ...).
Valid operations are: x (intersection), + (union), - (difference) and / (symmetric difference). Operations are performed from left to right.

Allowed options:
  -h [ --help ]                         produce help message
  -i [ --input-files ] vocab_file | op ...
                                        an alternating series of vocabulary
                                        files and operators that define the set
                                        operations to perform.
  -o [ --output-file ] merged_vocab (=STDOUT)
                                        file to write merged vocabulary

Other options:
  --sort [freq | lex | none] (=freq)    sort vocabulary by (freq)uency or
                                        (lex)icographical order
  -u [ --with-unknown ]                 include an unknown symbol in merged
                                        vocabulary if there isn't one
  -U [ --without-unknown ]              remove unknown symbol from merged
                                        vocabulary
@endverbatim
@cond
 */
#include <cstdlib>

#include <iostream>
#include <limits>
#include <string>
#include <utility>
#include <vector>

#include <boost/algorithm/string.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/timer/timer.hpp>

#include <ycutils/program_options.h>
#include <ycutils/vocabulary.h>

using std::cerr;
using std::endl;

using std::begin;
using std::end;
using std::make_pair;

using std::string;
using std::vector;
using std::pair;

static const string set_operators_ = "x+-/";
static const std::unordered_map<string, string> operator_names_ = {{"+", "union"}, {"x", "intersection"}, {"-", "difference"}, {"/", "symmetric difference"}};

int main(int argc, const char **argv)
{
  namespace po = boost::program_options;

  po::options_description app_options(string("Usage: ") + argv[0] + " [options] vocab1 op1 vocab2 op2 vocab3 ... -o merged\n\nMerge vocabulary files according to set operations specified on the command line (op1, op2, ...).\nValid operations are: x (intersection), + (union), - (difference) and / (symmetric difference). Operations are performed from left to right.\n\nAllowed options");

  vector<string> operation_tokens;
  app_options.add_options()
    ("help,h", "produce help message")
    ("input-files,i", po::value<vector<string> >(&operation_tokens)->required()->multitoken()->value_name("vocab_file | op ..."), "an alternating series of vocabulary files and operators that define the set operations to perform.")
    ("output-file,o", po::value<string>()->value_name("merged_vocab")->default_value("", "STDOUT"), "file to write merged vocabulary");

  po::options_description other_options("Other options");
  other_options.add_options()
    ("sort", po::value<string>()->default_value(ycutils::vocabulary::sort_order[0])->value_name(string("[") + boost::algorithm::join(ycutils::vocabulary::sort_order, " | ") + "]"), "sort vocabulary by (freq)uency or (lex)icographical order")
    ("with-unknown,u", "include an unknown symbol in merged vocabulary if there isn't one")
    ("without-unknown,U", "remove unknown symbol from merged vocabulary");
  app_options.add(other_options);

  po::positional_options_description pos_options;
  pos_options.add("input-files", -1);

  po::variables_map vm;

  try
  {
    po::store(po::command_line_parser(argc, argv).options(app_options).positional(pos_options).run(), vm);
    if (vm.count("help"))
    {
      cerr << app_options << "\n";
      return 1;
    }

    po::notify(vm);

    if (boost::find(ycutils::vocabulary::sort_order, vm["sort"].as<string>()) == ycutils::vocabulary::sort_order.end())
      throw std::logic_error(string("the argument for '--sort' is invalid. it should be one of ") + boost::algorithm::join(ycutils::vocabulary::sort_order | boost::adaptors::transformed([](const string& s) { return string("'") + s + "'"; }), ", ") + ".\n");

    if (vm.count("with-unknown") > 0 and vm.count("without-unknown") > 0)
      throw std::logic_error("'--with-unknown' and '--without-unknown' are mutually exclusive arguments.");

    if (operation_tokens.size() == 1)
      throw std::logic_error("you need at least 2 vocabulary for merging.");
    if (operation_tokens.size() % 2 == 0)
      throw std::logic_error("the merge operation does not seem to valid (maybe a missing operator?)");

    for (const auto& i : operation_tokens | boost::adaptors::indexed())
    {
      if (i.index() % 2 == 0 and not boost::filesystem::exists(i.value()))
        throw std::logic_error(i.value() + " does not exist.");
      else if (i.index() % 2 == 1 and not (i.value().size() == 1 and set_operators_.find(i.value()[0]) != string::npos))
        throw std::logic_error(i.value() + " is not a valid operator.");
    }
  }
  catch (std::exception &e)
  {
    cerr << e.what() << endl << "Type " << argv[0] << " --help for usage information." << endl;
    return EXIT_FAILURE;
  }

  boost::timer::auto_cpu_timer timer(cerr, 2, "Took %w seconds.\n");

  auto it = begin(operation_tokens);
  cerr << "Loading vocabulary from '" << *it << "'...";
  ycutils::vocabulary V_final;
  V_final.load(*it);
  cerr << V_final.size() << " types" << endl;

  auto cur_op = *(++it);  // the 0th operation is union with an empty vocabulary

  for (++it; ; ++it)
  {
    cerr << "Loading vocabulary from '" << *it << "'...";
    ycutils::vocabulary V_cur;
    V_cur.load(*it);
    cerr << "there are " << V_cur.size() << " types." << endl;

    cerr << "Performing " << operator_names_.at(cur_op) << " (" << cur_op << ") with \"" << *it << "\"..." << endl;

    if (cur_op == "+")
      V_final += V_cur;
    else if (cur_op == "x")
      V_final &= V_cur;
    else if (cur_op == "-")
      V_final -= V_cur;
    else if (cur_op == "/")
      V_final /= V_cur;

    if (++it == end(operation_tokens)) break;  // advance by one to get the next operator
    cur_op = *it;  // set the operator
  }

  if (vm.count("with-unknown"))
    V_final.add_type(V_final.unknown_symbol, 1, 1);
  else if (vm.count("without-unknown") and V_final.has_type(V_final.unknown_symbol))
    V_final.remove_type(V_final.unknown_symbol);

  cerr << "Rebuilding merged vocabulary...";
  V_final.rebuild(vm["sort"].as<string>(), false);
  cerr << "done!" << endl;
  cerr << "There are " << V_final.size() << " types in the merged vocabulary." << endl;

  if (vm["output-file"].as<string>().empty())
  {
    cerr << "Saving merged vocabulary to STDOUT...";
    V_final.save(std::cout);
  }
  else
  {
    cerr << "Saving merged vocabulary to '" << vm["output-file"].as<string>() << "'...";
    V_final.save(vm["output-file"].as<string>());
  }
  cerr << "done!" << endl;

  return EXIT_SUCCESS;
}
/// @endcond
