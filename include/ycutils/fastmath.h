/**
 * @file
 * @brief A wrapper of exponentials and logarithm functions using high performance math libraries.
 * @details Supported math libraries are
 *   - <a href="http://developer.amd.com/tools-and-sdks/cpu-development/libm/">AMD LibM</a> is a software library containing a collection of basic math functions optimized for x86-64 processor based machines. See \ref USE_AMDLIBM.
 */
#ifndef YCUTILS_FASTMATH_H_INCLUDED
#define YCUTILS_FASTMATH_H_INCLUDED

#include <cmath>

#include <limits>
#include <stdexcept>
#include <typeinfo>
#include <vector>

#include "ycutils.h"

using ycutils::size_type;

#ifdef DOXYGEN
  /**
   * @file amdlibm.h
   * @brief Header file for <a href="http://developer.amd.com/tools-and-sdks/cpu-development/libm/">AMD LibM</a> library.
   * @see \ref USE_AMDLIBM
   */

  /**
   * @brief Use <a href="http://developer.amd.com/tools-and-sdks/cpu-development/libm/">AMD LibM</a> library.
   * @details <a href="http://developer.amd.com/tools-and-sdks/cpu-development/libm/">AMD LibM</a> is a software library containing a collection of basic math functions optimized for x86-64 processor based machines. The static version of the library, and header files are included with \p ycutils.
   */
  #define USE_AMDLIBM
#else
  // #define USE_AMDLIBM
#endif

/// @cond
#ifdef USE_AMDLIBM
#include <amdlibm.h>

  #define CALL_AMDLIBM(s) \
    do \
    { \
      s; \
    } while (false)

  #define CALL_STDLIBM(s)
#else
  #define CALL_AMDLIBM(s)
  #define CALL_STDLIBM(s) \
    do \
    { \
      s; \
    } while (false)
#endif  // USE_AMDLIBM
/// @endcond

namespace ycutils {

/// Computes \f$ \exp(x) \f$.
inline double fastexp(const double x)
{
  CALL_AMDLIBM(return amd_exp(x));
  CALL_STDLIBM(return std::exp(x));
}

/// Computes \f$ \exp(x) \f$.
inline float fastexp(const float x)
{
  CALL_AMDLIBM(return amd_expf(x));
  CALL_STDLIBM(return std::exp(x));
}

/// Computes \f$ \exp(\mathbf v) \f$ for a vector of doubles \p vec. This uses SIMD to process the operations in parallel (and thus faster!).
inline void fastexp(double *vec, const size_type num)
{
  CALL_AMDLIBM(amd_vrda_exp(num, vec, vec));
  CALL_STDLIBM(for (index_type i = 0; i < num; ++i) vec[i] = std::exp(vec[i]));
}

/// Computes \f$ \exp(\mathbf v) \f$ for a vector of floats \p vec. This uses SIMD to process the operations in parallel (and thus faster!).
inline void fastexp(float *vec, const size_type num)
{
  CALL_AMDLIBM(amd_vrsa_expf(num, vec, vec));
  CALL_STDLIBM(for (index_type i = 0; i < num; ++i) vec[i] = std::exp(vec[i]));
}

/// Computes \f$ \log(x) \f$.
inline double fastlog(const double x)
{
  CALL_AMDLIBM(return amd_log(x));
  CALL_STDLIBM(return std::log(x));
}

/// Computes \f$ \log(x) \f$.
inline float fastlog(const float x)
{
  CALL_AMDLIBM(return amd_logf(x));
  CALL_STDLIBM(return std::log(x));
}

/// Computes \f$ \exp(\mathbf v) \f$ for a vector of doubles \p vec. This uses SIMD to process the operations in parallel (and thus faster!).
inline void fastlog(double *vec, const size_type num)
{
  // for loops seems to be faster than the amd version
  for (index_type i = 0; i < num; ++i)
    vec[i] = fastlog(vec[i]);
}

/// Computes \f$ \exp(\mathbf v) \f$ for a vector of floats \p vec. This uses SIMD to process the operations in parallel (and thus faster!).
inline void fastlog(float *vec, const size_type num)
{
  // for loops seems to be faster than the amd version
  for (index_type i = 0; i < num; ++i)
    vec[i] = fastlog(vec[i]);
}

/// Computes the log sigmoid function with double precision i.e \f$ x - \log (1 + \exp(x)) \f$.
inline double logsigmoid(const double x)
{
  CALL_AMDLIBM(return x - amd_log1p(x));
  CALL_STDLIBM(return x - std::log1p(x));
}

/// Computes the log sigmoid function with single precision i.e \f$ x - \log (1 + \exp(x)) \f$.
inline float logsigmoid(const float x)
{
  CALL_AMDLIBM(return x - amd_log1pf(x));
  CALL_STDLIBM(return x - std::log1p(x));
}

/// Computes the log sigmoid function for a vector of doubles i.e \f$ \bf v_i - \log (\sum_j \exp(\bf v_j)) \f$.
inline void logsigmoid(double *vec, const size_type num)
{
  double A = -std::numeric_limits<double>::max();
  double orig_vec[num];

  for (index_type i = 0; i < num; ++i)
    if (vec[i] > A)
      A = vec[i];
  for (index_type i = 0; i < num; ++i)
    orig_vec[i] = vec[i] - A;

  fastexp(orig_vec, num);

  double exp_sum = 0.0;
  for (index_type i = 0; i < num; ++i)
    exp_sum += orig_vec[i];

  const auto log_Z = fastlog(exp_sum);

  for (index_type i = 0; i < num; ++i)
    vec[i] -= A + log_Z;
}

/// Computes the log sigmoid function for a vector of floats i.e \f$ \bf v_i - \log (\sum_j \exp(\bf v_j)) \f$.
inline void logsigmoid(float *vec, const size_type num)
{
  float A = -std::numeric_limits<float>::max();
  float orig_vec[num];

  for (index_type i = 0; i < num; ++i)
    if (vec[i] > A)
      A = vec[i];
  for (index_type i = 0; i < num; ++i)
    orig_vec[i] = vec[i] - A;

  fastexp(orig_vec, num);

  float exp_sum = 0.0;
  for (index_type i = 0; i < num; ++i)
    exp_sum += orig_vec[i];

  const auto log_Z = fastlog(exp_sum);

  for (index_type i = 0; i < num; ++i)
    vec[i] -= A + log_Z;
}

/// Computes the sigmoid function with double precision i.e \f$ \frac{\exp(x)}{1 + \exp(x)} \f$.
inline double sigmoid(const double x) { return fastexp(logsigmoid(x)); }

/// Computes the sigmoid function with single precision i.e \f$ \frac{\exp(x)}{1 + \exp(x)} \f$.
inline float sigmoid(const float x) { return fastexp(logsigmoid(x)); }

/// Computes the sigmoid function with double precision for a vector: i.e \f$ \frac{\exp(v_i)}{\sum_j \exp(v_j)} \f$.
inline void sigmoid(double *vec, const size_type num)
{
  double A = -std::numeric_limits<double>::max();
  double orig_vec[num];
  index_type i;

  for (i = 0; i < num; ++i)
    if (vec[i] > A)
      A = vec[i];

  for (i = 0; i < num; ++i)
    orig_vec[i] = vec[i] - A;

  fastexp(orig_vec, num);

  double exp_sum = 0.0;
  for (i = 0; i < num; ++i)
    exp_sum += orig_vec[i];

  // const auto log_Z = fastlog(exp_sum);

  for (i = 0; i < num; ++i)
    vec[i] = orig_vec[i] / exp_sum;
    // vec[i] = fastexp(vec[i] - A - log_Z);
}

/// Computes the sigmoid function with single precision for a vector: i.e \f$ \frac{\exp(v_i)}{\sum_j \exp(v_j)} \f$.
inline void sigmoid(float *vec, const size_type num)
{
  float A = -std::numeric_limits<float>::max();
  float orig_vec[num];
  index_type i;

  for (i = 0; i < num; ++i)
    if (vec[i] > A)
      A = vec[i];

  for (i = 0; i < num; ++i)
    orig_vec[i] = vec[i] - A;

  fastexp(orig_vec, num);

  float exp_sum = 0.0;
  for (i = 0; i < num; ++i)
    exp_sum += orig_vec[i];

  // const auto log_Z = fastlog(exp_sum);

  for (i = 0; i < num; ++i)
    vec[i] = orig_vec[i] / exp_sum;
    // vec[i] = fastexp(vec[i] - A - log_Z);
}

}  // namespace ycutils

#endif  // YCUTILS_FASTMATH_H_INCLUDED
