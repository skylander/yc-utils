/**
 * @file
 * @brief Utility methods for dealing with \p boost::program_options.
 *
 * This header contains:
 * - Validation for commonly used argument types: \ref ycutils::input_file_argument "input files", \ref ycutils::input_dir_argument "directories", \ref ycutils::positive_argument "numerical arguments", and more.
 * - \p ycutils::at_option_parser -- a class for implementing <a href="http://www.boost.org/doc/libs/1_57_0/doc/html/program_options/howto.html">response files</a>.
 *
 * @see program_options.cpp
 */
#pragma once
#ifndef YCUTILS_PROGRAM_OPTIONS_H_INCLUDED
#define YCUTILS_PROGRAM_OPTIONS_H_INCLUDED

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <utility>

#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/adaptor/transformed.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/tokenizer.hpp>

#include "ycutils.h"

namespace ycutils {
  namespace fs = boost::filesystem;
  namespace po = boost::program_options;

  using std::string;

  /**
   * @brief This class is used to create an option parser that interpretes options starting with the \p @ symbol as reading additional arguments from the file specified (see <a href="http://www.boost.org/doc/libs/1_57_0/doc/html/program_options/howto.html">\em "Response file" section in Boost program options how-to</a>).
   * @details To use it, you need to first define an option,
   * @code{.cpp}
   * (ycutils::at_option_parser::default_key, po::value<ycutils::input_file_argument>(), "can be specified with '@name', too")
   * @endcode
   * Next, you can add the parser to the command line parser using \p extra_parser(ycutils::at_option_parser()).
   * The following snippet uses the \p ycutils::read_arguments_from_file helper function to load arguments from the file specified, and pass it on for further parsing.
   * @code{.cpp}
   * if (vm.count(ycutils::at_option_parser::default_key))
   * {
   *  std::vector<string> args;
   *  ycutils::read_arguments_from_file(vm[ycutils::at_option_parser::default_key].as<ycutils::input_file_argument>().path(), std::back_inserter(args));
   *  po::store(po::command_line_parser(args).options(app_options).positional(pos_options).run(), vm);
   * }
   * @endcode
   * @see <a href="http://www.boost.org/doc/libs/1_57_0/doc/html/program_options/howto.html">Boost program options how-to</a>, ycutils::read_arguments_from_file
   */
  class at_option_parser
  {
  public:
    /// Default option name.
    static const string default_name;

    /// Default prefix symbol for option.
    static const string default_prefix;

    /// Creates an \p ycutils::at_option_parser with default name and prefix symbol.
    at_option_parser() : _name(default_name), _prefix(default_prefix) {}

    /**
     * @brief Creates an \p ycutils::at_option_parser with specified name and default prefix symbol.
     * @param name option name
     */
    at_option_parser(const string& name) : _name(name), _prefix(default_prefix) {}

    /**
     * @brief Creates an \p ycutils::at_option_parser with specified name and prefix symbol.
     * @param name option name
     * @param prefix prefix symbol
     */
    at_option_parser(const string& name, const string& prefix) : _name(name), _prefix(prefix) {}

    /// @brief Overloaded function call operator for parsing option.
    std::pair<string, string> operator()(const string &s)
    {
      if (boost::starts_with(s, _prefix))
        return std::make_pair(_name, s.substr(_prefix.length()));
      return std::pair<string, string>();
    }

  private:
    /// Option name
    string _name;

    /// Prefix symbol
    string _prefix;
  };

  /// Base class for all \p ycutils custom command line arguments.
  template <typename T>
  class base_argument
  {
  public:
    /// Default empty constructor.
    base_argument() {}

    /// Constructs a \p base_argument by setting its \p value.
    base_argument(const T& value) : value_(value) {}

    /// \p value() function returns the actual value of the argument.
    const T& value() const { return value_; }

    /// Overloads the operator for casting the argument.
    template <typename U>
    operator U() const { return static_cast<U>(value_); }

    /// @see ycutils::operator<<(std::ostream& os, const base_argument<U>& arg)
    template <typename U>
    friend std::ostream& operator <<(std::ostream&, const base_argument<U>&);

  private:
    /// Argument value.
    T value_;
  };

  /**
   * @brief Overloads the \p << operator to facilitate \p boost::lexical_cast operations.
   * @param os \p std::ostream
   * @param arg \p ycutils::base_argument<U> object
   */
  template <typename U>
  inline std::ostream& operator<<(std::ostream& os, const base_argument<U>& arg)
  {
    os << arg.value();
    return os;
  }

  /**
   * @brief Custom \p boost::program_options argument type for input file arguments.
   * @details Checks file path to ensure that it exists and is readable.
   * Validation function is implemented in ::validate.
   */
  class input_file_argument : public base_argument<string>
  {
  public:
    input_file_argument() {}

    /**
     * @brief Constructs a \p input_file_argument with \p filename.
     * @param filename filename specified in command line, which will be checked for existence and readability.
     */
    input_file_argument(const string& filename) : base_argument<string>(filename) {}

    /// Returns the argument path as a \p boost::filesystem::path object.
    const fs::path path() const { return fs::path(value()); }
  };

  /**
   * @brief Custom \p boost::program_options argument type for input directory arguments.
   * @details Checks directory path to ensure that it exists.
   * Validation function is implemented in ::validate.
   */
  class input_dir_argument : public base_argument<string>
  {
  public:
    input_dir_argument() {}

    /**
     * @brief Constructs a \p input_dir_argument with \p dirname.
     * @param dirname directory name specified in command line, which will be checked for existence.
     */
    input_dir_argument(const string& dirname) : base_argument<string>(dirname) {}

    /// Returns the argument path as a \p boost::filesystem::path object.
    const fs::path path() const { return fs::path(value()); }
  };

  /**
   * @brief Custom \p boost::program_options argument type for output directory arguments.
   * @details Checks directory path to see if directory exists.
   * If directory does not exist, it will be created. If its parent directory do not exist, an exception will be thrown.
   * Validation function is implemented in ::validate.
   */
  class output_dir_argument : public base_argument<string>
  {
  public:
    output_dir_argument() {}

    /**
     * @brief Constructs a \p output_dir_argument with \p dirname.
     * @param dirname directory name specified in command line.
     */
    output_dir_argument(const string& dirname) : base_argument<string>(dirname) {}

    /// Returns the argument path as a \p boost::filesystem::path object.
    const fs::path path() const { return fs::path(value()); }
  };

  /**
   * @brief Custom \p boost::program_options argument type for positive valued arguments (i.e > 0).
   * @details Checks value to ensure that it is positive. Validation function is implemented in ::validate.
   * @tparam T underlying numerical (or numerically behaved) type
   */
  template <typename T>
  class positive_argument : public base_argument<T>
  {
  public:
    positive_argument() {}

    /**
     * @brief Constructs a \p positive_argument with \p value.
     * @param value argument value (converted from \p string using \p boost::lexical_cast<T>)
     */
    positive_argument(T value) : base_argument<T>(value) {}
  };

  /**
   * @brief Custom \p boost::program_options argument type for nonnegative valued arguments (i.e >= 0).
   * @details Checks value to ensure that it is non-negative.
   * Validation function is implemented in ::validate.
   * @tparam T underlying numerical (or numerically behaved) type
   */
  template <typename T>
  class nonnegative_argument : public base_argument<T>
  {
  public:
    nonnegative_argument() {}

    /**
     * @brief Constructs a \p nonnegative_argument with \p value.
     * @param value argument value (converted from \p string using \p boost::lexical_cast<T>)
     */
    nonnegative_argument(T value) : base_argument<T>(value) {}
  };

  /**
   * @brief Custom \p boost::program_options argument type for fixed size tuples.
   * @details This is useful for arguments where the number of arguments is fixed and limited.
   * Validation function is implemented in ::validate. Underlying storage is a \p std::vector.
   * @tparam T underlying argument type
   * @tparam N the number of arguments
   */
  template <typename T, size_type N>
  class fixed_count_arguments : public base_argument<std::vector<T> >
  {
  public:
    /// convenience type for vector of arguments
    using array_type = std::vector<T>;

    /// Default constructor
    fixed_count_arguments() {}

    /**
     * @brief Constructs a \p fixed_count_arguments with \p value.
     * @param values a vector of arguments as entered in the command line.
     */
    fixed_count_arguments(const array_type& values) : base_argument<array_type>(values) {}

    /**
     * @brief Helper function to get a specific argument.
     * @param i
     * @returns the \p i th argument
     */
     const T& get(int i) const { return this->value()[i]; }
  };

  /**
   * @brief Overloads the \p << operator to facilitate \p boost::lexical_cast operations.
   * @param os \p std::ostream
   * @param arg \p ycutils::input_file_arguments object
   */
  template <typename T, size_type N>
  inline std::ostream& operator<<(std::ostream &os, const fixed_count_arguments<T, N>& arg)
  {
    if (arg.value().empty()) return os;
    std::copy(arg.value().begin(), arg.value().end() - 1, std::ostream_iterator<T>(os, " "));
    os << arg.value().back();
    return os;
  }

  /// Alias for vector of \p input_file_argument.
  using input_file_arguments = std::vector<input_file_argument>;

  /**
   * @brief Overloads the \p << operator to facilitate \p boost::lexical_cast operations.
   * @param os \p std::ostream
   * @param args \p ycutils::input_file_arguments object
   */
  inline std::ostream& operator<<(std::ostream &os, const input_file_arguments& args)
  {
    if (args.empty()) return os;
    std::copy(args.begin(), args.end() - 1, std::ostream_iterator<input_file_argument>(os, " "));
    os << args.back();
    return os;
  }

  /// Alias for vector of \p positive_argument with underlying type \p T.
  template <typename T>
  using positive_arguments = std::vector<positive_argument<T> >;

  /**
   * @brief Overloads the \p << operator to facilitate \p boost::lexical_cast operations.
   * @param os \p std::ostream
   * @param args \p ycutils::positive_arguments<T> object
   * @tparam T underlying argument type
   */
  template <typename T>
  inline std::ostream& operator<<(std::ostream &os, const positive_arguments<T>& args)
  {
    if (args.empty()) return os;
    std::copy(args.begin(), args.end() - 1, std::ostream_iterator<positive_argument<T> >(os, " "));
    os << args.back();
    return os;
  }

  /// Alias for vector of \p nonnegative_argument with underlying type \p T.
  template <typename T>
  using nonnegative_arguments = std::vector<nonnegative_argument<T> >;

  /**
   * @brief Overloads the \p << operator to facilitate \p boost::lexical_cast operations.
   * @param os \p std::ostream
   * @param args \p ycutils::nonnegative_arguments<T> object
   * @tparam T underlying argument type
   */
  template <typename T>
  inline std::ostream& operator<<(std::ostream &os, const nonnegative_arguments<T>& args)
  {
    if (args.empty()) return os;
    std::copy(args.begin(), args.end() - 1, std::ostream_iterator<nonnegative_arguments<T> >(os, " "));
    os << args.back();
    return os;
  }

  /**
   * @brief Helper function for extracting container of our custom arguments into an iterator.
   * @param args arguments as a sequence.
   * @param it argument values go here.
   */
  template <typename ArgumentList, typename OutputIterator>
  inline void extract_arguments(const ArgumentList& args, OutputIterator it)
  {
    for (const auto& a : args)
      *(it++) = a.value();
  }

  /**
   * @brief Helper function for extracting container of our custom arguments as a range using \p boost::adaptors.
   * @param args arguments as a sequence.
   * @returns a \p boost::transformed_iterator.
   */
  template <typename ArgumentList>
  inline auto extract_arguments(const ArgumentList& args) -> decltype(args | boost::adaptors::transformed(std::mem_fn(&ArgumentList::value_type::value)))
  {
    return args | boost::adaptors::transformed(std::mem_fn(&ArgumentList::value_type::value));
  }

  /// @brief Custom validator for ycutils::input_file_argument type.
  /// @details This function will be called by \p boost::program_options.
  inline void validate(boost::any& v, const std::vector<string>& values, ycutils::input_file_argument* target_type, int)
  {
    po::validators::check_first_occurrence(v);
    const string& filename = po::validators::get_single_string(values);

    std::ifstream f(filename);
    if (!f.is_open())
      throw std::runtime_error("unable to open '" + filename + "' for reading");
    f.close();

    v = boost::any(ycutils::input_file_argument(filename));
  }

  /// @brief Custom validator for ycutils::input_dir_argument type.
  /// @details This function will be called by \p boost::program_options.
  inline void validate(boost::any& v, const std::vector<string>& values, ycutils::input_dir_argument* target_type, int)
  {
    po::validators::check_first_occurrence(v);
    const string& dirname = po::validators::get_single_string(values);

    fs::path dirpath(dirname);
    if (!fs::is_directory(dirpath))
      throw std::runtime_error("unable to open directory '" + dirname + "'");

    v = boost::any(ycutils::input_dir_argument(dirname));
  }

  /// @brief Custom validator for ycutils::output_dir_argument type.
  /// @details This function will be called by \p boost::program_options.
  inline void validate(boost::any& v, const std::vector<string>& values, ycutils::output_dir_argument* target_type, int)
  {
    po::validators::check_first_occurrence(v);
    const string& dirname = po::validators::get_single_string(values);

    fs::path dirpath(dirname);
    fs::create_directory(dirpath);
    if (!fs::is_directory(dirpath))
      throw std::runtime_error("unable to create directory '" + dirname + "''.\n");

    v = boost::any(ycutils::output_dir_argument(dirname));
  }

  /// @brief Custom validator for ycutils::positive_argument type.
  /// @details This function will be called by \p boost::program_options.
  template <typename T>
  inline void validate(boost::any& v, const std::vector<string>& values, ycutils::positive_argument<T>* target_type, int)
  {
    po::validators::check_first_occurrence(v);
    const auto& value = boost::lexical_cast<T>(po::validators::get_single_string(values));

    if (value > 0)
      v = boost::any(ycutils::positive_argument<T>(value));
    else
      throw po::error_with_option_name("'%canonical_option%' must be positive");
  }

  /// @brief Custom validator for ycutils::nonnegative_argument type.
  /// @details This function will be called by \p boost::program_options.
  template <typename T>
  inline void validate(boost::any& v, const std::vector<string>& values, ycutils::nonnegative_argument<T>* target_type, int)
  {
    po::validators::check_first_occurrence(v);
    const auto& value = boost::lexical_cast<T>(po::validators::get_single_string(values));

    if (value >= 0)
      v = boost::any(ycutils::nonnegative_argument<T>(value));
    else
      throw po::error_with_option_name("'%canonical_option%' must be non-negative");
  }

  /// @brief Custom validator for ycutils::fixed_count_arguments type.
  /// @details This function will be called by \p boost::program_options.
  template <typename T, size_type N>
  inline void validate(boost::any& v, const std::vector<string>& values, ycutils::fixed_count_arguments<T, N>* target_type, int)
  {
    po::validators::check_first_occurrence(v);
    boost::char_separator<char> sep(" ");

    std::vector<T> arg_values;
    for (const auto v : values)
    {
      boost::tokenizer<boost::char_separator<char> > tokens(v, sep);
      boost::transform(tokens, std::back_inserter(arg_values), boost::lexical_cast<T, std::string>);
    }

    if (arg_values.size() != N)
      throw po::error_with_option_name(string("'%canonical_option%' takes exactly ") + boost::lexical_cast<string>(N) + " arguments (" + boost::lexical_cast<string>(values.size()) + " given)");

    v = boost::any(ycutils::fixed_count_arguments<T, N>(arg_values));
  }

  /**
   * @brief Helper function to read arguments from file.
   * @param path file path to read from
   * @param out container to store read arguments for further processing
   * @todo more robust parsing and interpreting of arguments in file: inline comments, and quotations
   */
  template <typename OutputIterator>
  inline void read_arguments_from_file(const fs::path& path, OutputIterator out)
  {
    fs::ifstream ifs(path);
    for (std::string line; std::getline(ifs, line);)
    {
      if (boost::starts_with(line, "#") || line.empty()) continue;
      std::vector<std::string> args;
      boost::split(args, line, boost::is_any_of(" "));
      boost::copy(args, out);
    }
  }

}  // namespace ycutils

#endif  // YCUTILS_PROGRAM_OPTIONS_H_INCLUDED
