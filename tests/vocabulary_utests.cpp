/**
 * @file
 * @brief Unit test code for the vocabulary header library \p ycutils/vocabulary.h.
 * @cond
 */
#include <iostream>
#include <sstream>

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE VocabularyTest
#include <boost/test/unit_test.hpp>

#include <ycutils/vocabulary.h>

BOOST_AUTO_TEST_SUITE(vocabulary_test_suite);

BOOST_AUTO_TEST_CASE(vocabulary_basic)
{
  ycutils::vocabulary V;

  V.load("sample1.vocab");

  BOOST_REQUIRE_EQUAL(V[0], "__UNK__");
  BOOST_REQUIRE_EQUAL(V[1], "the");

  BOOST_REQUIRE_EQUAL(V["the"], 1);
  BOOST_REQUIRE_EQUAL(V["brown"], 11);

  BOOST_REQUIRE_EQUAL(V.get_freq(1), 2);
  BOOST_REQUIRE_EQUAL(V.get_freq("the"), 2);

  BOOST_REQUIRE_EQUAL(V.get_doc_freq(1), 1);
  BOOST_REQUIRE_EQUAL(V.get_doc_freq("the"), 1);

  BOOST_REQUIRE_EQUAL(V.get_freq("brown"), 1);
  BOOST_REQUIRE_EQUAL(V.get_doc_freq("brown"), 1);

  BOOST_REQUIRE(V.has_type("fox"));
  BOOST_REQUIRE(!V.has_type("abc"));

  BOOST_REQUIRE(V.add_type("new", 50, 2));
  BOOST_REQUIRE(V.has_type("new"));
  BOOST_REQUIRE_EQUAL(V.get_freq("new"), 50);
  BOOST_REQUIRE_EQUAL(V.get_doc_freq("new"), 2);

  V.remove_type("new");
  V.rebuild();
  BOOST_REQUIRE(!V.has_type("new"));
}

BOOST_AUTO_TEST_CASE(vocabulary_intersection)
{
  ycutils::vocabulary V1, V2;
  V1.load("sample1.vocab");
  V2.load("sample2.vocab");

  V1 &= V2;
  V1.rebuild();
  BOOST_REQUIRE(V1.has_type("the"));
  BOOST_REQUIRE(V1.has_type("over"));
  BOOST_REQUIRE(!V1.has_type("bye"));

  V1.load("sample1.vocab");
  V1 &= V2;
  V1.add_type("quick", 1, 1);
  V1.rebuild();
  BOOST_REQUIRE(V1.has_type("quick"));
  BOOST_REQUIRE(V1.has_type("the"));
  BOOST_REQUIRE(V1.has_type("over"));
}

BOOST_AUTO_TEST_CASE(vocabulary_union)
{
  ycutils::vocabulary V1, V2;
  V1.load("sample1.vocab");
  V2.load("sample2.vocab");

  V1 += V2;
  V1.rebuild();
  BOOST_REQUIRE(V1.has_type("the"));
  BOOST_REQUIRE(V1.has_type("over"));
  BOOST_REQUIRE(V1.has_type("bye"));
}

BOOST_AUTO_TEST_CASE(vocabulary_difference)
{
  ycutils::vocabulary V1, V2;
  V1.load("sample1.vocab");
  V2.load("sample2.vocab");

  V1 -= V2;
  V1.rebuild();
  BOOST_REQUIRE(!V1.has_type("the"));
  BOOST_REQUIRE(!V1.has_type("over"));
  BOOST_REQUIRE(V1.has_type("dog"));

  V1.load("sample1.vocab");
  V1 -= V2;
  V1.add_type("the", 1, 1);
  V1.rebuild();
  BOOST_REQUIRE(V1.has_type("the"));
  BOOST_REQUIRE(V1.has_type("dog"));
  BOOST_REQUIRE(!V1.has_type("over"));
}

BOOST_AUTO_TEST_CASE(vocabulary_symmetric_difference)
{
  ycutils::vocabulary V1, V2;
  V1.load("sample1.vocab");
  V2.load("sample2.vocab");

  V1 /= V2;
  V1.rebuild();
  BOOST_REQUIRE(!V1.has_type("the"));
  BOOST_REQUIRE(!V1.has_type("over"));
  BOOST_REQUIRE(V1.has_type("dog"));
  BOOST_REQUIRE(V1.has_type("bye"));

  V1.load("sample1.vocab");
  V1 /= V2;
  V1.add_type("the", 1, 1);
  V1.rebuild();
  BOOST_REQUIRE(V1.has_type("the"));
  BOOST_REQUIRE(!V1.has_type("over"));
  BOOST_REQUIRE(V1.has_type("dog"));
  BOOST_REQUIRE(V1.has_type("bye"));
}

BOOST_AUTO_TEST_SUITE_END();
/// @endcond
