# Java stuff
find_package(Java REQUIRED)
include(UseJava)

FILE(GLOB_RECURSE JAVA_SRCS edu/*.java)
FILE(GLOB CORENLP_MODELS ${PROJECT_SOURCE_DIR}/lib/models/*)
FILE(GLOB JAVA_DEPS ${PROJECT_SOURCE_DIR}/lib/*.jar)

add_jar(yc-utils SOURCES ${JAVA_SRCS} INCLUDE_JARS ${JAVA_DEPS})

add_custom_command(OUTPUT corenlp-models.jar COMMAND ${Java_JAR_EXECUTABLE} cf ${CMAKE_CURRENT_BINARY_DIR}/corenlp-models.jar . WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}/lib/models DEPENDS ${CORENLP_MODELS})
add_custom_target(corenlp-models ALL DEPENDS corenlp-models.jar)

install_jar(yc-utils ${INSTALL_LIB_DIR})
install(FILES ${JAVA_DEPS} ${CMAKE_CURRENT_BINARY_DIR}/corenlp-models.jar DESTINATION ${INSTALL_LIB_DIR})
install(PROGRAMS ${CMAKE_CURRENT_SOURCE_DIR}/corenlpwrapper.sh DESTINATION ${INSTALL_BIN_DIR})
