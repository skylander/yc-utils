/**
 * @file
 * @brief A Java wrapper around Stanford CoreNLP for tokenizing, sentence splitting, lemmatizing, POS tagging and NER labeling.
 * @details This wrapper is designed around using STDIN and STDOUT for processing data. This makes it easily extensible and integratable with other programming languages.
 * We included a copy of Stanford CoreNLP 3.5.0 in this installation, as well as POS and NER models for English in standard (<tt>english-left3words-distsim.tagger</tt>, <tt>english.all.3class.distsim.crf.ser.gz</tt>) and caseless versions (<tt>english-caseless-left3words-distsim.tagger</tt>, <tt>english.all.3class.caseless.distsim.crf.ser.gz</tt>).
 * For convenience, these models are in the root level of \p corenlp-models.jar file in the installation's \p lib folder.
 * For other models, please visit <a href="http://nlp.stanford.edu/software/corenlp.shtml">http://nlp.stanford.edu/software/corenlp.shtml</a>.
 *
 * A bash shell script wraps around this Java code (so you do not need to specify classpaths, etc). For example,
@verbatim
$ corenlpwrapper.sh -pos english-left3words-distsim.tagger -lemma
Adding annotator tokenize
Adding annotator ssplit
edu.stanford.nlp.pipeline.AnnotatorImplementations:ssplit.isOneSentence=true
Adding annotator pos
Reading POS tagger model from english-left3words-distsim.tagger ... done [1.4 sec].
Adding annotator lemma
Hello world
hello/UH world/NN
$
@endverbatim

You can see usage instructions using the \p -h argument.
@verbatim
usage: corenlpwrapper.sh [--lemma] [--ssplit] [--pos <model>] [--ner <model>]
  -h,--help         perform sentence splitting on input
  -l,--lemma        use lemmas instead of original tokens
  -n,--ner <model>  perform NER tagging with given model
  -p,--pos <model>  perform POS tagging with given model
  -s,--ssplit       perform sentence splitting on input
  -t,--tokenize     perform tokenization on input
 * @endverbatim
 *
 * @todo Suppose for JSON communications
 */

package edu.yc.nlp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;

/// @cond
public class StanfordCoreNLPWrapper
{
  @SuppressWarnings("static-access")
  public static void main(String[] args) throws IOException
  {
    Options options = new Options();

    options.addOption("h", "help", false, "perform sentence splitting on input");
    options.addOption("s", "ssplit", false, "perform sentence splitting on input");
    options.addOption("t", "tokenize", false, "perform tokenization on input");
    options.addOption("l", "lemma", false, "use lemmas instead of original tokens");
    options.addOption(OptionBuilder.withDescription("perform POS tagging with given model").withArgName("model").hasArg().withLongOpt("pos").create('p'));
    options.addOption(OptionBuilder.withDescription("perform NER tagging with given model").withArgName("model").hasArg().withLongOpt("ner").create('n'));

    CommandLineParser parser = new GnuParser();
    Properties props = new Properties();
    CommandLine A = null;

    try
    {
      A = parser.parse(options, args);

      if (A.hasOption("help"))
      {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp(new PrintWriter(System.err, true), 80, "corenlpwrapper.sh [--lemma] [--ssplit] [--pos <model>] [--ner <model>]", "", options, 2, 2, "");
        System.exit(0);
      }

      Set<String> annotators = new HashSet<String>();

      if (A.hasOption("tokenize"))
      {
        annotators.add("tokenize");
        props.put("tokenize.language", "english");
      }
      else
        props.put("tokenize.whitespace", "true");

      if (A.hasOption("ssplit"))
      {
        annotators.add("tokenize");
        annotators.add("ssplit");
      }
      else
        props.put("ssplit.isOneSentence", "true");

      if (A.hasOption("pos"))
      {
        annotators.add("tokenize");
        annotators.add("ssplit");
        annotators.add("pos");
        props.put("pos.model", A.getOptionValue("pos"));
      }
      else
        props.put("pos.model", "english-left3words-distsim.tagger");

      if (A.hasOption("lemma"))
      {
        annotators.add("tokenize");
        annotators.add("ssplit");
        annotators.add("pos");
        annotators.add("lemma");
      }

      if (A.hasOption("ner"))
      {
        annotators.add("tokenize");
        annotators.add("ssplit");
        annotators.add("pos");
        annotators.add("lemma");
        annotators.add("ner");
        props.put("ner.model", A.getOptionValue("ner"));
      }

      if (annotators.size() == 0)
        throw new ParseException("You need to specify some arguments!");

      StringBuilder sb = new StringBuilder();
      if (annotators.contains("tokenize"))
        sb.append("tokenize,");
      if (annotators.contains("ssplit"))
        sb.append("ssplit,");
      if (annotators.contains("pos"))
        sb.append("pos,");
      if (annotators.contains("lemma"))
        sb.append("lemma,");
      if (annotators.contains("ner"))
        sb.append("ner,");

      props.put("annotators", sb.substring(0, sb.length() - 1));
    }
    catch (ParseException exp)
    {
      HelpFormatter formatter = new HelpFormatter();
      System.err.println(exp.getMessage());
      formatter.printHelp(new PrintWriter(System.err, true), 80, "corenlpwrapper.sh [--lemma] [--ssplit] [--pos <model>] [--ner <model>]", "", options, 2, 2, "");
      System.exit(-1);
    }

    StanfordCoreNLP pipeline = new StanfordCoreNLP(props);

    BufferedReader br = new BufferedReader(new InputStreamReader(System.in, "UTF8"));
    String line;
    while ((line = br.readLine()) != null)
    {
      line = line.trim();
      if (line.isEmpty())
      {
        System.out.println(line);
        continue;
      }

      Annotation document = new Annotation(line);
      pipeline.annotate(document);

      if (A.hasOption("ssplit"))
      {
        List<CoreMap> sentences = document.get(SentencesAnnotation.class);
        for (CoreMap sentence : sentences)
        {
          StringBuilder sb = new StringBuilder();
          for (CoreLabel token : sentence.get(TokensAnnotation.class))
          {
            sb.append(A.hasOption("lemma") ? token.lemma() : token.value());
            if (A.hasOption("pos"))
              sb.append("/" + token.tag());
            if (A.hasOption("ner"))
              sb.append("/" + token.ner());
            sb.append(' ');
          }
          System.out.println(sb.substring(0, sb.length() - 1));
        }
      }
      else
      {
        StringBuilder sb = new StringBuilder();
        for (CoreLabel token : document.get(TokensAnnotation.class))
        {
          sb.append(A.hasOption("lemma") ? token.lemma() : token.value());
          if (A.hasOption("pos"))
            sb.append("/" + token.tag());
          if (A.hasOption("ner"))
            sb.append("/" + token.ner());
          sb.append(' ');
        }
        System.out.println(sb.substring(0, sb.length() - 1));
      }
    }
  }
}
/// @endcond
