## @file
# @brief A simple wrapper object for interfacing with the Java application \link StanfordCoreNLPWrapper.java StanfordCoreNLPWrapper\endlink using \p subprocess.
# @details Details are in corenlpwrapper.StanfordCoreNLP
# @see corenlpwrapper.StanfordCoreNLP
# @todo make this into a full fledged wrapper?
import imp
import os
import subprocess

try:
  import splitta.sbd
except ImportError:
  (_file, _filename, _data) = imp.find_module('splitta', [os.path.join(os.path.dirname(os.path.realpath(__file__)))])
  ## Dynamically import the Splitta module
  splitta = imp.load_module('splitta', _file, _filename, _data)
  import splitta.sbd
#end try


## @brief A simple wrapper object for interfacing with the Java application \link StanfordCoreNLPWrapper.java StanfordCoreNLPWrapper\endlink.
class StanfordCoreNLP(object):

  ## Default command line options for calling Java runtime.
  _default_java_cmdline = ['java', '-Xms256m', '-Xmx2g', '-XX:+UseParallelGC', '-XX:ParallelGCThreads=2']

  ## @brief Constructor taking arguments that have the same meaning as in the Java application.
  ## @note For sentence splitting, we use the <a href="https://code.google.com/p/splitta/">Splitta</a> package instead of Stanford CoreNLP's own system. Splitta is better performing (with lower error rate), while the Stanford system is based on Penn Treebank tokenization rules.
  ## Also, there is no way for us to know how much data Stanford CoreNLP is going to return us, due to the problem with using line by line pipe communications.
  ## @see corenlpwrapper.StanfordCoreNLP
  ## @todo Use JSON for communication between processes.
  def __init__(self, ssplit=False, tokenize=False, lemma=False, pos=None, ner=None, java_args=_default_java_cmdline):
    args = []
    if tokenize: args.append('-tokenize')
    if lemma: args.append('-lemma')
    if pos: args += ['-pos', pos]
    if ner: args += ['-ner', ner]

    script_dir = os.path.dirname(os.path.realpath(__file__))

    ## Internal Splitta object
    if ssplit: self._splitta = splitta.sbd.load_sbd_model(script_dir + u'/splitta/model_nb/', use_svm=False)
    else: self._splitta = None

    ## Internal variable to store the \p subprocess.Pipe for communicating with the Java process.
    self._tagger = None

    ## Arguments to \link StanfordCoreNLPWrapper.java StanfordCoreNLPWrapper\endlink
    self._tagger_args = java_args + ['-cp', ':'.join(map(lambda jar: os.path.join(script_dir, '..', 'lib', jar), ['commons-cli-1.2.jar', 'corenlp-models.jar', 'stanford-corenlp-3.5.0.jar', 'yc-utils.jar'])), 'edu.yc.nlp.StanfordCoreNLPWrapper'] + args
  #end def

  ## @brief Annotates a chunk of text. It first splits sentences using Splitta (if \p ssplit is set to \p True during construction), then calls \p StanfordCoreNLP.annotate_sent to annotate each sentence.
  ## @param text text to annotate
  ## @param sentence_separator if Splitta is not used, we will split sentence using this character
  ## @return a list of annotated sentences
  def annotate(self, text, sentence_separator=u'\n'):
    if not self._tagger: self._tagger = subprocess.Popen(self._tagger_args, stdin=subprocess.PIPE, stdout=subprocess.PIPE, shell=False, close_fds=True, bufsize=1)

    if self._splitta: sents = splitta.sbd.sbd_text(self._splitta, text, do_tok=False)
    else: sents = text.split(sentence_separator)

    return map(self.annotate_sent, sents)
  #end def

  ## @brief Annotates a single sentence by passing the data to \p STDIN of the Java wrapper (see \p edu.yc.nlp)
  def annotate_sent(self, sent):
    if not self._tagger: self._tagger = subprocess.Popen(self._tagger_args, stdin=subprocess.PIPE, stdout=subprocess.PIPE, shell=False, close_fds=True, bufsize=1)

    sent = sent.strip()
    if not sent: return u''

    print >>self._tagger.stdin, sent.encode('utf-8')
    self._tagger.stdin.flush()
    pos_tagged = self._tagger.stdout.readline().decode('utf-8').strip()

    return pos_tagged
  #end def

  ## @brief Destructor. Closes the \p STDIN stream and waits for the process to terminate.
  def __del__(self):
    if getattr(self, '_tagger', None):
      self._tagger.stdin.close()
      self._tagger.wait()
    #end if
  #end def
#end class
