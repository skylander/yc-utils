import argparse
import codecs
import sys
import unicodedata

parser = argparse.ArgumentParser(description='Helper script to generate C++ headers containing unicode character information.')
A = parser.parse_args()

if __name__ == '__main__':
  sys.stdout = codecs.getwriter('utf-8')(sys.stdout)
  sys.stderr = codecs.getwriter('utf-8')(sys.stderr)

  unicode_punctuations = []
  for c in map(unichr, xrange(sys.maxunicode + 1)):
    cat = unicodedata.category(c)
    if cat[0] == 'P':
      unicode_punctuations.append(c)

  print u', '.join(map(lambda c: '\"\u{:04x}\"'.format(ord(c)), unicode_punctuations))
#end if
