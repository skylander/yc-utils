/**
 * @file
 * @brief Implements the two-way feature mapping class.
 */
#ifndef YCUTILS_FEATURE_MAP_H_INCLUDED
#define YCUTILS_FEATURE_MAP_H_INCLUDED

#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

#include <boost/range/algorithm/copy.hpp>
#include <boost/range/algorithm/for_each.hpp>
#include <boost/range/algorithm/sort.hpp>

#ifdef USE_EIGEN
#include <Eigen/SparseCore>
#endif

#include "ycutils.h"
#include "ycutils/collections.h"
#include "ycutils/vecmath.h"

using ycutils::index_type;
using ycutils::size_type;

namespace ycutils {

/**
 * @brief A class to hold two-way mappings between (usually) strings-based feature description and their corresponding indexes.
 * @details Essentially an \p unordered_map and a \p vector to store the two way mapping between feature names and indexes.
 * @tparam T type of feature description
 */
template <typename T = std::string>
class feature_map
{
public:
  /// Type alias describing the feature.
  using feature_type = T;

  /// Default constructor.
  feature_map() {}

  /// Constructs feature map from a sequence.
  template <typename SinglePassRange>
  feature_map(const SinglePassRange& seq) { add_many(seq); }

  /// Add \p feature to \p feature_map.
  /// @return the index of the new (or already existing) feature
  index_type add(const T& feature)
  {
    auto ret = _map.emplace(feature, 0);
    if (!ret.second)
      return ret.first->second;

    ret.first->second = _rmap.size();
    _rmap.push_back(feature);

    return ret.first->second;
  }

  /// Add many features at one go.
  template <typename SinglePassRange>
  void add_many(const SinglePassRange& seq)
  {
    boost::for_each(seq, [&](const T& feature) {
      add(feature);
    });
  }

  /// Test whether feature map is empty.
  bool empty() const { return _rmap.empty(); }

  /// Return number of features.
  size_type size() const { return _rmap.size(); }

  /// Request a change in capacity.
  void reserve(size_type n) { _rmap.reserve(n); _map.reserve(n); }

  /// Return \p vector<T>::const_iterator to the beginning.
  typename std::vector<T>::const_iterator begin() const { return _rmap.cbegin(); }

  /// Return \p vector<T>::const_iterator to the end.
  typename std::vector<T>::const_iterator end() const { return _rmap.cend(); }

  /// Return the index of feature.
  const index_type operator[](const T& feature) const { return _map.at(feature); }

  /// Return feature description at position \p i.
  const T& at(index_type i) const { return _rmap[i]; }

  /// Return feature description at position \p i.
  const T& index(index_type i) const { return _rmap[i]; }

  /// Return the first feature in the map.
  const T& front() const { return _rmap.front(); }

  /// Return the last feature in the map.
  const T& back() const { return _rmap.back(); }

  /// Returns true if feature map contains \p feature.
  const bool contains(const T& feature) const { return (_map.find(feature) != _map.end()); }

  /// Add feature to \p feature_map.
  void push_back(const T& feature) { add(feature); }

  /// Clear feature map.
  void clear() { _map.clear(); _rmap.clear(); }

  /// Sort feature names according to given predicate \p pred.
  template <typename BinaryPredicate>
  void sort(BinaryPredicate pred)
  {
    _map.clear();
    boost::sort(_rmap, pred);
    for (auto i : ycutils::irange(_rmap.size()))
      _map.emplace(_rmap[i], i);
  }

  /// Sort feature names lexicographically.
  void sort() {sort([](const T& a, const T& b) { return a < b; }); }

  /**
   * @brief Converts an raw feature vector to a \p ycutils::sparse_vector type, in the process creating feature map entries if they dont exist.
   * @details Raw feature vector should be an iterable sequence of \p std::pair<T, value_type>.
   * @param featvec raw feature vector
   * @param svec \p ycutils::sparse_vector to store results
   */
  template <typename SinglePassRange, typename value_type>
  void make_sparse_vector(const SinglePassRange& featvec, sparse_vector<value_type> *svec)
  {
    boost::for_each(featvec, [&](const std::pair<T, value_type>& e) {
      auto i = add(e.first);
      (*svec)[i] = e.second;
    });
  }

#if defined(USE_EIGEN) || defined(DOXYGEN)
  /**
   * @brief Converts an raw feature vector to a \p Eigen::SparseVector type, in the process creating feature map entries if they dont exist.
   * @details Raw feature vector should be an iterable sequence of \p std::pair<T, value_type>.
   * @param featvec raw feature vector
   * @param svec \p Eigen::SparseVector to store results
   * @see USE_EIGEN
   */
  template <typename SinglePassRange, typename value_type>
  void make_sparse_vector(const SinglePassRange& featvec, Eigen::SparseVector<value_type> *svec)
  {
    boost::for_each(featvec, [&](const std::pair<T, value_type>& e) {
      auto i = add(e.first);
      svec->insert(i) = e.second;
    });
  }
#endif

  /**
   * @brief Makes a list of feature names from feature vector.
   * @param featvec an iterable of \p std::pair<T, int_type>.
   * @param it pushes list items onto \p it
   */
  template <typename SinglePassRange, typename OutputIterator, typename int_type = ycutils::size_type>
  void make_list(const SinglePassRange& featvec, OutputIterator it)
  {
    boost::for_each(featvec, [&](const std::pair<T, int_type>& e) {
      auto i = add(e.first);
      for (int k = e.second; k >= 0; --k)
        *it++ = i;
    });
  }

private:
  /// Store mappings from \p T -> \p ycutils::index_type.
  std::unordered_map<T, index_type> _map;

  /// Store mappings from\p ycutils::index_type -> \p T.
  std::vector<T> _rmap;
};

/// \p ostream overloading for \p ycutils::feature_map
template <typename T>
std::ostream& operator<<(std::ostream& os, const ycutils::feature_map<T>& feat_map)
{
  os << "[";
  index_type i = 0;
  for (auto feat : feat_map)
  {
    os << i++ << ": " << feat;
    if (i < feat_map.size()) os << ", ";
  }
  os << "]";

  return os;
}

}  // namespace ycutils


#endif  // YCUTILS_FEATURE_MAP_H_INCLUDED
