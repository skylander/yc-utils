/**
 * @file
 * @brief Unit test code for the collections header library \p ycutils/collections.h.
 * @cond
 */
#include <iostream>

#include <array>
#include <deque>
#include <forward_list>
#include <list>
#include <map>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE CollectionsTest
#include <boost/test/unit_test.hpp>

#include <boost/lexical_cast.hpp>

#include <ycutils/collections.h>

BOOST_AUTO_TEST_SUITE(collections_test);

BOOST_AUTO_TEST_CASE(printing)
{
  BOOST_REQUIRE(boost::lexical_cast<std::string>(std::vector<std::pair<int, int> >({{1, 1}, {2, 2}, {3, 3}})) == "[1:1, 2:2, 3:3]");

  BOOST_REQUIRE(boost::lexical_cast<std::string>(std::unordered_map<int, int>()) == "{}");
  BOOST_REQUIRE(boost::lexical_cast<std::string>(std::unordered_set<int>()) == "{}");
  BOOST_REQUIRE(boost::lexical_cast<std::string>(std::vector<int>()) == "[]");

  BOOST_REQUIRE(boost::lexical_cast<std::string>(std::vector<int>({1, 2, 3, 4, 5})) == "[1, 2, 3, 4, 5]");
  BOOST_REQUIRE(boost::lexical_cast<std::string>(std::deque<int>({1, 2, 3, 4, 5})) == "[1, 2, 3, 4, 5]");
  BOOST_REQUIRE(boost::lexical_cast<std::string>(std::forward_list<int>({1, 2, 3, 4, 5})) == "[1, 2, 3, 4, 5]");
  BOOST_REQUIRE(boost::lexical_cast<std::string>(std::list<int>({1, 2, 3, 4, 5})) == "[1, 2, 3, 4, 5]");

  // std::cout << boost::lexical_cast<std::string>(std::unordered_map<int, int>({{1, 1}, {2, 2}, {3, 3}})) << std::endl;
  // BOOST_REQUIRE(boost::lexical_cast<std::string>(std::unordered_map<int, int>({{1, 1}, {2, 2}, {3, 3}})) == "{3:3, 1:1, 2:2}");
  // BOOST_REQUIRE(boost::lexical_cast<std::string>(std::unordered_set<int>({1, 2, 3, 4, 5})) == "{5, 1, 2, 3, 4}");
  BOOST_REQUIRE(boost::lexical_cast<std::string>(std::map<int, int>({{1, 1}, {2, 2}, {3, 3}})) == "{1:1, 2:2, 3:3}");
  BOOST_REQUIRE(boost::lexical_cast<std::string>(std::set<int>({1, 2, 3, 4, 5})) == "{1, 2, 3, 4, 5}");

  BOOST_REQUIRE(ycutils::collection_to_string(std::array<int, 5>({1, 2, 3, 4, 5}), "", "", "") == "12345");
  BOOST_REQUIRE(ycutils::collection_to_string(std::vector<int>({1, 2, 3, 4, 5}), "", "", "") == "12345");
  BOOST_REQUIRE(ycutils::collection_to_string(std::deque<int>({1, 2, 3, 4, 5}), "", "", "") == "12345");
  BOOST_REQUIRE(ycutils::collection_to_string(std::forward_list<int>({1, 2, 3, 4, 5}), "", "", "") == "12345");
  BOOST_REQUIRE(ycutils::collection_to_string(std::list<int>({1, 2, 3, 4, 5}), "", "", "") == "12345");

  // BOOST_REQUIRE(ycutils::collection_to_string(std::unordered_map<int, int>({{1, 1}, {2, 2}, {3, 3}}), "(", ")", "|") == "(3:3|1:1|2:2)");
  // BOOST_REQUIRE(ycutils::collection_to_string(std::unordered_set<int>({1, 2, 3, 4, 5}), "(", ")", " ") == "(5 1 2 3 4)");
  BOOST_REQUIRE(ycutils::collection_to_string(std::map<int, int>({{1, 1}, {2, 2}, {3, 3}}), "(", ")", "|") == "(1:1|2:2|3:3)");
  BOOST_REQUIRE(ycutils::collection_to_string(std::set<int>({1, 2, 3, 4, 5}), "(", ")", " ") == "(1 2 3 4 5)");
}

BOOST_AUTO_TEST_CASE(irange)
{
  ycutils::size_type j = 0;
  for (auto i : ycutils::irange<>(10))
    BOOST_REQUIRE(i == j++);
}

BOOST_AUTO_TEST_SUITE_END();
/// @endcond
