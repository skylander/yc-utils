/**
 * @file
 * @brief Performs sigmoid operations quickly by pre-computing their values.
 * @details This header includes \p ycutils::sigmoid_table class for pre-computing these function values and storing them in a table.
 * Custom mathematic functions can be easily extended using the \p ycutils::fastmath_table base class.
 */
#ifndef YCUTILS_MATHTABLES_H_INCLUDED
#define YCUTILS_MATHTABLES_H_INCLUDED

#include <cmath>

#include <stdexcept>
#include <vector>

#include <boost/math/special_functions/log1p.hpp>
#include <boost/math/special_functions/expm1.hpp>

#include "ycutils.h"
#include "math.h"

using ycutils::size_type;

namespace ycutils {

#ifdef DOXYGEN
  /**
   * @brief Disable fallback functions for all of the fastmath classes when \p x is out of range.
   * @see ycutils::exp_table
   */
  #define YCUTILS_FASTMATH_NO_FALLBACK

  /**
   * @brief Do range checking and fallback to using \p std::exp when x is out of range.
   * @see YCUTILS_SIGMOID_TABLE_NO_FALLBACK
   */
  #define YCUTILS_SIGMOID_TABLE_USE_FALLBACK true

  /**
   * @brief Do not do range checking and fallback to using \p std::exp when x is out of range.
   * @see YCUTILS_SIGMOID_TABLE_USE_FALLBACK
   */
  #define YCUTILS_SIGMOID_TABLE_NO_FALLBACK

#else

  #ifdef YCUTILS_FASTMATH_NO_FALLBACK
    #define YCUTILS_SIGMOID_TABLE_USE_FALLBACK false
  #else
    #define YCUTILS_SIGMOID_TABLE_USE_FALLBACK true
  #endif

  #ifdef YCUTILS_SIGMOID_TABLE_NO_FALLBACK
    #define YCUTILS_SIGMOID_TABLE_USE_FALLBACK false
  #else
    #define YCUTILS_SIGMOID_TABLE_USE_FALLBACK true
  #endif

  template <typename Real>
  class fastmath_table;
#endif

/**
 * @brief Pre-compute values for the sigmoid function (\f$ \sigma(x) = \frac{\exp(x)}{1 + \exp(x)} \f$).
 * @details This class have relative error of < 1e-4 at 1e-6 precision between [-5, 5].
 * The relative precision of this function is about \p sigmoid(p).
 *
 * @note Instead of using \p std::exp for fallback, the function directly returns 0/1 when the range is exceeded.
 */
template <typename Real = math::default_floating_point_type>
class sigmoid_table : public fastmath_table<Real>
{
public:
  /// Default minimum range of \p sigmoid_table.
  static constexpr Real default_min = -5.0;

  /// Default maximum range of \p sigmoid_table.
  static constexpr Real default_max = 5.0;

  /// Default precision of \p sigmoid_table.
  static constexpr Real default_precision = 1e-6;

  /// Default constructor.
  sigmoid_table() : fastmath_table<Real>() {}

  /// @see \p ycutils::fastmath_table::fastmath_table(Real min, Real max, Real precision).
  sigmoid_table(Real min, Real max, Real precision) : fastmath_table<Real>(min, max, precision) {}

  /// Initialize the table with default values: \p sigmoid_table::default_min, \p sigmoid_table::default_max and \p sigmoid_table::default_precision.
  size_type initialize() { return ((fastmath_table<Real> *)this)->initialize(default_min, default_max, default_precision); }

  /// Computes \p std::exp(x) using our pre-computed tables.
  inline Real operator()(Real x) const { return YCUTILS_SIGMOID_TABLE_USE_FALLBACK ? check_fallback(x) : this->lookup(x); }

  /// <tt>f(x) = std::exp(x)</tt>
  inline Real f(Real x) const { return std::exp(x) / (1.0 + std::exp(x)); }

  /// the name of this class.
  /// @see fastmath_table::name()
  inline std::string name() const { return "sigmoid_table"; }

  /// calculates the estimated relative accuracy of pre-computed values.
  inline Real accuracy() const { return f(this->precision()); }

protected:
  /// Automatically fallback if \p x is not in range.
  inline Real check_fallback(Real x) const
  {
    if (x < this->min())
      return 0.0;
    if (x > this->max())
      return 1.0;

    return this->lookup(x);
  }
};

/**
 * @brief \p fastmath_table implements a base class for storing pre-computed values of mathematical functions.
 * @details Mathematical functions, such as \p exp, \p log, \p sigmoid take several CPU cycles to compute.
 * To increase the speed of such computations, we can pre-compute values of these functions at pre-defined intervals and store them in a huge table, which we can lookup rapidy.
 *
 * @tparam Real = math::default_floating_point_type the data type that will be stored here.
 * @see exp_table
 */
template <typename Real = math::default_floating_point_type>
class fastmath_table
{
public:
  /// Typedef for vlues stored in our table.
  using value_type = Real;

  /**
   * @brief Initialize \p fastmath_table with default values. See individual class implementations for what these default values are.
   */
  virtual size_type initialize() { throw std::logic_error("initialize() method not implemented."); }

  /**
   * @brief Initialize \p fastmath_table with values specified in the argument.
   * @details A \p std::vector<Real> of size \p table_size will be populated with values of \p f(x) at regularly spaced intervals between \p min and \p max inclusive.
   *
   * @param min minimum value to pre-compute.
   * @param max maximum value to pre-compute.
   * @param table_size size of table.
   * @return the size of the pre-computed table
   * @see initialize(Real min, Real max, Real precision)
   */
  size_type initialize(Real min, Real max, size_type table_size)
  {
    BOOST_ASSERT(max > min);
    min_ = min;
    max_ = max;
    precision_ = (max - min) / (table_size - 1);  // to fit everything in the table

    table_.clear();
    table_.resize(table_size);
    for (size_type i = 0; i < table_size; ++i)
      table_[i] = f(min + (i * precision_));
    table_.back() = f(max);

    BOOST_ASSERT(table_size == table_.size());

    return table_.size();
  }

  /**
   * @brief Initialize \p fastmath_table with values specified in the argument.
   * @details A \p std::vector<Real> will be populated with values of \p f(x) computed at intervals of \p precision between \p min and \p max inclusive.
   * Smaller values of precision encourages more accuracy pre-computed results but at the expense of larger memory usage.
   *
   * @param min minimum value to pre-compute
   * @param max maximum value to pre-compute
   * @param precision difference between consecutive table entries
   * @return the size of the pre-computed table
   * @see initialize(Real min, Real max, size_type table_size)
   */
  inline size_type initialize(Real min, Real max, Real precision) { return initialize(min, max, static_cast<size_type>(std::ceil((max - min) / precision))); }

  /**
   * @brief Returns the computed value \f$ f(x) \f$.
   * @param x the value to compute
   * @return \f$ f(x) \f$ either from pre-computed tables or through the fallback function
   * @note If the associated \p USE_FALLBACK macro is defined, range checking will be done. If \p x falls outside the pre-computed range, the fallback function will be used instead.
   */
  virtual inline Real operator()(Real x) const { return check_fallback(x); }

  /// Implementation of \f$ f(x) \f$ (should be overridden by derived classes).
  virtual inline Real f(Real x) const { throw std::logic_error("f() method not implemented."); }

  /// Name of the class (should be overridden by derived classes).
  virtual inline std::string name() const { throw std::logic_error("name() method not implemented."); }

  /// Relative accuracy of pre-computed values (should be overridden by derived classes).
  virtual inline Real accuracy() const { throw std::logic_error("accuracy method not implemented."); }

  /// Minimum range of pre-computed table.
  inline Real min() const { return min_; }

  /// Maximum range of pre-computed table.
  inline Real max() const { return max_; }

  /// Precision of pre-computed table.
  inline Real precision() const { return precision_; }

  /// Size of pre-computed table.
  inline Real table_size() const { return table_.size(); }

protected:
  /// Default constructor; does not initialize/populate tables.
  fastmath_table() : min_(0.0), max_(0.0), precision_(0.0) {}

  /// Pre-computes and populates table for the given parameters.
  fastmath_table(Real min, Real max, Real precision) { initialize(min, max, precision); }

  /// Lookup the value of \f$ f(x) \f$ in the tables.
  inline Real lookup(Real x) const
  {
    BOOST_ASSERT_MSG(precision_ > 0, name() + " is uninitialized.");
    BOOST_ASSERT_MSG(x >= min_ && x <= max_, "exceeded range of " + name());

    return table_[static_cast<size_type>((x - min_) / precision_)];
  }

  /// Automatically fallback if \p x is not in range.
  inline Real check_fallback(Real x) const
  {
    if (x < min_ or x > max_)
      return f(x);
    return lookup(x);
  }

private:
  /// Private variables storing the table parameters.
  Real min_;

  /// Private variables storing the table parameters.
  Real max_;

  /// Private variables storing the table parameters.
  Real precision_;

  /// Store our pre-computed values in a \p std::vector
  std::vector<Real> table_;
};

}  // namespace ycutils

#endif  // YCUTILS_MATHTABLES_H_INCLUDED
