/**
 * @file
 * @brief A collection of utility functions for doing math.
 */
#ifndef YCUTILS_MATH_H_INCLUDED
#define YCUTILS_MATH_H_INCLUDED

#include <cmath>

#include <stdexcept>
#include <vector>

#include <boost/math/special_functions/log1p.hpp>
#include <boost/math/special_functions/expm1.hpp>

#if defined(USE_EIGEN)
#include <Eigen/Dense>
#endif

#include "ycutils.h"
#include "ycutils/collections.h"

using ycutils::size_type;

namespace ycutils {

/// Namespace for mathematical constants and non-public implementations of base classes.
namespace math {

/// Floating point type used for this header library.
using default_floating_point_type = double;

/// Used in log space operations, where any exponent smaller than \p logarithmic_operations_difference_limit will be treated as 0.
static constexpr default_floating_point_type logarithmic_operations_difference_limit = -20.0;

/// Used in place of negative infinity (if its to be used at all).
static constexpr default_floating_point_type log_zero = -1e64;

/// Tolerance value for comparing floating points.
static constexpr default_floating_point_type epsilon_tolerance = 1e-8;

/// For when we need a value close to zero.
static constexpr default_floating_point_type close_to_zero = 1e-16;
}  // namespace math

/**
 * @brief Performs log space addition.
 * @details Given \f$\log x\f$, and \f$\log y\f$, computes \f$\log (x+y)\f$.
 *
 * @param log_x \f$\log x\f$
 * @param log_y \f$\log y\f$
 * @return \f$\log (x+y)\f$
 */
template <typename Real = math::default_floating_point_type>
inline Real log_add(Real log_x, Real log_y)
{
  // Make sure that the floating point implementation supports infinity.
  static_assert(std::numeric_limits<Real>::has_infinity, "'Real' type must support infinity values.");

  if (log_y > log_x)
  {
    auto temp = log_x;
    log_x = log_y;
    log_y = temp;
  }

#ifdef HONOR_INFINITIES
  if (std::isinf(log_x) and log_x < 0.0)  // negative infinity
    return log_x;

  if (std::isinf(log_y) and log_y < 0.0)  // negative infinity
    return log_x;
#endif

  auto neg_diff = log_y - log_x;
  if (neg_diff < math::logarithmic_operations_difference_limit)
    return log_x;

  return log_x + boost::math::log1p(std::exp(neg_diff));
}

/**
 * @brief Performs log space subtraction.
 * @details Given \f$\log x\f$, and \f$\log y\f$, computes \f$\log (x-y)\f$.
 *
 * @param log_x \f$\log x\f$
 * @param log_y \f$\log y\f$
 * @return \f$\log (x-y)\f$
 */
template <typename Real = math::default_floating_point_type>
inline Real log_subtract(Real log_x, Real log_y)
{
  // Make sure that the floating point implementation supports infinity.
  static_assert(std::numeric_limits<Real>::has_infinity, "'Real' type must support infinity values.");

  BOOST_ASSERT_MSG(log_x >= log_y, "log_x must be >= log_y");

#ifdef HONOR_INFINITIES
  if ((std::isinf(log_y) and log_y < 0.0) || std::isnan(log_y))
    return log_x;
#endif

  return log_y + std::log(std::exp(log_x - log_y) - 1);
}

/**
 * @brief Compares two floating point number to see if they are "equal" to each other.
 * @details Uses \p ycutils::math::epsilon_tolerance as the tolerance value.
 *
 * @param x
 * @param y
 * @return \p true if \f$|x-y| < \epsilon\f$
 */
template <typename Real = math::default_floating_point_type>
inline bool is_close(Real x, Real y)
{
#ifdef HONOR_INFINITIES
  if (std::isinf(x))
    return std::isinf(y) && std::signbit(x) == std::signbit(y);
  if (std::isinf(y))
    return false;
#endif

#if HONOR_NANS
  if (std::isnan(x) || std::isnan(y))
    return false;
#endif

  return std::abs(x - y) < math::epsilon_tolerance;
}

/**
 * @brief Performs safe division between two numbers.
 * @details It returns a prespecified \p default_value.
 * @param x
 * @param y
 * @param default_value return this value when \f$ x / y\f$ is infinite or NaN
 * @tparam Real = math::default_floating_point_type
 * @return \f$ x / y\f$ on success or \p default_value on bad divide
 */
template <typename Real = math::default_floating_point_type>
inline Real safe_divide(const Real& x, const Real& y, const Real& default_value = 0.0)
{
  const Real ans = x / y;

#if HONOR_NANS
  if (std::isnan(ans))
    return default_value;
#endif

#if HONOR_INFINITIES
  if (std::isinf(ans))
    return default_value;
#endif

  assert(std::isnormal(ans));

  return ans;
}

/**
 * @brief Computes the softmax transformation of a vector in-place.
 * @param vec vector to compute softmax transformation of.
 */
template <typename Vector>
inline void softmax_inplace(Vector& vec)
{
  const size_type N = vec.size();
  double v_max = vec[0];
  for (index_type i = 1; i < N; ++i)
    if (vec[i] > v_max)
      v_max = vec[i];

  double sum = 0.0;
  for (const auto i : ycutils::irange(N))
  {
    vec[i] = std::exp(vec[i] - v_max);
    sum += vec[i];
  }
  for (const auto i : ycutils::irange(N))
    vec[i] /= sum;
}

/**
 * @brief Computes the softmax transformation of a vector and return it.
 * @param vec vector to compute softmax transformation of.
 */
inline std::vector<double> softmax(const std::vector<double>& vec)
{
  std::vector<double> r(vec.size());

  const size_type N = vec.size();
  double v_max = vec[0];
  for (index_type i = 1; i < N; ++i)
    if (vec[i] > v_max)
      v_max = vec[i];

  double sum = 0.0;
  for (const auto i : ycutils::irange(N))
  {
    r[i] = std::exp(vec[i] - v_max);
    sum += vec[i];
  }
  for (const auto i : ycutils::irange(N))
    r[i] /= sum;

  return r;
}

#if defined(USE_EIGEN) || defined(DOXYGEN)
/**
 * @brief Computes the softmax transformation of an \p Eigen vector and return it.
 * @param vec vector to compute softmax transformation of.
 */
inline Eigen::VectorXd softmax(const Eigen::VectorXd& vec)
{
  const size_type N = vec.size();
  double v_max = vec[0];
  for (index_type i = 1; i < N; ++i)
    if (vec[i] > v_max)
      v_max = vec[i];

  Eigen::VectorXd r(N);
  double sum = 0.0;
  for (const auto i : ycutils::irange(N))
  {
    r[i] = std::exp(vec[i] - v_max);
    sum += r[i];
  }
  for (const auto i : ycutils::irange(N))
    r[i] /= sum;

  return r;
}
#endif

/**
 * @brief Computes the softmax transformation of a vector and storing the results in \p dst.
 * @param dst Store transformed vector here.
 * @param vec Vector to compute softmax transformation of.
 */
template <typename Vector1, typename Vector2>
inline void softmax(Vector1& dst, const Vector2& vec)
{
  const size_type N = vec.size();
  double v_max = vec[0];
  for (index_type i = 1; i < N; ++i)
    if (vec[i] > v_max)
      v_max = vec[i];

  double sum = 0.0;
  for (const auto i : ycutils::irange(N))
  {
    dst[i] = std::exp(vec[i] - v_max);
    sum += dst[i];
  }
  for (const auto i : ycutils::irange(N))
    dst[i] /= sum;
}

}  // namespace ycutils

#endif  // YCUTILS_MATH_H_INCLUDED
