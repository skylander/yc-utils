/**
 * @file
 * @brief App for building n-gram vocabulary files from tokenized text.
 *
 * Accepts a variety of input format and saves output in a TAB-delimited file.
 *
 * @todo handle gzipped input
 *
 * @verbatim
Usage: src/buildvocab [options] [input files ...]

Builds vocabulary from tokenized texts. Accepts a variety of input format and saves output in a TAB-delimited file.

Allowed options:
  -h [ --help ]                         produce help message
  -i [ --input ] doc1 doc2 ...          input documents, if no files are
                                        specified, data is read from STDIN
                                        where each sentence is treated as a
                                        document
  -o [ --vocab-file ] file (=vocab)     file to write vocabulary information to

Advanced options (use `prunevocab' for more advanced filtering options):
  --min-freq N (=5)                     remove types that appear < N times
  --sort [freq | lex | none] (=freq)    sort vocabulary by (freq)uency or
                                        (lex)icographical order
  -n [ --ngrams ] n1 n2 ...             list of n-gram sizes to use
  -t [ --tag-sep ] sep                  tokens contains POS tags separated by
                                        <sep>.
  -s [ --stop-file ] file1 file2 ...    use a custom set of stop words
                                        (remember to include punctuations in
                                        the file too)
  -S [ --stop-list ] [english | chinese | punctuations]
                                        use one (or more) of the predefined
                                        stopwords list
  -U [ --without-unk ]                  do not include the unknown symbol
                                        (__UNK__)
  -L [ --line-doc ]                     treats each line as a document
@endverbatim

Some examples of how to use the app:
\code{.sh}
  pv zhwiki.sents.tokenized | buildvocab -o zhwiki.vocab
\endcode
@cond
 */
#include <cstdlib>

#include <iostream>
#include <iterator>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>

#include <boost/algorithm/string.hpp>
#include <boost/program_options.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/algorithm_ext.hpp>
#include <boost/timer/timer.hpp>
#include <boost/tokenizer.hpp>

#include <ycutils/program_options.h>
#include <ycutils/stopwords.h>
#include <ycutils/vocabulary.h>

namespace po = boost::program_options;

using std::cerr;
using std::cin;
// using std::cout;
using std::endl;

using std::ifstream;
using std::ofstream;

using std::string;
using std::vector;
using std::pair;
using std::unordered_set;
using std::unordered_map;

using ycutils::freq_type;
const ycutils::vocabulary V;

long int count_tokens(const vector<uint>&, const unordered_set<string>&, const string&, std::istream&, unordered_map<string, pair<long, long> > *, bool);

int main(int argc, const char **argv)
{
  po::options_description app_options(string("Usage: ") + argv[0] + " [options] [input files ...]\n\nBuilds vocabulary from tokenized texts. Accepts a variety of input format and saves output in a TAB-delimited file.\n\nAllowed options");

  string vocab_filename;
  vector<uint> ngrams;
  bool line_as_doc, without_unk = false;

  app_options.add_options()
    ("help,h", "produce help message")
    ("input,i", po::value<ycutils::input_file_arguments>()->value_name("doc1 doc2 ..."), "input documents, if no files are specified, data is read from STDIN where each sentence is treated as a document")
    ("vocab-file,o", po::value<string>(&vocab_filename)->default_value("vocab")->value_name("file"), "file to write vocabulary information to");

  po::options_description advanced_options("Advanced options (use `prunevocab' for more advanced filtering options)");
  advanced_options.add_options()
    ("min-freq", po::value<ycutils::positive_argument<freq_type> >()->default_value(ycutils::positive_argument<freq_type>(5), "5")->value_name("N"), "remove types that appear < N times")
    ("sort", po::value<string>()->default_value(ycutils::vocabulary::sort_order[0])->value_name(string("[") + boost::algorithm::join(ycutils::vocabulary::sort_order, " | ") + "]"), "sort vocabulary by (freq)uency or (lex)icographical order")
    ("ngrams,n", po::value<ycutils::positive_arguments<uint> >()->multitoken()->value_name("n1 n2 ..."), "list of n-gram sizes to use")
    ("tag-sep,t", po::value<string>()->value_name("sep"), "tokens contains POS tags separated by <sep>.")
    ("stop-file,s", po::value<ycutils::input_file_arguments>()->value_name("file1 file2 ...")->default_value(ycutils::input_file_arguments(), "")->multitoken(), "use a custom set of stop words (remember to include punctuations in the file too)")
    ("stop-list,S", po::value<vector<string> >()->multitoken()->value_name(string("[") + boost::algorithm::join(ycutils::stopwords::predefined, " | ") + "]")->default_value(vector<string>(), ""), "use one (or more) of the predefined stopwords list")
    ("without-unk,U", po::bool_switch(&without_unk), "do not include the unknown symbol")
    ("line-doc,L", po::bool_switch(&line_as_doc), "treats each line as a document");

  app_options.add(advanced_options);

  po::positional_options_description pos_options;
  pos_options.add("input", -1);

  po::variables_map vm;

  try
  {
    po::store(po::command_line_parser(argc, argv).options(app_options).positional(pos_options).run(), vm);
    if (vm.count("help"))
    {
      cerr << app_options << "\n";
      return 1;
    }
    po::notify(vm);

    if (boost::find(ycutils::vocabulary::sort_order, vm["sort"].as<string>()) == ycutils::vocabulary::sort_order.end())
      throw std::logic_error(string("the argument for '--sort' is invalid. it should be one of ") + boost::algorithm::join(ycutils::vocabulary::sort_order | boost::adaptors::transformed([](const string& s) { return string("'") + s + "'"; }), ", ") + ".\n");

    if (vm.count("stop-list") > 0)
      for (auto& name : vm["stop-list"].as<vector<string> >())
        ycutils::use_predefined(name);
  } catch (std::exception &e)
  {
    cerr << e.what() << endl << "Type " << argv[0] << " --help for usage information." << endl;
    return EXIT_FAILURE;
  }

  if (vm.count("ngrams"))
    transform(begin(vm["ngrams"].as<ycutils::positive_arguments<uint> >()), end(vm["ngrams"].as<ycutils::positive_arguments<uint> >()), back_inserter(ngrams), [](const ycutils::positive_argument<uint> & pia) { return pia.value(); });
  else
    ngrams.push_back(1);

  string pos_tag_sep = vm.count("tag-sep") > 0 ? vm["tag-sep"].as<string>() : "";

  auto min_freq = vm["min-freq"].as<ycutils::positive_argument<freq_type> >().value();

  boost::timer::auto_cpu_timer timer(cerr, 2, "Took %w seconds.\n");

  ycutils::stopwords_set stopwords;
  ycutils::init_stopwords(ycutils::extract_arguments(vm["stop-file"].as<ycutils::input_file_arguments>()), vm["stop-list"].as<vector<string> >(), &stopwords);

  // remove duplicate ngram sizes and sort them in ascending order
  boost::erase(ngrams, boost::unique<boost::return_found_end>(boost::sort(ngrams)));
  const auto ngrams_desc = boost::algorithm::join(ngrams | boost::adaptors::transformed(boost::lexical_cast<string, int>), ", ") + "-grams";

  // actual processing of text file
  unordered_map<string, pair<long, long> > D;
  D.reserve(100000);  // set to a fairly large number for speed

  if (vm.count("input"))
  {
    for (const ycutils::input_file_argument input_file : vm["input"].as<ycutils::input_file_arguments>())
    {
      ifstream fin(input_file.value());
      if (!fin.good())
      {
        cerr << "Error trying to read from " << input_file.value() << "!";
        continue;
      }
      cerr << "Reading " << input_file.value() << " for " << ngrams_desc << "...";
      cerr << count_tokens(ngrams, stopwords, pos_tag_sep, fin, &D, line_as_doc) << " tokens found!" << endl;

      fin.close();
    }
  }
  else
  {
    cerr << "Reading STDIN for " << ngrams_desc << "...";
    cerr << count_tokens(ngrams, stopwords, pos_tag_sep, cin, &D, line_as_doc) << " tokens found!" << endl;
  }

  // gather all the tokens in a list
  vector<string> tokens;
  boost::copy(D | boost::adaptors::map_keys, back_inserter(tokens));

  if (vm["sort"].as<string>() == "lex")
  {
    cerr << "Sorting by lexicographical order...";
    boost::sort(tokens);
    cerr << "done!" << endl;
  }
  else if (vm["sort"].as<string>() == "freq")
  {
    cerr << "Sorting by type frequency...";
    boost::sort(tokens, [&](const string& a, const string& b) { return D[a].first > D[b].first; });
    cerr << "done!" << endl;
  }

  // save tokens to file.
  cerr << "Saving types that appeared >= " << min_freq << " times to '" << vocab_filename <<"'...";
  int type_count = 1;
  ofstream fout(vocab_filename, std::ios_base::out | std::ios_base::trunc);

  if (!without_unk)  // save unknown symbol
    V.write_line(fout, ycutils::type_info(V.unknown_symbol, min_freq, 1));

  for_each(begin(tokens), end(tokens),
    [&](string tok)
    {
      if (D[tok].first < min_freq) return;
      V.write_line(fout, ycutils::type_info(tok, D[tok].first, D[tok].second));
      type_count++;
    });
  fout.close();

  cerr << type_count << " types saved!" << endl;

  return EXIT_SUCCESS;
}

long int count_tokens(const vector<uint>& ngrams, const ycutils::stopwords_set& stopwords, const string& pos_tag_sep, std::istream& in, unordered_map<string, pair<long, long> > *D, bool line_is_document)
{
  long int total = 0;
  boost::char_separator<char> sep(" ");
  unordered_set<string> doc_toks;  // tokens that appears in this document

  for(string line; std::getline(in, line); )
  {
    boost::tokenizer<boost::char_separator<char> > tokens(line, sep);

    vector<string> seen_tokens;
    for (const auto& tok0 : tokens)
    {
      auto tok = tok0;
      if (!pos_tag_sep.empty()) tok = tok0.substr(0, tok0.rfind(pos_tag_sep));

      seen_tokens.push_back(tok);

      if (stopwords.count(tok) > 0)
      {
        seen_tokens.clear();
        continue;
      }

      for (auto n : ngrams)
      {
        if (seen_tokens.size() < n) continue;
        string s = boost::algorithm::join(seen_tokens | boost::adaptors::sliced(seen_tokens.size() - n, seen_tokens.size()), V.ngram_separator);
        (*D)[s].first++;
        doc_toks.emplace(s);
        total++;
      }
    }

    if (line_is_document)
    {
      for (const auto& tok : doc_toks)
        (*D)[tok].second++;
      doc_toks.clear();
    }
  }

  for (const auto& tok : doc_toks)
    (*D)[tok].second++;

  return total;
}
/// @endcond
