/**
 * @file
 * @brief Contain static definitions of class constants of classes in @ref program_options.h.
 */
#include <string>

#include <ycutils/program_options.h>

namespace ycutils {

/// @see ycutils::at_option_parser
const std::string at_option_parser::default_name = "response-file";

/// @see ycutils::at_option_parser
const std::string at_option_parser::default_prefix = "@";

}  // namespace ycutils
