/**
 * @file
 * @brief A collection of utility functions for doing vectorial math.
 */
#ifndef YCUTILS_VECMATH_H_INCLUDED
#define YCUTILS_VECMATH_H_INCLUDED

#include <unordered_map>

#include "ycutils.h"

using std::unordered_map;

using ycutils::index_type;

namespace ycutils {

/// Sparse vector type, which is essentially an \p std::unordered_map.
template <typename T = double>
using sparse_vector = unordered_map<index_type, T>;

/**
 * @brief Copies values element-wise between vectorial types using the \p [] operator.
 * @param d destination vector
 * @param s source vector
 * @param length size of source vector
 */
template <typename DestVector, typename SrcVector>
inline void vector_copy(DestVector& d, const SrcVector& s, const size_type length)
{
  for (index_type i = 0; i < length; ++i)
    d[i] = s[i];
}

}  // namespace ycutils

#endif  // YCUTILS_VECMATH_H_INCLUDED