/**
 * @file
 * @brief Helper methods for use with <a href="http://eigen.tuxfamily.org/">Eigen</a> library.
 */
#ifndef YCUTILS_EIGEN_H_INCLUDED
#define YCUTILS_EIGEN_H_INCLUDED

#if defined(USE_EIGEN) || defined(DOXYGEN)

#include "ycutils.h"

namespace ycutils {

}  // namespace ycutils

#endif  // USE_EIGEN || DOXYGEN

#endif  // YCUTILS_EIGEN_H_INCLUDED
