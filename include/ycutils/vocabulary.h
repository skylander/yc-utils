/**
 * @file
 * @brief Implementation of a \p ycutils::vocabulary class.
 *
 * THe \p vocabulary class handles loading and saving of a vocabulary file,
 * which is in an easy to read/consume TAB delimited format.
 */
#pragma once
#ifndef YCUTILS_VOCABULARY_H_INCLUDED
#define YCUTILS_VOCABULARY_H_INCLUDED

#include <fstream>
#include <functional>
#include <iostream>
#include <regex>
#include <stdexcept>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>

#include <boost/algorithm/string.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/algorithm_ext.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/range/irange.hpp>
#include <boost/serialization/access.hpp>
#include <boost/serialization/unordered_map.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/version.hpp>

#include "ycutils.h"
#include "collections.h"

namespace ycutils
{
  using std::cerr;
  using std::endl;

  using std::string;
  using std::vector;
  using std::pair;

  using boost::adaptors::indexed;

  /// Used to represent frequency counts, not unsigned because that maybe times when we want to represent negative values?
  using freq_type = long;

  /**
   * @brief Simple class to store details about a vcabulary type.
   * @details As of now, contains frequency and document frequency (no. of documents in which \p type has appeared in).
   */
  class type_info
  {
  public:
    /// Word
    string type;

    /// No. of times \p type has appeared in the corpus.
    freq_type freq;

    /// No. of documents in the corpus where \p type has appeared.
    freq_type doc_freq;

    /// Default constructor
    type_info() : type(""), freq(0), doc_freq(0) {}

    /// Constructor to populate all three values.
    type_info(const string& type, freq_type freq, freq_type doc_freq) : type(type), freq(freq), doc_freq(doc_freq) {}

  private:
    /// Support for boost::serialization
    friend class boost::serialization::access;

    /// \p boost::serialization function for \p ycutils::type_info.
    template <class Archive>
    inline void serialize(Archive & ar, const unsigned int version)
    {
      ar & type;
      ar & freq;
      ar & doc_freq;
    }
  };

  /**
   * @brief \p vocabulary are containers for storing types (words, phrases, etc)
   * along with their statistics.
   *
   * @details This implementation of \p ycutils::vocabulary stores the type,
   * along with corpus frequency and number of documents that it has appeared in.
   * It provides handy functions for saving and loading from a TAB delimited text
   * file, sorting lexicographically or by frequency, and quick pruning (by
   * using the app \p prunevocab).
   *
   * \p ycutils::vocabulary also supports set-like operation on the types in the
   * \p vocabulary.
   *
   * We store information about the vocabulary in 2 forms: as a list of types,
   * and as a hash mapping from the types to its index in the list. This ensures
   * that the index of the vocabulary is contiguous and provides a constant time
   * bi-directional mapping between types and indexes.
   *
   * This clas supports \p boost::serialization library through its \p vocabulary::serialize
   * method.
   */
  class vocabulary
  {
  public:
    /// A forward iterator to the \p vector types.
    using const_iterator = vector<type_info>::const_iterator;

    /// The separator between columns in the vocabulary file, which is set to " " (space).
    const string field_separator;

    /// The symbol for unknown tokens, set to \p __UNK__.
    const string unknown_symbol;

    /// The separator between tokens in n-gram types.
    const string ngram_separator;

    /// List of possible sorting order for vocabulary.
    static const vector<string> sort_order;

    /// Constructs an empty vocabulary.
    vocabulary(const string field_separator = " ", const string unknown_symbol = "__UNK__", const string ngram_separator = "_") :
      field_separator(field_separator),
      unknown_symbol(unknown_symbol),
      ngram_separator(ngram_separator),
      regex_vocab_line_(string("(.+?)") + field_separator + "(\\d+)" + field_separator + "(\\d+)")
    {
      V_list_.clear(); V_list_.reserve(expected_size_);
      V_map_.clear(); V_map_.reserve(expected_size_);
      to_remove_.clear();
    }

    /// Empty destructor.
    ~vocabulary()
    {
      V_list_.clear();
      V_map_.clear();
      to_remove_.clear();
    }

    /**
     * @brief Loads a vocabulary by reading it from \p filename.
     * @param filename to load vocabulary from
     */
    void load(const string& filename)
    {
      std::ifstream fin(filename);

      if (!fin.good())
        throw std::runtime_error("unable to open vocabulary file \"" + filename + "\"");

      load(fin);
      fin.close();
    }

    /**
     * @brief Loads a vocabulary by reading it from input stream \p in.
     * @param in reads vocabulary from this \p istream.
     */
    void load(std::istream& in)
    {
      if (!in.good())
        throw std::runtime_error("bad vocabulary std::istream");

      // clear vocabulary object, and set aside some size for expected amount of data
      V_list_.clear();
      V_list_.reserve(expected_size_);
      V_map_.clear();
      V_map_.reserve(expected_size_);
      to_remove_.clear();

      string line;

      while (line.empty())
        if (!std::getline(in, line))
          return;

      std::smatch what;
      std::function<void(const string&, index_type, type_info *)> parse_func;
      if (std::regex_match(line, what, regex_vocab_line_))
        parse_func = [&](const string& line_, index_type i_, type_info *tinfo_) { parse_vocab_line_with_columns_(regex_vocab_line_, line_, i_, tinfo_); };
      else
        parse_func = parse_vocab_line_without_columns_;

      index_type i = 0;
      type_info tinfo;
      do
      {
        if (line.empty())
          continue;

        parse_func(line, i, &tinfo);

        V_list_.push_back(tinfo);

        if (!V_map_.insert(std::make_pair(tinfo.type, i)).second)
        {
          cerr << "WARNING: duplicate token '" << tinfo.type << "' found in line " << i << " of vocabulary file; ignoring." << endl;
          continue;
        }

        i++;

        if (tinfo.freq < tinfo.doc_freq)
          cerr << "type '" << tinfo.type << "' in vocabulary file has smaller term frequency than document frequency." << endl;
        else if (tinfo.freq == 0)
          cerr << "type '" << tinfo.type << "' in vocabulary file has 0 term frequency." << endl;
      } while (std::getline(in, line));
    }

    /**
     * @brief Saves vocabulary object to \p filename.
     * @param filename write vocabulary here
     */
    inline void save(const string& filename) const
    {
      std::ofstream fout(filename, std::ios_base::out | std::ios_base::trunc);
      save(fout);
      fout.close();
    }

    /**
     * @brief Saves vocabulary object to output stream \p out.
     * @param out writes vocabulary to the ouput stream.
     */
    inline void save(std::ostream& out) const
    {
      for (const auto& tinfo : V_list_)
        write_line(out, tinfo);
    }

    /**
     * @brief Writes a line of vocabulary type info to \p ostream.
     * @param out \p std::ostream object to write to.
     * @param tinfo details about type.
     */
    inline void write_line(std::ostream& out, const type_info& tinfo) const
    {
      out << tinfo.type << field_separator << tinfo.freq << field_separator << tinfo.doc_freq << endl;
    }

    /**
     * @brief Rebuilds the vocabulary data structures after removing tokens.
     * @details Also called when you want to sort the vocabulary in a different manner.
     * @param sortby sorts the vocabulary by one of \p freq, \p lex or \p none.
     * @param keep_unknown_if_removed if \p vocabulary::unknown_symbol is found to be deleted, decide what to do with it.
     */
    void rebuild(const string sortby = "freq", const bool keep_unknown_if_removed = true)
    {
      vector<type_info> new_V_list;
      new_V_list.reserve(V_list_.size());

      // remove tokens
      for (auto it : V_list_ | indexed())
        if (to_remove_.find(it.index()) == to_remove_.end() || (it.value().type == unknown_symbol && keep_unknown_if_removed))
          new_V_list.push_back(std::move(it.value()));

      V_list_.clear(); V_map_.clear();
      to_remove_.clear();

      // sort the vocabulary
      if (sortby != "none")
      {
        // sort them by the key
        if (sortby == "lex")
          boost::sort(new_V_list, [&](type_info& a, type_info& b)
            {
              if (a.type == unknown_symbol) return true;
              if (b.type == unknown_symbol) return false;
              return a.type < b.type;
            });
        else
          boost::sort(new_V_list, [&](type_info& a, type_info& b)
            {
              if (a.type == unknown_symbol) return true;
              if (b.type == unknown_symbol) return false;
              return a.freq > b.freq;
            });
      }

      V_list_ = std::move(new_V_list);  // move it over!

      // rebuild the type -> index map
      V_map_.reserve(V_list_.size());
      for (const auto& it : V_list_ | indexed())
        V_map_[it.value().type] = it.index();
    }

    /**
     * @brief Return an random access iterator to the beginning of \p vocabulary.
     * @returns a \p const_iterator.
     */
    const_iterator begin() const { return V_list_.cbegin(); };

    /**
     * @brief Return an random access iterator to the end of \p vocabulary.
     * @returns a \p const_iterator.
     */
    const_iterator end() const { return V_list_.cend(); };

    /**
     * @brief Return the size of the vocabulary.
     * @returns vocabulary size.
     */
    std::size_t size() const { return V_list_.size(); }

    /**
     * @brief Return the type at position \p n.
     * @param n type position.
     * @returns \p type or throws an out of bounds exception.
     */
    const string& operator[] (index_type n) const { return V_list_[n].type; }

    /**
     * @brief Return the position of \p type.
     * @param type
     * @return index position.
     * @throw exception if \p type if not found
     */
    index_type operator[] (const string& type) const { return V_map_.at(type); }

    /**
     * @brief Returns \p true if \p type can be found in the vocabulary.
     * @param type \p type to look for
     * @return \p true if found
     */
    bool has_type(const string& type) const { return (V_map_.find(type) != V_map_.end()); }

    /**
     * @brief Return the corpus frequency of type at position \p n.
     * @param n position in the index.
     * @return the corpus frequency.
     * @throw out of bounds exception if bad \p n.
     */
    freq_type get_freq(index_type n) const { return V_list_[n].freq; }

    /**
     * @brief Return the frequency of \p type in the corpus.
     * @param type
     * @return the corpus frequency.
     * @throw exception if \p type not found.
     */
    freq_type get_freq(const string& type) const { return V_list_[V_map_.at(type)].freq; }

    /**
     * @brief Return the rank by frequency of type at position \p n.
     * @param n position in the index.
     * @return the corpus frequency rank (0 based).
     * @throw out of bounds exception if bad \p n.
     */
    freq_type get_freq_rank(index_type n) const { return (V_list_[0].type == unknown_symbol ? n - 1 : n); }

    /**
     * @brief Return the rank by frequency of \p type in the corpus.
     * @param type
     * @return the corpus frequency rank (0 based).
     * @throw exception if \p type not found.
     */
    freq_type get_freq_rank(const string& type) const { return get_freq_rank(V_map_.at(type)); }

    /**
     * @brief Return the number of documents that type (at position \p n) appears in.
     * @param n position in the index.
     * @return the number of documents.
     */
    freq_type get_doc_freq(index_type n) const { return V_list_[n].doc_freq; }

    /**
     * @brief Return the number of documents that \p type appears in.
     * @param type
     * @return the number of documents.
     */
    freq_type get_doc_freq(const string& type) const { return V_list_[V_map_.at(type)].doc_freq; }

    /**
     * @brief Adds a \p type and its frequency information to the vocabulary.
     * @details If \p type exists, the counts would be added to existing count and the function would return \p false. If \p type is new, it would be appended to the end of the vocabulary and \p true would be returned.
     * @param type the new vocabulary type to be added.
     * @param freq frequency of occurrence of type
     * @param doc_freq document frequency of type
     * @return \p true if a new entry is created.
     */
    bool add_type(const string& type, freq_type freq, freq_type doc_freq)
    {
      const auto it = V_map_.find(type);

      if (it == V_map_.end())  // never seen type, add it.
      {
        add_new_type_(type, freq, doc_freq);
        return true;
      }

      // if the type was previously removed, unremove it.
      const auto it_remove = to_remove_.find(it->second);
      if (it_remove != to_remove_.end())
        to_remove_.erase(it_remove);

      V_list_[it->second].freq += freq;
      V_list_[it->second].doc_freq += doc_freq;

      return false;
    }

    /**
     * @brief Adds a type specified in \p tinfo to the vocabulary.
     * @see vocabulary::add_type(const string& type, freq_type freq, freq_type doc_freq)
     */
    bool add_type(const type_info& tinfo)
    {
      return add_type(tinfo.type, tinfo.freq, tinfo.doc_freq);
    }

    /**
     * @brief Removes a type from the vocabulary.
     * @brief See \p vocabulary::remove_type.
     * @param i the type at position \p i will be removed from the vocabulary
     *
     * @note
     * Types will only be deleted after a call to \p vocabulary::rebuild.
     */
    void remove_type(index_type i) { to_remove_.insert(i); }

    /**
     * @brief See \p vocabulary::remove_type.
     * @param it iterator pointing to a single element to be removed from the vocabulary
     */
    void remove_type(const const_iterator& it) { remove_type(distance(this->begin(), it)); }

    /**
     * @brief See \p vocabulary::remove_type.
     * @param type \p type will be removed from the vocabulary
     */
    void remove_type(const string& type) { remove_type(V_map_.at(type)); }

    /**
     * @brief Vocabulary intersection.
     * @details Vocabulary will consist of types that are found in both \p *this and \p rhs. Frequencies will be the sum from both sides.
     *
     * @param rhs right hand side vocabulary.
     */
    vocabulary& operator&=(const vocabulary& rhs)
    {
      for (const auto it : V_list_ | indexed())
      {
        const auto it_rhs = rhs.V_map_.find(it.value().type);
        const auto i = it.index();

        if (it_rhs == rhs.V_map_.end())
          this->remove_type(i);  // remove if not found in rhs
        else
        {
          V_list_[i].freq += rhs.V_list_[it_rhs->second].freq;
          V_list_[i].doc_freq += rhs.V_list_[it_rhs->second].doc_freq;
        }
      }

      return *this; // return the result by reference
    }

    /**
     * @brief Vocabulary union.
     * @details Vocabulary will consist of types that are found in either \p *this or \p rhs. Frequencies will be the sum from both sides.
     *
     * @param rhs right hand side vocabulary.
     */
    vocabulary& operator+=(const vocabulary& rhs)
    {
      for (const auto rhs_tinfo : rhs.V_list_)
        add_type(rhs_tinfo);

      return *this; // return the result by reference
    }

    /**
     * @brief Vocabulary difference.
     * @details Vocabulary will consist of types that are found in \p *this but not in \p rhs. Frequencies will not be changed.
     *
     * @param rhs right hand side vocabulary.
     */
    vocabulary& operator-=(const vocabulary& rhs)
    {
      for (const auto it : V_list_ | indexed())
        if (rhs.has_type(it.value().type))  // if rhs guy has it, we take it out of our system
          this->remove_type(it.index());

      return *this; // return the result by reference
    }

    /**
     * @brief Vocabulary symmetric difference.
     * @details Vocabulary will consist of types that are found in exclusively in \p *this or \p rhs. Frequencies will not be changed..
     *
     * @param rhs right hand side vocabulary.
     */
    vocabulary& operator/=(const vocabulary& rhs)
    {
      for (const auto it_rhs : rhs.V_list_ | indexed())
      {
        const auto it = V_map_.find(it_rhs.value().type);

        if (it == V_map_.end())
          add_new_type_(it_rhs.value().type, rhs.V_list_[it_rhs.index()].freq, rhs.V_list_[it_rhs.index()].doc_freq);
        else
          this->remove_type(it->second);
      }

      return *this; // return the result by reference
    }

  private:
    /// Support for boost::serialization
    friend class boost::serialization::access;

    /// Private static member function for parsing vocabulary lines (that are not in column format)
    static inline void parse_vocab_line_without_columns_(const string& line, index_type i, type_info* tinfo)
    {
      tinfo->type = line;
      tinfo->freq = 1;
      tinfo->doc_freq = 1;
    }

    /// Private static member function for parsing vocabulary lines (that are in column format)
    static inline void parse_vocab_line_with_columns_(const std::regex& R, const string& line, index_type i, type_info* tinfo)
    {
      std::smatch what;  // parse line using regex
      if (!std::regex_match(line, what, R))
        throw std::runtime_error("bad line format in line " + std::to_string(i) + " of vocabulary file.");

      tinfo->type = what[1];
      tinfo->freq = std::stol(what[2]);
      tinfo->doc_freq = std::stol(what[3]);
    }

    /// Private method to add a new type directly without checking. Use with care.
    inline void add_new_type_(const string& type, freq_type freq, freq_type doc_freq)
    {
      V_list_.push_back(type_info(type, freq, doc_freq));
      V_map_[type] = V_list_.size() - 1;
    }

    /// \p boost::serialization function for \p ycutils::vocabulary.
    template <class Archive>
    inline void serialize(Archive & ar, const unsigned int version)
    {
        ar & V_list_;
        ar & V_map_;
    }

    /// Estimate the size of the vocabulay for efficient \p vector allocation.
    static constexpr size_type expected_size_ = 30000;

    /// Regex expression for parsing each TAB separated line of the vocabulary file.
    const std::regex regex_vocab_line_;

    /// We store our types as a \p vector of \p vocabulary::type_info for speedy random access.
    vector<type_info> V_list_;

    /// Type -> index mappings stored in an \p unordered_map.
    std::unordered_map<string, index_type> V_map_;

    /// Keeps a temporary list of indexes that are going to be removed during \p vocabulary::rebuild.
    std::unordered_set<index_type> to_remove_;
  };

}  // namespace ycutils

/// Sets version of class for \p boost::serialization.
BOOST_CLASS_VERSION(ycutils::vocabulary, 1);

/// Sets version of class for \p boost::serialization.
BOOST_CLASS_VERSION(ycutils::type_info, 1);

#endif  // YCUTILS_VOCABULARY_H_INCLUDED
