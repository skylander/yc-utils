#!/bin/sh

JAVA_EXEC=`which java`
BASEDIR=$(dirname $0)

CLASSPATH=${BASEDIR}/../lib/stanford-corenlp-3.5.0.jar:${BASEDIR}/../lib/commons-cli-1.2.jar:${BASEDIR}/../lib/yc-utils.jar:${BASEDIR}/../lib/corenlp-models.jar
JAVA_FLAGS="-Xms256m -Xmx2048m -XX:+UseParallelOldGC -XX:ParallelGCThreads=2"

${JAVA_EXEC} ${JAVA_FLAGS} -cp ${CLASSPATH} edu.yc.nlp.StanfordCoreNLPWrapper "$@"
