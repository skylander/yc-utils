# yc-utils -- Collection of NLP and data processing utilities

This is an assortment of utilities / functions that I have written and found useful for data manipulation and NLP.
It is a mismash of scripts, libraries, modules, headers, etc written in various languages.

## Getting started

### Requirements

To run Python scripts, you will need:

- [Python 2.7](https://www.python.org/downloads/) (>= 2.7.3)

To use the Java packages tools, you will need

- [Java SE 7](http://www.oracle.com/technetwork/java/javase/downloads/index.html) or better

To use the C++ headers and compile the code, you will need:

- [GCC 4.9.1](https://gcc.gnu.org/) -- All development work has been done with this compiler. The C++ codebase uses mostly standard C++11 (I think?) techniques, so it could possibly work with other C++11 compilers as well; your mileage may vary.
- [Boost libraries](http://www.boost.org/) (>=1.56.0)


## Applications and scripts

I have implemented a few applications and scripts for common data handling tasks.

### For manipulating vocabularies

- [buildvocab](@ref buildvocab.cpp) -- App for building n-gram vocabulary files from tokenized text.
- [prunevocab](@ref prunevocab.cpp) -- App for pruning vocabulary files created by [buildvocab](@ref buildvocab.cpp).
- [mergevocab](@ref mergevocab.cpp) -- App for merging multiple vocabulary files created by [buildvocab](@ref buildvocab.cpp).

### Data processing related utilities

- [samplelines](@ref samplelines.cpp) -- Utility for rapidly sampling lines from a large file.
- [shufflelines](@ref shufflelines.cpp) -- Utility for shuffling lines from a large file.
- [splitlines](@ref splitlines.cpp) -- Quick utility for splitting into cross validation or training/dve/test sets.

### Shell scripts and wrapper code

- [corenlpwrapper.sh](@ref StanfordCoreNLPWrapper.java) -- A Java wrapper around [Stanford CoreNLP](http://nlp.stanford.edu/software/corenlp.shtml) for tokenizing, sentence splitting, lemmatizing, POS tagging and NER labeling. You can run this using the bash shell script [`corenlpwrapper.sh`](@ref StanfordCoreNLPWrapper.java).
- [phrasinator.py](@ref phrasinator.py) -- Phrasinator is a Python application that extracts phrases from text, where phrases are sequence of tokens whose POS tags are of the form <tt>(ADJ*)(NN+)</tt>.

## Libraries and packages

### C++ headers

Several C++ header only source code are available, so compiling and linking libraries are not required.

- [collections.h](@ref collections.h) -- Suite of C++ functions for working with collections.
- [ioutils.h](@ref ioutils.h) -- A collection of utility functions for handling I/O reading and writing.
- [fastmath.h](@ref fastmath.h) -- A wrapper of exponentials and logarithm functions using high performance math libraries.
- [math.h](@ref math.h) -- A collection of utility functions for doing math.
- [eigen.h](@ref eigen.h) -- Helper methods for use with [Eigen library](http://eigen.tuxfamily.org/).
- [feature_map.h](@ref feature_map.h) -- Implements the two-way feature mapping class.
- [lbfgs.h](@ref lbfgs.h) -- Helper methods for use with [Naoaki Okazaki's libLBFGS](http://www.chokkan.org/software/liblbfgs/).
- [mathtables.h](@ref mathtables.h) -- Performs fast exponentials and logarithmic operations by pre-computing their values.
- [metropolis_hastings.h](@ref metropolis_hastings.h) -- Implementation of the Metropolis-Hastings algorithm.
- [program_options.h](@ref program_options.h) -- Utility methods for dealing with `boost::program_options`.
- [random.h](@ref random.h) -- Helper functions for dealing with random number generation.
- [sampling.h](@ref sampling.h) -- A collections of functions for handling sampling.
- [stopwords.h](@ref stopwords.h) -- Utility functions for managing stopwords.
- [vecmath.h](@ref vecmath.h) -- A collection of utility functions for doing vectorial math.
- [vocabulary.h](@ref vocabulary.h) -- Implementation of a `vocabulary` -- includes saving/load to/from files, set-like operations, etc.

### C++ library

The use of some functions will require linking against `ycutils` library. They are

- [vocabulary.cpp](@ref vocabulary.cpp) due to use of class static constants.

### Python modules

Some Python modules that I wrote for quick and dirty data processing tasks.

- [corenlpwrapper.py](@ref corenlpwrapper.py) -- A Python module that interfaces with @ref StanfordCoreNLPWrapper.java.

## Other included libraries that are not mine

- [Stanford CoreNLP 3.5.0](http://nlp.stanford.edu/software/corenlp.shtml) -- Stanford CoreNLP provides a set of natural language analysis tools written in Java. Used by [corenlpwrapper.sh](@ref StanfordCoreNLPWrapper.java), [phrasinator.py](@ref phrasinator.py) and [corenlpwrapper.py](@ref corenlpwrapper.py).
- [Splitta 0.1.0](https://code.google.com/p/splitta/) -- Statistical sentence boundary detection library by Dan Gilick at Google. Used by [phrasinator.py](@ref phrasinator.py) and [corenlpwrapper.py](@ref corenlpwrapper.py).
- [Apache Commons CLI 1.2](http://commons.apache.org/proper/commons-cli/) -- The Apache Commons CLI library provides an API for parsing command line options passed to programs. Used by [StanfordCoreNLPWrapper](@ref StanfordCoreNLPWrapper.java).

## Future

See [TODOs](@ref todo).
