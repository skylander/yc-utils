/**
 * @file
 * @brief Quick utility for splitting into training/test sets.
 *
 * This is a generic tool for splitting a file by lines. There are options for
 * splitting by proportions or splitting into multiple training/testing sets for
 * cross validation. Other useful options include shuffling the lines before
 * splitting, and assigning lines to output files in a round robin manner.
 *
 * @verbatim
Usage: splitlines [options]

Split a file by lines according to proportions given. Proportions can be specified in any form, it will be normalized to the number of lines in the file automatically.

Allowed options:
  -h [ --help ]                         produce help message
  -o [ --input ] file (=STDIN)          read input lines from here
  -o [ --output ] split_file (=output1 output2 ...)
                                        save split files here
  -x [ --xvalidate ] K                  split the data for cross validation,
                                        output files will be of the form
                                        'outputX.train' and 'outputX.test'
  -p [ --proportions ] p1 p2 ... (=1/N 1/N ...)
                                        proportion to split file by (defaults
                                        to equal proportions)
  -s [ --shuffle ]                      shuffle file before splitting
  -r [ --round-robin ]                  assign lines to output files by round
                                        robin order
@endverbatim
@cond
 */
#include <cmath>
#include <cstdlib>

#include <iostream>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>

#include <boost/algorithm/string.hpp>
#include <boost/program_options.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/range/numeric.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/timer/timer.hpp>

#include <ycutils/collections.h>
#include <ycutils/ioutils.h>
#include <ycutils/program_options.h>

using std::cerr;  // usings for iostream
using std::cin;
using std::cout;
using std::endl;

using std::string;  // using for STL types
using std::vector;
using std::pair;
using std::unordered_map;

void split_lines(std::istream&, const vector<string>&, const vector<double>&, ycutils::size_type, bool, bool);
void write_xvalidate_lines(const string&, const vector<string>&, ycutils::size_type, bool);
void write_lines_normal(const vector<string>&, const vector<string>&, const vector<unsigned long>&);
void write_lines_round_robin(const vector<string>&, const vector<string>&, const vector<unsigned long>&);

int main(int argc, const char **argv)
{
  namespace po = boost::program_options;
  po::options_description app_options("Usage: " + string(argv[0]) + " [options] \n\nSplit a file by lines according to proportions given. Proportions can be specified in any form, it will be normalized to the number of lines in the file automatically.\n\nAllowed options");

  vector<string> output_filenames;
  string output_name;

  app_options.add_options()
    ("help,h", "produce help message")
    ("input,i", po::value<ycutils::input_file_argument>()->default_value(ycutils::input_file_argument(), "STDIN")->value_name("file"), "read input lines from here")
    ("output,o", po::value<vector<string> >(&output_filenames)->default_value(vector<string>(), "<name>1 <name>2 ...")->value_name("split_file")->multitoken(), "save split files here")
    ("name,n", po::value<string>(&output_name)->default_value("output")->value_name("name")->multitoken(), "base name to use for output files (if '--output' is not specified)")
    ("xvalidate,x", po::value<ycutils::positive_argument<ycutils::size_type> >()->value_name("K"), "split the data for cross validation, output files will be of the form '<name>X.train' and '<name>X.test'")
    ("proportions,p", po::value<ycutils::positive_arguments<double> >()->default_value(ycutils::positive_arguments<double>(), "1/N 1/N ...")->value_name("p1 p2 ...")->multitoken(), "proportion to split file by (defaults to equal proportions)")
    ("shuffle,s", "shuffle file before splitting")
    ("round-robin,r", "assign lines to output files by round robin order");

  po::positional_options_description pos_options;
  pos_options.add("input", 1);
  pos_options.add("proportions", -1);

  po::variables_map vm;
  ycutils::size_type split_count;
  ycutils::size_type xvalidate = 0;
  vector<double> proportions;

  try
  {
    po::store(po::command_line_parser(argc, argv).options(app_options).positional(pos_options).run(), vm);

    if (vm.count("help") || argc == 1)
    {
      cerr << app_options << "\n";
      return 1;
    }

    po::notify(vm);

    boost::copy(ycutils::extract_arguments(vm["proportions"].as<ycutils::positive_arguments<double> >()), std::back_inserter(proportions));
    if (vm.count("xvalidate") > 0)
      xvalidate = vm["xvalidate"].as<ycutils::positive_argument<ycutils::size_type> >().value();

    if (xvalidate > 0)
    {
      if (!proportions.empty())
        throw std::runtime_error("you cannot use '--proportions' with '--xvalidate'");
      else if (!output_filenames.empty() and output_filenames.size() != 1)
        throw std::runtime_error("number of arguments in '--output' should be at most 1");
    }
    else
    {
      split_count = output_filenames.empty() ? proportions.size() : output_filenames.size();

      if (split_count <= 1)
        throw std::runtime_error("you need to specify at least 2 arguments for either '--output' or '--proportions'");
      else if (!output_filenames.empty() and !proportions.empty() and output_filenames.size() != proportions.size())
        throw std::runtime_error("the number of arguments of '--output' and '--proportions' are different");
    }
  } catch (std::exception &e)
  {
    cerr << e.what() << endl << "Type " << argv[0] << " --help for usage information." << endl;
    return EXIT_FAILURE;
  }

  if (xvalidate > 0)
  {
    if (output_filenames.empty())
      output_filenames.push_back(output_name);
  }
  else
  {
    if (output_filenames.empty())
    {
      auto width = std::to_string(split_count).size();
      boost::transform(boost::irange((ycutils::size_type)1, split_count + 1), std::back_inserter(output_filenames),
        [&](ycutils::size_type i)
        {
          string i_string = std::to_string(i);
          i_string.insert(0, width - i_string.size(), '0');
          return string(output_name) + i_string;
        });
    }

    if (proportions.empty())
      proportions.assign(split_count, 1);
  }

  bool shuffle_file = vm.count("shuffle") != 0;
  bool round_robin = vm.count("round-robin") != 0;

  auto input_filename = vm["input"].as<ycutils::input_file_argument>().value();
  if (input_filename.empty())
  {
    cerr << "Reading lines from STDIN...";
    split_lines(cin, output_filenames, proportions, xvalidate, shuffle_file, round_robin);
  }
  else
  {
    cerr << "Reading lines from \"" << input_filename << "\"...";
    std::ifstream ifs(input_filename);
    split_lines(ifs, output_filenames, proportions, xvalidate, shuffle_file, round_robin);
    ifs.close();
  }

  return EXIT_SUCCESS;
}

void split_lines(std::istream& in, const vector<string>& out_fnames, const vector<double>& p, ycutils::size_type xvalidate, bool shuffle, bool round_robin)
{
  vector<string> lines;
  boost::timer::cpu_timer timer_;

  lines.reserve(ycutils::ioutils::expected_num_lines);
  ycutils::read_lines_into_vector_with_progress(in, std::back_inserter(lines), cerr);
  cerr << ". " << lines.size() << " lines read. (took " << timer_.format(2, "%t") << " seconds)." << endl;
  timer_.start();

  if (shuffle)
  {
    cerr << "Shuffling lines...";
    std::random_shuffle(lines.begin(), lines.end());
    cerr << "done! (took " << timer_.format(2, "%t") << " seconds)." << endl;
  }

  if (xvalidate > 0)
  {
    write_xvalidate_lines(out_fnames[0], lines, xvalidate, round_robin);
  }
  else
  {
    auto p_sum = boost::accumulate(p, 0.0);
    vector<unsigned long> p_lines;
    boost::transform(p, std::back_inserter(p_lines), [&](double p_) { return (unsigned long) std::ceil(p_ * ((double)lines.size() / p_sum)); });
    p_lines.back() -= boost::accumulate(p_lines, 0) - lines.size();  // readjust the last items length so that the number of lines sum up correctly
    BOOST_ASSERT(boost::accumulate(p_lines, 0.0) == lines.size());

    if (round_robin)
      write_lines_round_robin(out_fnames, lines, p_lines);
    else
      write_lines_normal(out_fnames, lines, p_lines);

    cerr << lines.size() << " lines have been written to " << out_fnames.size() << " files." << endl;
  }
}

void write_xvalidate_lines(const string& name, const vector<string>& lines, ycutils::size_type xvalidate, bool round_robin)
{
  cerr << "Writing " << lines.size() << " lines for " << xvalidate << "-fold cross validation (" << (round_robin ?  "round robin" : "sequential") << " style)..." << endl;

  const auto format_width = std::to_string(xvalidate).size();

  vector<unsigned long> fold_sizes(xvalidate + 1);
  for (auto k : ycutils::irange(xvalidate))
    fold_sizes[k] = k * std::floor(lines.size() / xvalidate);
  fold_sizes[xvalidate] = lines.size();

  boost::timer::cpu_timer timer_;

  for (auto k : ycutils::irange(xvalidate))
  {
    string k_string = std::to_string(k + 1);
    k_string.insert(0, format_width - k_string.size(), '0');

    const string train_fname = name + k_string + ".train";
    const string test_fname = name + k_string + ".test";
    std::ofstream f_train(train_fname);
    std::ofstream f_test(test_fname);

    if (!f_train.good())
      throw std::runtime_error("unable to open " + train_fname + " for writing.");
    if (!f_test.good())
      throw std::runtime_error("unable to open " + test_fname + " for writing.");

    unsigned long train_count = 0, test_count = 0;

    for (auto it_lines : lines | boost::adaptors::indexed(0))
    {
      bool test_line = false;
      if (round_robin)
        test_line = (it_lines.index() % xvalidate == k);
      else
        test_line = ((ycutils::size_type) it_lines.index() >= fold_sizes[k] and (ycutils::size_type) it_lines.index() < fold_sizes[k + 1]);

      if (test_line)
      {
        test_count++;
        f_test << it_lines.value() << endl;
      }
      else
      {
        train_count++;
        f_train << it_lines.value() << endl;
      }
    }

    f_train.close();
    f_test.close();

    cerr << "Done writing " << train_count << " lines to \"" << train_fname << "\" and " << test_count << " lines to \"" << test_fname << "\" (took " << timer_.format(2, "%t") << " seconds)." << endl;
  }
}

void write_lines_round_robin(const vector<string>& out_fnames, const vector<string>& lines, const vector<unsigned long>& p_lines)
{
  cerr << "Writing " << lines.size() << " lines to files round-robin style..." << endl;

  boost::timer::cpu_timer timer_;

  vector<std::ofstream> fouts(out_fnames.size());
  for (const auto it : out_fnames | boost::adaptors::indexed(0))
    fouts[it.index()].open(it.value());

  vector<pair<ycutils::size_type, unsigned long> > lines_left;
  for (auto it : p_lines | boost::adaptors::indexed(0))
    if (it.value() > 0)
      lines_left.push_back(std::make_pair(it.index(), it.value()));
    else
      cerr << "Done writing 0 lines to \"" << out_fnames[it.index()] << "\"." << endl;

  for (auto it_lines : lines | boost::adaptors::indexed(0))
  {
    BOOST_ASSERT(lines_left.size() > 0);

    auto k = it_lines.index() % lines_left.size();
    auto f_index = lines_left[k].first;

    fouts[f_index] << it_lines.value() << endl;
    if (--lines_left[k].second == 0)
    {
      cerr << "Done writing " << p_lines[f_index] <<  " lines to \"" << out_fnames[f_index] << "\" (took " << timer_.format(2, "%t") << " seconds)." << endl;
      lines_left.erase(lines_left.begin() + k);
    }
  }
  BOOST_ASSERT(lines_left.empty());

  for (auto& fout : fouts)
    fout.close();
}

void write_lines_normal(const vector<string>& out_fnames, const vector<string>& lines, const vector<unsigned long>& p_lines)
{
  auto it_lines = lines.begin();
  for (auto i = 0u; i < p_lines.size(); ++i)
  {
    boost::timer::auto_cpu_timer timer_(cerr, 2, " (took %t seconds)\n");
    cerr << "Writing " << p_lines[i] << " lines to \"" << out_fnames[i] << "\"...";

    std::ofstream fout(out_fnames[i]);
    for (auto j = 1u; j <= p_lines[i]; ++j)
    {
      BOOST_ASSERT(it_lines != lines.end());
      fout << *(it_lines++) << endl;
      ycutils::report_progress(j, cerr);
    }
    fout.close();

    cerr << "done!";
  }
}
/// @endcond
