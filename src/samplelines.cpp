/**
 * @file
 * @brief Utility for rapidly sampling lines from a large file.
 *
 * This app uses reservoir sampling / bernoulli trials sampling to sample on the fly from a large file. Includes ability to generate multiple different set of samples in a single pass of the input file.
 *
 * @verbatim
Usage: src/samplelines [options]

Sample lines from a file using reservoir sampling. We can adjust the sampling mechanism by setting the sampling rate 'r' in several ways: If 0 < r < 1, we sample using a bernoulli trial of probability r for each line; if r >= 1, we sample exactly r lines from input.

Allowed options:
  -h [ --help ]                  produce help message
  -i [ --input ] file (=STDIN)   read input lines from here
  -o [ --output ] file (=STDOUT) save sample files here
  -r [ --sampling-rate ] r       sampling rate for each output file (see usage
                                 information for details on r)
@endverbatim
@cond
 */
#include <cstdlib>
#include <ctime>

#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include <boost/program_options.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_int_distribution.hpp>
#include <boost/random/uniform_01.hpp>
#include <boost/random/variate_generator.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/timer/timer.hpp>

#include <ycutils/ioutils.h>
#include <ycutils/program_options.h>

namespace po = boost::program_options;

using std::cerr;  // usings for iostream
using std::cin;
using std::cout;
using std::endl;

using std::string;  // using for STL types
using std::vector;

using ycutils::size_type;

void sample_lines(std::istream&, const vector<string>&, const vector<double>&);

int main(int argc, const char **argv)
{
  namespace po = boost::program_options;

  po::options_description app_options("Usage: " + std::string(argv[0]) + " [options] \n\nSample lines from a file using reservoir sampling. We can adjust the sampling mechanism by setting the sampling rate 'r' in several ways: If 0 < r < 1, we sample using a bernoulli trial of probability r for each line; if r >= 1, we sample exactly r lines from input.\n\nAllowed options");

  vector<string> output_filenames;

  app_options.add_options()
    ("help,h", "produce help message")
    ("input,i", po::value<ycutils::input_file_argument>()->default_value(ycutils::input_file_argument(), "STDIN")->value_name("file"), "read input lines from here")
    ("output,o", po::value<vector<string> >(&output_filenames)->default_value(vector<string>(), "STDOUT")->value_name("file")->multitoken(), "save sample files here")
    ("sampling-rate,r", po::value<ycutils::positive_arguments<double> >()->value_name("r")->multitoken()->required(), "sampling rate for each output file (see usage information for details on r)");

  po::positional_options_description pos_options;
  pos_options.add("input", 1);
  pos_options.add("sampling-rate", -1);

  po::variables_map vm;
  vector<double> sampling_rates;

  try
  {
    po::store(po::command_line_parser(argc, argv).options(app_options).positional(pos_options).run(), vm);

    if (vm.count("help"))
    {
      cerr << app_options << "\n";
      return 1;
    }

    po::notify(vm);

    ycutils::extract_arguments(vm["sampling-rate"].as<ycutils::positive_arguments<double> >(), std::back_inserter(sampling_rates));

    if (output_filenames.empty() and sampling_rates.size() != 1)
      throw std::logic_error("you need to specify output files for '--sampling-rate' > 1");
    else if (!output_filenames.empty() and sampling_rates.size() > 1 and output_filenames.size() != sampling_rates.size())
      throw std::logic_error("number of arguments mismatch between '--output' and '--sampling-rate'");
  }
  catch (std::exception& e)
  {
    cerr << e.what() << endl << "Type " << argv[0] << " --help for usage information." << endl;
    return EXIT_FAILURE;
  }

  auto input_filename = vm["input"].as<ycutils::input_file_argument>().value();
  if (input_filename.empty())
  {
    cerr << "Reading lines from STDIN...";
    sample_lines(cin, output_filenames, sampling_rates);
  }
  else
  {
    cerr << "Reading lines from \"" << input_filename << "\"...";
    std::ifstream ifs(input_filename);
    sample_lines(ifs, output_filenames, sampling_rates);
    ifs.close();
  }

  return EXIT_SUCCESS;
}

void sample_lines(std::istream& in, const vector<string>& output_filenames, const vector<double>& sampling_rates)
{
  using int_variate_generator = boost::random::variate_generator<boost::random::mt19937, boost::random::uniform_int_distribution<size_type> >;
  using u01_variate_generator = boost::random::variate_generator<boost::random::mt19937, boost::random::uniform_01<> >;

  boost::random::mt19937 rng(static_cast<unsigned int>(std::time(0)));
  vector<int_variate_generator> unif_int_dist;

  vector<size_type> sample_count(sampling_rates.size(), 0);
  vector<vector<string>> sampled_lines(sampling_rates.size());

  for (auto it : sampling_rates | boost::adaptors::indexed())
  {
    if (it.value() >= 1.0)
    {
      sampled_lines[it.index()].resize((size_type) it.value());
      sample_count[it.index()] = (size_type) it.value();
      unif_int_dist.push_back(int_variate_generator(rng, boost::random::uniform_int_distribution<size_type>(0, sample_count[it.index()] - 1)));
    }
    else
      unif_int_dist.push_back(int_variate_generator(rng, boost::random::uniform_int_distribution<size_type>(0, 0)));
  }

  // prepare files for writing
  std::ostream *outs[sampling_rates.size()];
  if (output_filenames.empty())
    outs[0] = &cout;
  else
    boost::transform(output_filenames, outs, [](const string& filename) { return new std::ofstream(filename); });

  boost::timer::cpu_timer timer_;
  u01_variate_generator unif_dist(rng, boost::random::uniform_01<>());

  size_type lines_count = 1;
  for (string line; std::getline(in, line); ++lines_count)
  {
    ycutils::report_progress(lines_count, cerr);

    for (auto it : sampling_rates | boost::adaptors::indexed())
    {
      if (it.value() < 1.0)  // standard bernoulli sampling
      {
        if (unif_dist() < it.value())  // bernoulli
        {
          *(outs[it.index()]) << line << endl;
          sample_count[it.index()]++;
        }
      }
      else
      {
        if (sample_count[it.index()] >= lines_count)  // reservoir sampling for k items
          sampled_lines[it.index()][lines_count - 1] = line;
        else if (unif_dist() * lines_count < sample_count[it.index()])  // only accept if unif_dist() < k / i
          sampled_lines[it.index()][unif_int_dist[it.index()]()] = line;  // replace one of the items randomly
      }
    }
  }
  cerr << ". " << --lines_count << " lines read. (took " << timer_.format(2, "%t") << " seconds)." << endl;

  for (auto it : sampling_rates | boost::adaptors::indexed())
  {
    for (auto& line : sampled_lines[it.index()])
      *(outs[it.index()]) << line << endl;

    sampled_lines[it.index()].clear();

    if (outs[it.index()] != &cout)
    {
      ((std::ofstream *) outs[it.index()])->close();
      delete outs[it.index()];
    }

    cerr << sample_count[it.index()] << " lines written to " << (output_filenames.empty() ? "STDOUT" : "\"" + output_filenames[it.index()] + "\"") << "." << endl;
  }
}
/// @endcond
