/**
 * @file
 * @brief Helper methods for use with <a href="http://www.chokkan.org/software/liblbfgs/">Naoaki Okazaki's libLBFGS</a>.
 * @todo Add liblbfgs source code to repository.
 */
#ifndef YCUTILS_LBFGS_H_INCLUDED
#define YCUTILS_LBFGS_H_INCLUDED

#include <iostream>

#include "ycutils.h"
#include "ycutils/collections.h"

#ifdef USE_EIGEN
#include <Eigen/SparseCore>
#endif

#include <lbfgs.h>

#include "vecmath.h"

namespace ycutils {

/// Overloadable instance class for use in \p lbfgs() to easily track progress and instance variables.
class lbfgs_instance
{
public:
  /// Default constructor.
  lbfgs_instance() : _progress_os(nullptr), _progress_func(nullptr) {}

  /**
   * @brief Constructs an \p lbfgs_instance object with \p std::ostream.
   * @param os output stream for progress updates
   */
  lbfgs_instance(std::ostream *os) : _progress_os(os), _progress_func(nullptr) {}

  /**
   * @brief Constructs an \p lbfgs_instance object with a progress callback.
   * @param func callback for progress updates (called after displaying progress)
   */
  lbfgs_instance(lbfgs_progress_t func) : _progress_func(func) {}

  /**
   * @brief Constructs an \p lbfgs_instance object with a progress callback.
   * @param os output stream for progress updates
   * @param func callback for progress updates (called after displaying progress)
   */
  lbfgs_instance(std::ostream *os, lbfgs_progress_t func) : _progress_os(os), _progress_func(func) {}

  /// Default destructor
  ~lbfgs_instance() {}

  /// Set the function pointer to the progress function.
  void progress_func(lbfgs_progress_t progress_func) { _progress_func = progress_func; }

  /// Get the function pointer to the current progress function.
  const lbfgs_progress_t progress_func() const { return _progress_func; }

  /**
   * @brief \p ycutils::lbfgs_progress_t callback function for \p lbfgs() that uses the \p std::ostream \p ycutils::lbfgs_instance::_progress_os.
   * @details If \p lbfgs_instance::_progress_func is defined, this function will call \p lbfgs_instance::_progress_func after displaying progress output.
   * @see _progress_func
   * @param instance The user data sent for \p lbfgs() function by the client.
   * @param x The current values of variables.
   * @param g The current gradient values of variables.
   * @param fx The current value of the objective function.
   * @param xnorm The Euclidean norm of the variables.
   * @param gnorm The Euclidean norm of the gradients.
   * @param step The line-search step used for this iteration.
   * @param n The number of variables.
   * @param k The iteration count.
   * @param ls The number of evaluations called for this iteration.
   * @return Zero to continue the optimization process. Returning a non-zero value will cancel the optimization process.
   * @note If specified, we will return the value of \p lbfgs_instance::_progress_func.
   */
  static int progress(void *instance, const lbfgsfloatval_t *x, const lbfgsfloatval_t *g, const lbfgsfloatval_t fx, const lbfgsfloatval_t xnorm, const lbfgsfloatval_t gnorm, const lbfgsfloatval_t step, int n, int k, int ls)
  {
    std::ostream *os = &std::cerr;
    auto p = static_cast<lbfgs_instance *>(instance);

    if (p && p->_progress_os)
      os = p->_progress_os;

    *os << "iter=" << k << ", fx=" << fx << ", xnorm=" << xnorm << ", gnorm=" << gnorm << ", step=" << step << ", evals=" << ls << std::endl;

    if (p && p->_progress_func)
      return (*p->_progress_func)(instance, x, g, fx, xnorm, gnorm, step, n, k, ls);

    return 0;
  }

protected:
  /// \p std::ostream for \p lbfgs() displaying progress.
  std::ostream *_progress_os;

  /// \p ycutils::lbfgs_instance::_progress_func(...) will be called right after \p ycutils::lbfgs_instance::progress displays the progress.
  lbfgs_progress_t _progress_func;
};

/// A wrapper around LBFGS instances that keeps track of LBFGS progress by recording details such as iterations, function values, \p x and gradient norms.
class lbfgs_progress_tracker
{
public:
  /// Constructs a \p ycutils::lbfgs_progress_tracker with no instance and progress function.
  lbfgs_progress_tracker() : _instance(nullptr), _progress_func(nullptr) { reset(); }

  /// Construct a \p ycutils::lbfgs_progress_tracker with instance.
  lbfgs_progress_tracker(void *instance) : _instance(instance), _progress_func(nullptr) { reset(); }

  /// Construct a \p ycutils::lbfgs_progress_tracker with instance and progress fucntion.
  lbfgs_progress_tracker(void *instance, lbfgs_progress_t progress_func) : _instance(instance), _progress_func(progress_func) { reset(); }

  /**
   * @brief \p ycutils::lbfgs_progress_tracker callback function for \p lbfgs(...) that keeps track of the last seen \p fx, \p xnorm, \p gnorm and iteration values.
   * @details If \p lbfgs_instance::_progress_func is specified, this function will call \p lbfgs_progress_tracker::_progress_func after.
   * @see _progress_func
   * @param instance The user data sent for \p lbfgs() function by the client.
   * @param x The current values of variables.
   * @param g The current gradient values of variables.
   * @param fx The current value of the objective function.
   * @param xnorm The Euclidean norm of the variables.
   * @param gnorm The Euclidean norm of the gradients.
   * @param step The line-search step used for this iteration.
   * @param n The number of variables.
   * @param k The iteration count.
   * @param ls The number of evaluations called for this iteration.
   * @return Zero to continue the optimization process. Returning a non-zero value will cancel the optimization process.
   * @note If specified, we will return the value of \p lbfgs_progress_tracker::_progress_func.
   */
  static int progress(void *instance, const lbfgsfloatval_t *x, const lbfgsfloatval_t *g, const lbfgsfloatval_t fx, const lbfgsfloatval_t xnorm, const lbfgsfloatval_t gnorm, const lbfgsfloatval_t step, int n, int k, int ls)
  {
    auto tracker_instance = static_cast<lbfgs_progress_tracker *>(instance);

    tracker_instance->_fx = fx;
    tracker_instance->_xnorm = xnorm;
    tracker_instance->_gnorm = gnorm;
    tracker_instance->_iter = k;

    if (tracker_instance->_progress_func)
      return (*tracker_instance->_progress_func)(tracker_instance->_instance, x, g, fx, xnorm, gnorm, step, n, k, ls);

    return 0;
  }

  /// Static function to get pointer to the original LBFGS instance from a \p ycutils::lbfgs_progress_tracker instance.
  static void *get_instance(void *tracker_instance) { return static_cast<lbfgs_progress_tracker *>(tracker_instance)->_instance; }

  /// Returns a \p void \p * pointer to this object.
  void *instance_ptr() { return static_cast<void *>(this); }

  /// Reset the tracking variables.
  void reset()
  {
    _fx = 0;
    _xnorm = 0;
    _gnorm = 0;
    _iter = 0;
  }

  /// Get the last function value.
  lbfgsfloatval_t last_fx() const { return _fx; }

  /// Get the last norm of \p x.
  lbfgsfloatval_t last_xnorm() const { return _xnorm; }

  /// Get the last gradient norm.
  lbfgsfloatval_t last_gnorm() const { return _gnorm; }

  /// Get the last iteration number.
  lbfgsfloatval_t last_iter() const { return _iter; }

  /// Set the function pointer to the progress function.
  void progress_func(lbfgs_progress_t progress_func) { _progress_func = progress_func; }

  /// Get the function pointer to the current progress function.
  const lbfgs_progress_t progress_func() const { return _progress_func; }

private:
  /// function value
  lbfgsfloatval_t _fx;

  /// norm of the x vector
  lbfgsfloatval_t _xnorm;

  /// norm of the gradient
  lbfgsfloatval_t _gnorm;

  /// iteration
  int _iter;

  /// original instance for use with lbfgs
  void *_instance;

  /// progress function to call after recording progress details in \p lbfgs_progress_tracker::progress.
  lbfgs_progress_t _progress_func;
};

/**
 * @brief Perform dot product between \p lbfgsfloatval_t arrays and any \p operator[] indexable containers.
 * @param x \p lbfgsfloatval_t array
 * @param y \p operator[] indexable type
 * @param n vector dimensions
 * @tparam Real return type (defaults to float)
 * @return dot product \f$x^\top y\f$
 */
template <typename Real = float, typename RealVector>
Real lbfgs_dot_product(const lbfgsfloatval_t *x, RealVector y, int n)
{
  Real d = 0.0;
  for (const auto i : irange(n))
    d += x[i] * y[i];

  return d;
}

#if defined(USE_EIGEN) || defined(DOXYGEN)
/**
 * @brief Perform dot product between \p lbfgsfloatval_t arrays and \p Eigen::SparseVector.
 * @param x \p lbfgsfloatval_t array
 * @param y \p Eigen::SparseVector
 * @param n vector dimensions
 * @tparam Real return type (defaults to float)
 * @return dot product \f$x^\top y\f$
 * @see USE_EIGEN
 */
template <typename Real = float>
Real lbfgs_dot_product(const lbfgsfloatval_t *x, const Eigen::SparseVector<Real> &y, int n)
{
  Real d = 0.0;
  for (typename Eigen::SparseVector<Real>::InnerIterator it(y); it; ++it)
  {
    d += x[it.index()] * it.value();
    assert(it.index() < n);
  }

  return d;
}

#endif

/**
 * @brief Perform dot product between \p lbfgsfloatval_t arrays and \p ycutils::sparse_vector.
 * @param x \p lbfgsfloatval_t array
 * @param y \p ycutils::sparse_vector
 * @param n vector dimensions
 * @tparam Real return type (defaults to float)
 * @return dot product \f$x^\top y\f$
 */
template <typename Real>
Real lbfgs_dot_product(const lbfgsfloatval_t *x, const sparse_vector<Real> &y, int n)
{
  Real d = 0.0;
  for (auto e : y)
  {
    d += x[e.first] * e.second;
    assert(e.first < n);
  }

  return d;
}

/**
 * @brief Converts an \p lbfgsfloatval_t array to vector.
 * @param x \p lbfgsfloatval_t array
 * @param n vector dimensions
 * @tparam Vector return type
 * @return the \p Vector representation of \p x
 */
template <typename Vector>
Vector lbfgs_array_to_vector(const lbfgsfloatval_t *x, int n)
{
  Vector e(n);
  for (auto i : irange(n))
    e[i] = x[i];

  return e;
}

/**
 * @brief Converts a vector to \p lbfgsfloatval_t array.
 * @param x \p lbfgsfloatval_t array to store results
 * @param e \p Vector to convert
 */
template <typename Vector>
void vector_to_lbfgs_array(lbfgsfloatval_t *x, const Vector& e)
{
  for (auto i : irange(e.size()))
    x[i] = e[i];
}

/**
 * @brief Converts a vector to \p lbfgsfloatval_t array.
 * @param e \p Vector to convert
 * @return the converted \p lbfgsfloatval_t array
 */
template <typename Vector>
lbfgsfloatval_t *vector_to_lbfgs_array(const Vector& e)
{
  lbfgsfloatval_t *x = lbfgs_malloc(e.size());
  for (auto i : irange(e.size()))
    x[i] = e[i];
  return x;
}

/**
 * @brief Converts a sparse vector to \p lbfgsfloatval_t array.
 * @param e \p Vector to convert
 * @return the converted \p lbfgsfloatval_t array
 */
template <typename Real>
lbfgsfloatval_t *vector_to_lbfgs_array(const ycutils::sparse_vector<Real>& e)
{
  lbfgsfloatval_t *x = lbfgs_malloc(e.size());
  for (auto i : irange(e.size()))
    x[i] = 0;

  for (const auto& p : e)
    x[p.first] = p.second;

  return x;
}

/**
 * @brief Returns the descriptive form of \p lbfgs() return values.
 * @see http://www.chokkan.org/software/liblbfgs/group__liblbfgs__api.html#gga06fc87d81c62e9abb8790b6e5713c55ba681422b3ac6769f6684af7aa216aaa02
 * @param ret \p lbfgs() return value
 * @param long_description if true, return the long description, otherwise, return only the enumeration constant name
 * @return description string
 */
inline const string lbfgs_return_value_description(int ret, bool long_description = true)
{
  if (long_description)
  {
    if (ret == LBFGS_SUCCESS || ret == LBFGS_CONVERGENCE) return "LBFGS_CONVERGENCE: L-BFGS reaches convergence.";
    else if (ret == LBFGS_STOP) return "LBFGS_STOP: The minimization process has been stopped.";
    else if (ret == LBFGS_ALREADY_MINIMIZED) return "LBFGS_ALREADY_MINIMIZED: The initial variables already minimize the objective function.";
    else if (ret == LBFGSERR_UNKNOWNERROR) return "LBFGSERR_UNKNOWNERROR: Unknown error.";
    else if (ret == LBFGSERR_LOGICERROR) return "LBFGSERR_LOGICERROR: Logic error.";
    else if (ret == LBFGSERR_OUTOFMEMORY) return "LBFGSERR_OUTOFMEMORY: Insufficient memory.";
    else if (ret == LBFGSERR_CANCELED) return "LBFGSERR_CANCELED: The minimization process has been canceled.";
    else if (ret == LBFGSERR_INVALID_N) return "LBFGSERR_INVALID_N: Invalid number of variables specified.";
    else if (ret == LBFGSERR_INVALID_N_SSE) return "LBFGSERR_INVALID_N_SSE: Invalid number of variables (for SSE) specified.";
    else if (ret == LBFGSERR_INVALID_X_SSE) return "LBFGSERR_INVALID_X_SSE: The array x must be aligned to 16 (for SSE).";
    else if (ret == LBFGSERR_INVALID_EPSILON) return "LBFGSERR_INVALID_EPSILON: Invalid parameter lbfgs_parameter_t::epsilon specified.";
    else if (ret == LBFGSERR_INVALID_TESTPERIOD) return "LBFGSERR_INVALID_TESTPERIOD: Invalid parameter lbfgs_parameter_t::past specified.";
    else if (ret == LBFGSERR_INVALID_DELTA) return "LBFGSERR_INVALID_DELTA: Invalid parameter lbfgs_parameter_t::delta specified.";
    else if (ret == LBFGSERR_INVALID_LINESEARCH) return "LBFGSERR_INVALID_LINESEARCH: Invalid parameter lbfgs_parameter_t::linesearch specified.";
    else if (ret == LBFGSERR_INVALID_MINSTEP) return "LBFGSERR_INVALID_MINSTEP: Invalid parameter lbfgs_parameter_t::max_step specified.";
    else if (ret == LBFGSERR_INVALID_MAXSTEP) return "LBFGSERR_INVALID_MAXSTEP: Invalid parameter lbfgs_parameter_t::max_step specified.";
    else if (ret == LBFGSERR_INVALID_FTOL) return "LBFGSERR_INVALID_FTOL: Invalid parameter lbfgs_parameter_t::ftol specified.";
    else if (ret == LBFGSERR_INVALID_WOLFE) return "LBFGSERR_INVALID_WOLFE: Invalid parameter lbfgs_parameter_t::wolfe specified.";
    else if (ret == LBFGSERR_INVALID_GTOL) return "LBFGSERR_INVALID_GTOL: Invalid parameter lbfgs_parameter_t::gtol specified.";
    else if (ret == LBFGSERR_INVALID_XTOL) return "LBFGSERR_INVALID_XTOL: Invalid parameter lbfgs_parameter_t::xtol specified.";
    else if (ret == LBFGSERR_INVALID_MAXLINESEARCH) return "LBFGSERR_INVALID_MAXLINESEARCH: Invalid parameter lbfgs_parameter_t::max_linesearch specified.";
    else if (ret == LBFGSERR_INVALID_ORTHANTWISE) return "LBFGSERR_INVALID_ORTHANTWISE: Invalid parameter lbfgs_parameter_t::orthantwise_c specified.";
    else if (ret == LBFGSERR_INVALID_ORTHANTWISE_START) return "LBFGSERR_INVALID_ORTHANTWISE_START: Invalid parameter lbfgs_parameter_t::orthantwise_start specified.";
    else if (ret == LBFGSERR_INVALID_ORTHANTWISE_END) return "LBFGSERR_INVALID_ORTHANTWISE_END: Invalid parameter lbfgs_parameter_t::orthantwise_end specified.";
    else if (ret == LBFGSERR_OUTOFINTERVAL) return "LBFGSERR_OUTOFINTERVAL: The line-search step went out of the interval of uncertainty.";
    else if (ret == LBFGSERR_INCORRECT_TMINMAX) return "LBFGSERR_INCORRECT_TMINMAX: A logic error occurred; alternatively, the interval of uncertainty became too small.";
    else if (ret == LBFGSERR_ROUNDING_ERROR) return "LBFGSERR_ROUNDING_ERROR: A rounding error occurred; alternatively, no line-search step satisfies the sufficient decrease and curvature conditions.";
    else if (ret == LBFGSERR_MINIMUMSTEP) return "LBFGSERR_MINIMUMSTEP: The line-search step became smaller than lbfgs_parameter_t::min_step.";
    else if (ret == LBFGSERR_MAXIMUMSTEP) return "LBFGSERR_MAXIMUMSTEP: The line-search step became larger than lbfgs_parameter_t::max_step.";
    else if (ret == LBFGSERR_MAXIMUMLINESEARCH) return "LBFGSERR_MAXIMUMLINESEARCH: The line-search routine reaches the maximum number of evaluations.";
    else if (ret == LBFGSERR_MAXIMUMITERATION) return "LBFGSERR_MAXIMUMITERATION: The algorithm routine reaches the maximum number of iterations.";
    else if (ret == LBFGSERR_WIDTHTOOSMALL) return "LBFGSERR_WIDTHTOOSMALL: Relative width of the interval of uncertainty is at most lbfgs_parameter_t::xtol.";
    else if (ret == LBFGSERR_INVALIDPARAMETERS) return "LBFGSERR_INVALIDPARAMETERS: A logic error (negative line-search step) occurred.";
    else if (ret == LBFGSERR_INCREASEGRADIENT) return "LBFGSERR_INCREASEGRADIENT: The current search direction increases the objective function value.";
  }
  else
  {
    if (ret == LBFGS_SUCCESS || ret == LBFGS_CONVERGENCE) return "LBFGS_CONVERGENCE";
    else if (ret == LBFGS_STOP) return "LBFGS_STOP";
    else if (ret == LBFGS_ALREADY_MINIMIZED) return "LBFGS_ALREADY_MINIMIZED";
    else if (ret == LBFGSERR_UNKNOWNERROR) return "LBFGSERR_UNKNOWNERROR";
    else if (ret == LBFGSERR_LOGICERROR) return "LBFGSERR_LOGICERROR";
    else if (ret == LBFGSERR_OUTOFMEMORY) return "LBFGSERR_OUTOFMEMORY";
    else if (ret == LBFGSERR_CANCELED) return "LBFGSERR_CANCELED";
    else if (ret == LBFGSERR_INVALID_N) return "LBFGSERR_INVALID_N";
    else if (ret == LBFGSERR_INVALID_N_SSE) return "LBFGSERR_INVALID_N_SSE";
    else if (ret == LBFGSERR_INVALID_X_SSE) return "LBFGSERR_INVALID_X_SSE";
    else if (ret == LBFGSERR_INVALID_EPSILON) return "LBFGSERR_INVALID_EPSILON";
    else if (ret == LBFGSERR_INVALID_TESTPERIOD) return "LBFGSERR_INVALID_TESTPERIOD";
    else if (ret == LBFGSERR_INVALID_DELTA) return "LBFGSERR_INVALID_DELTA";
    else if (ret == LBFGSERR_INVALID_LINESEARCH) return "LBFGSERR_INVALID_LINESEARCH";
    else if (ret == LBFGSERR_INVALID_MINSTEP) return "LBFGSERR_INVALID_MINSTEP";
    else if (ret == LBFGSERR_INVALID_MAXSTEP) return "LBFGSERR_INVALID_MAXSTEP";
    else if (ret == LBFGSERR_INVALID_FTOL) return "LBFGSERR_INVALID_FTOL";
    else if (ret == LBFGSERR_INVALID_WOLFE) return "LBFGSERR_INVALID_WOLFE";
    else if (ret == LBFGSERR_INVALID_GTOL) return "LBFGSERR_INVALID_GTOL";
    else if (ret == LBFGSERR_INVALID_XTOL) return "LBFGSERR_INVALID_XTOL";
    else if (ret == LBFGSERR_INVALID_MAXLINESEARCH) return "LBFGSERR_INVALID_MAXLINESEARCH";
    else if (ret == LBFGSERR_INVALID_ORTHANTWISE) return "LBFGSERR_INVALID_ORTHANTWISE";
    else if (ret == LBFGSERR_INVALID_ORTHANTWISE_START) return "LBFGSERR_INVALID_ORTHANTWISE_START";
    else if (ret == LBFGSERR_INVALID_ORTHANTWISE_END) return "LBFGSERR_INVALID_ORTHANTWISE_END";
    else if (ret == LBFGSERR_OUTOFINTERVAL) return "LBFGSERR_OUTOFINTERVAL";
    else if (ret == LBFGSERR_INCORRECT_TMINMAX) return "LBFGSERR_INCORRECT_TMINMAX";
    else if (ret == LBFGSERR_ROUNDING_ERROR) return "LBFGSERR_ROUNDING_ERROR";
    else if (ret == LBFGSERR_MINIMUMSTEP) return "LBFGSERR_MINIMUMSTEP";
    else if (ret == LBFGSERR_MAXIMUMSTEP) return "LBFGSERR_MAXIMUMSTEP";
    else if (ret == LBFGSERR_MAXIMUMLINESEARCH) return "LBFGSERR_MAXIMUMLINESEARCH";
    else if (ret == LBFGSERR_MAXIMUMITERATION) return "LBFGSERR_MAXIMUMITERATION";
    else if (ret == LBFGSERR_WIDTHTOOSMALL) return "LBFGSERR_WIDTHTOOSMALL";
    else if (ret == LBFGSERR_INVALIDPARAMETERS) return "LBFGSERR_INVALIDPARAMETERS";
    else if (ret == LBFGSERR_INCREASEGRADIENT) return "LBFGSERR_INCREASEGRADIENT";
  }
  return "Unknown return value.";
}

}  // namespace ycutils

#endif  // YCUTILS_LBFGS_H_INCLUDED
