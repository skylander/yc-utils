
/**
 * @file
 * @brief A collection of utility functions for handling I/O reading and writing.
 */
#ifndef YCUTILS_IOUTILS_H_INCLUDED
#define YCUTILS_IOUTILS_H_INCLUDED

#include <cmath>
#include <fstream>
#include <string>
#include <vector>

#include <boost/filesystem/fstream.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/iostreams/device/file_descriptor.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <boost/iostreams/filter/gzip.hpp>

#include "ycutils.h"

namespace ycutils {
  namespace ioutils {
    /// Reporting symbol for minor reporting intervals.
    static const char* progress_report_symbol = ".";

    /// Reporting format string for major reporting intervals.
    /// Use %d for actual line counts, and %k for reporting in terms of thousands.
    static const char* progress_report_format = "%k";

    /// Default reporting interval -- \p progress_report_symbol will be printed after every \p progress_interval_small lines.
    static constexpr size_type progress_interval_small = 10000;

    /// Default reporting interval -- No. of lines read so far will be reported after every \p progress_interval_big lines.
    static constexpr size_type progress_interval_big = 100000;

    /// Expected file sizes for reserving vectors of strings (~1M lines)
    static constexpr size_type expected_num_lines = 1 << 20;
  }  // namespace ioutils

  void report_progress(const size_type, std::ostream&, const size_type, const size_type, const std::string&, const std::string&);

  /**
   * @brief Read lines from file into a vector of strings.
   * @param in \p std::istream to read from.
   * @param it output iterator to append to.
   */
  template <typename OutputIterator>
  inline void read_lines_into_vector(std::istream& in, OutputIterator it)
  {
    for (std::string line; std::getline(in, line);)
      *it++ = line;
  }

  /**
   * @brief Read lines from file into a vector of strings.
   * @param filename name of file to read from
   * @param it output iterator to append to.
   */
  template <typename OutputIterator>
  inline void read_lines_into_vector(const std::string& filename, OutputIterator it)
  {
    std::ifstream ifs(filename);
    read_lines_into_vector(ifs, it);
    ifs.close();
  }

  /**
   * @brief Read lines from file into a vector of strings.
   * @param filename \p boost::filesystem::path of filename to read from
   * @param it output iterator to append to.
   */
  template <typename OutputIterator>
  inline void read_lines_into_vector(const boost::filesystem::path& filename, OutputIterator it)
  {
    boost::filesystem::ifstream ifs(filename);
    read_lines_into_vector(ifs, it);
    ifs.close();
  }

  /**
   * @brief Read lines from file into a vector of strings, while displaying progress information.
   * @param filename name of file to read from
   * @param it output iterator to append to.
   * @param os output stream to write progress updates to
   * @param progress_interval_small reporting interval for minor progress
   * @param progress_interval_big reporting interval for major progress
   * @param progress_report_symbol symbol to use for reporting minor progress
   * @param progress_report_format progress reporting format (%d will be automatically substituted with the count)
   */
  template <typename OutputIterator>
  inline void read_lines_into_vector_with_progress(const std::string& filename, OutputIterator it, std::ostream& os, const size_type progress_interval_small = ioutils::progress_interval_small, const size_type progress_interval_big = ioutils::progress_interval_big, const std::string& progress_report_symbol = ioutils::progress_report_symbol, const std::string& progress_report_format = ioutils::progress_report_format)
  {
    std::ifstream ifs(filename);
    read_lines_into_vector_with_progress(ifs, it, os, progress_interval_small, progress_interval_big, progress_report_symbol, progress_report_format);
    ifs.close();
  }

  /**
   * @brief Read lines from \p std::istream into a vector of strings, while displaying progress information.
   * @param in input stream to read lines from
   * @param it output iterator to append to
   * @param os output stream to write progress updates to
   * @param progress_interval_small reporting interval for minor progress
   * @param progress_interval_big reporting interval for major progress
   * @param progress_report_symbol symbol to use for reporting minor progress
   * @param progress_report_format progress reporting format (%d will be automatically substituted with the count)
   */
  template <typename OutputIterator>
  inline void read_lines_into_vector_with_progress(std::istream& in, OutputIterator it, std::ostream& os, const size_type progress_interval_small = ioutils::progress_interval_small, const size_type progress_interval_big = ioutils::progress_interval_big, const std::string& progress_report_symbol = ioutils::progress_report_symbol, const std::string& progress_report_format = ioutils::progress_report_format)
  {
    size_type lines_count = 1;

    for (std::string line; std::getline(in, line); ++it, ++lines_count)
    {
      *it = line;
      report_progress(lines_count, os, progress_interval_small, progress_interval_big, progress_report_symbol, progress_report_format);
    }
  }

  /**
   * @brief Progress reporting helper function.
   * @param cur current progress counter
   * @param os output stream to write progress updates to
   * @param progress_interval_small reporting interval for minor progress
   * @param progress_interval_big reporting interval for major progress
   * @param progress_report_symbol symbol to use for reporting minor progress
   * @param progress_report_format progress reporting format (%d will be automatically substituted with the count)
   */
  inline void report_progress(const size_type cur, std::ostream& os, const size_type progress_interval_small = ioutils::progress_interval_small, const size_type progress_interval_big = ioutils::progress_interval_big, const std::string& progress_report_symbol = ioutils::progress_report_symbol, const std::string& progress_report_format = ioutils::progress_report_format)
  {
    if (cur % progress_interval_big == 0)
    {
      std::string update_line = progress_report_format;

      auto f = update_line.find("%k");
      if (f != std::string::npos) update_line.replace(f, 2, std::to_string((size_type)std::floor(cur / 1000)) + "k");

      f = update_line.find("%d");
      if (f != std::string::npos) update_line.replace(f, 2, std::to_string(cur));

      os << update_line;
    }
    else if (cur % progress_interval_small == 0)
      os << progress_report_symbol;
  }

  /**
   * @brief Creates an \p boost::iostreams::filtering_ostream object that writes out to specified \p path in GZip format.
   * @details It uses \p boost::iostreams.
   * @param path filename to write to.
   * @return \p boost::iostreams::filtering_ostream object
   */
  inline std::unique_ptr<boost::iostreams::filtering_ostream> gzip_ostream(const boost::filesystem::path& path) {
    std::unique_ptr<boost::iostreams::filtering_ostream> out(new boost::iostreams::filtering_ostream());
    out->push(boost::iostreams::gzip_compressor(boost::iostreams::gzip_params(boost::iostreams::zlib::best_compression)));
    out->push(boost::iostreams::file_descriptor_sink(path));

    return out;
  }
}  // namespace ycutils

#endif  // YCUTILS_IOUTILS_H_INCLUDED
