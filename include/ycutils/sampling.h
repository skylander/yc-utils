/**
 * @file
 * @brief A collections of functions for handling sampling.
 */
#ifndef YCUTILS_SAMPLING_H_INCLUDED
#define YCUTILS_SAMPLING_H_INCLUDED

#include <cmath>
#include <iterator>
#include <limits>
#include <random>
#include <vector>

#include <boost/range/algorithm/lower_bound.hpp>

#include "ycutils.h"

#ifdef USE_EIGEN
#include <Eigen/Dense>
#endif

#include "ycutils/collections.h"
#include "ycutils/fastmath.h"
#include "ycutils/math.h"
#include "ycutils/random.h"

namespace ycutils {
using ycutils::index_type;

#if defined(USE_EIGEN) || defined(DOXYGEN)

/**
 * @brief Generates multivariate random numbers according to the Normal (or Gaussian) random number distribution.
 * @details See definition <a href="http://en.wikipedia.org/wiki/Multivariate_normal_distribution">here</a>.
 * @tparam _Scalar Numerical type
 */
template <typename _Scalar = double>
class multivariate_normal_distribution
{
public:
  /// Name alias for Eigen vector type.
  using Vector = Eigen::Matrix<_Scalar, Eigen::Dynamic, 1>;

  /// Name alias for Eigen matrix type.
  using Matrix = Eigen::Matrix<_Scalar, Eigen::Dynamic, Eigen::Dynamic>;

  /// Construct \p size dimensional MVN with \p 0 mean and identity covariance.
  multivariate_normal_distribution(const size_type size) : _size(size), _normal(0, 1.0)
  {
    multivariate_normal_distribution(size, 0.0, 1.0);
  }

  /// Construct \p size dimensional MVN with scalar \p mean and identity covariance.
  multivariate_normal_distribution(const size_type size, const double mean) : _size(size), _normal(0, 1.0)
  {
    multivariate_normal_distribution(size, mean, 1.0);
  }

  /// Construct \p size dimensional MVN with scalar \p mean and diagonal (\f$ \sigma^2 I \f$) covariance.
  multivariate_normal_distribution(const size_type size, const double mean, const double sigma2) : _size(size), _normal(0, 1.0)
  {
    _mean.setConstant(size, mean);
    _scale.setConstant(size, std::sqrt(sigma2));
    _rotation = Matrix::Identity(size, size);
  }

  /// Construct \p size dimensional MVN with scalar \p mean and \p diagonal covariance.
  multivariate_normal_distribution(const size_type size, const double mean, const Vector& diagonal) : _size(size), _normal(0, 1.0)
  {
    _mean.setConstant(size, mean);
    _scale = diagonal.unaryExpr([](const double v) { return std::sqrt(v); });
    _rotation = Matrix::Identity(size, size);
  }

  /// Construct \p size dimensional MVN with scalar \p mean and full covariance.
  multivariate_normal_distribution(const size_type size, const double mean, const Matrix& covar) : _size(size), _normal(0, 1.0)
  {
    _mean.setConstant(size, mean);
    covariance(covar);
  }

  /// Construct \p size dimensional MVN with vector \p mean and diagonal \f$ \sigma^2 I \f$ covariance.
  multivariate_normal_distribution(const size_type size, const Vector& mean, const double sigma2) : _mean(mean), _size(size), _normal(0, 1.0)
  {
    _scale.setConstant(size, std::sqrt(sigma2));
    _rotation = Matrix::Identity(size, size);
  }

  /// Construct \p size dimensional MVN with vector \p mean and diagonal \p diagonal covariance.
  multivariate_normal_distribution(const size_type size, const Vector& mean, const Vector& diagonal) : _mean(mean), _size(size), _normal(0, 1.0)
  {
    _scale = diagonal.unaryExpr([](const double v) { return std::sqrt(v); });
    _rotation = Matrix::Identity(size, size);
  }

  /// Construct \p size dimensional MVN with vector \p mean and full covariance.
  multivariate_normal_distribution(const size_type size, const Vector& mean, const Matrix& covar) : _mean(mean), _size(size), _normal(0, 1.0)
  {
    covariance(covar);
  }

  /// Get the covariance matrix for the distribution.
  /// @note This rebuilds the covariance matrix from the eigen decomposed form. Numerically, we may not get back the same answer as before.
  const Matrix& covariance() const { return _rotation * _scale.cwiseProduct(_scale).asDiagonal() * _rotation.inverse(); }

  /// Get the mean vector of the distribution.
  const Vector& mean() const { return _mean; }

  /// Decompose full covariance matrix into \f$ \Sigma = AA^\top \f$.
  void covariance(const Matrix& covar)
  {
    Eigen::SelfAdjointEigenSolver<Eigen::Matrix<_Scalar, Eigen::Dynamic, Eigen::Dynamic> > eigen_solver(covar);
    _rotation = eigen_solver.eigenvectors();
    _scale = eigen_solver.eigenvalues();

    for (const auto i : ycutils::irange(_size))
      _scale[i] = std::sqrt(_scale[i]);

    // std::cout << _rotation << std::endl << std::endl;
    // std::cout << _scale.transpose() << std::endl;
  }

  /// Set the mean vector.
  void mean(const Vector& vec) { _mean = vec; }

  /// Returns a vector distributed according to \f$ \text{MVN}(\mu, \Sigma) \f$.
  template <typename Generator>
  Vector operator()(Generator &g)
  {
    Vector sample(_scale);
    for (const auto i : ycutils::irange(_size))
      sample[i] *= _normal(g);

    return _mean + (_rotation * sample);
  }

private:
  /// No. of dimensions
  size_type _size;

  /// Mean vector.
  Vector _mean;

  /// Eigenvector of covariance matrix.
  Matrix _rotation;

  /// Eigenvalues of covariance matrix.
  Vector _scale;

  /// Univariate normal number generator.
  std::normal_distribution<> _normal;
};

#endif  // USE_EIGEN || DOXYGEN

/**
 * @brief Samples from a cumulative sum array using binary search.
 *
 * @param cum_rng Cumulative sum range.
 * @param u Random number in <tt>[0, 1)</tt>.
 * @return the index of the sampled item
 */
template <typename RandomAccessRange>
inline index_type sample_from_discrete_cumsum(const RandomAccessRange& cum_rng, const double u)
{
  auto it = boost::lower_bound(cum_rng, u * cum_rng.back());
  return std::distance(std::begin(cum_rng)  , it);
}

/**
 * @brief Sample from a categorical distribution with weights defined in a range \p rng. The weights need not be normalized.
 * @details This function generates a uniform random number \p r from \p gen_01 and iterates the list to sample the item. Time complexity: \p O(N).
 *
 * @param rng \p Range containing weights for each category.
 * @param sum Sum of the sequence of weights.
 * @param u A random number in <tt>[0, 1)</tt>.
 * @return the index of the sampled item
 */
template <typename Range>
inline index_type sample_from_discrete_range(const Range& rng, const double sum, const double u)
{
  double r = u * sum;
  index_type i = 0;
  for (const auto x : rng)
  {
    r -= x;
    if (r < 0.0)
      return i;
    ++i;
  }

  return i - 1;
}

/**
 * @brief Sample from a categorical distribution with weights defined in a range \p rng. The weights need not be normalized.
 * @see ycutils::sample_from_discrete_range
 */
template <typename Range>
inline index_type sample_from_discrete_range(const Range& rng, const double u)
{
  std::vector<double> cum_sum(rng.size());
  index_type i = 0;
  for (const auto x : rng)
    cum_sum[i] = (i > 0 ? cum_sum[i - 1] : 0.0) + x;

  return sample_from_discrete_cumsum(cum_sum, u);
}

/**
 * @brief Sample from a categorical distribution with weights defined in a range \p rng. The weights need not be normalized.
 * @see ycutils::sample_from_discrete_range
 */
template <typename Range, typename Generator>
inline index_type sample_from_discrete_range(const Range& rng)
{
  mersenne_generator<std::uniform_real_distribution<> > gen_01(default_random_engine(ycutils::time_seed()), std::uniform_real_distribution<>(0, 1));

  return sample_from_discrete_range(rng, gen_01());
}

/**
 * @brief Sample from a categorical distribution with weights defined in a range \p arr. The weights need not be normalized.
 * @details This function generates a uniform random number \p r from \p gen_01 and iterates the list to sample the item. Time complexity: \p O(N).
 *
 * @param arr \p Array containing weights for each category.
 * @param N size of array \p arr.
 * @param sum Sum of the sequence of weights.
 * @param u A random number in <tt>[0, 1)</tt>.
 * @return the index of the sampled item
 */
template <typename Array>
inline index_type sample_from_discrete_array(const Array& arr, const size_type N, const double sum, const double u)
{
  double r = u * sum;
  for (index_type i = 0; i < N; ++i)
  {
    r -= arr[i];
    if (r < 0.0)
      return i;
  }

  return N - 1;
}

/**
 * @brief Sample from a categorical distribution with weights defined in a range \p arr. The weights need not be normalized.
 * @see ycutils::sample_from_discrete_array
 */
template <typename Array>
inline index_type sample_from_discrete_array(const Array& arr, const size_type N, const double u)
{
  auto sum = arr[0];
  for (index_type i = 1; i < N; ++i)
    sum += arr[i];

  return sample_from_discrete_array(arr, N, sum, u);
}

/**
 * @brief Sample from a categorical distribution with weights defined in a range \p arr. The weights need not be normalized.
 * @see ycutils::sample_from_discrete_array
 */
template <typename Array>
inline index_type sample_from_discrete_array(const Array& arr, const size_type N)
{
  mersenne_generator<std::uniform_real_distribution<> > gen_01(default_random_engine(ycutils::time_seed()), std::uniform_real_distribution<>(0, 1));
  return sample_from_discrete_array(arr, N, gen_01());
}

/**
 * @brief Sample from a categorical distributions whose weights are in log space and defined by \p rng. The weights need not be normalized.
 * @details This function does all operations in log space to be as robust to numerical underflow/overflow as possible.
 * It computes the cumulative sum using \p ycutils::log_add and log transforms \p gen_01() to find the correct sample in the list by comparisons.
 *
 * @param rng \p Range containing weights for each category.
 * @param log_sum Logarithm of sum of the exponentiated weights.
 * @param u A random number in <tt>[0, 1)</tt>.
 * @return the index of the sampled item
 */
template <typename Range>
inline index_type sample_from_log_discrete_range(const Range& rng, const double log_sum, const double u)
{
  double r = u == 0 ? log_sum : (std::log(u) + log_sum);
  index_type i = 0;
  for (const auto x : rng)
  {
    r -= x;
    if (r < 0.0)
      return i;
    ++i;
  }

  return i - 1;
}

/**
 * @brief Sample from a categorical distributions whose weights are in log space and defined by \p rng. The weights need not be normalized.
 * @see ycutils::sample_from_log_discrete_range
 */
template <typename Range>
inline index_type sample_from_log_discrete_range(const Range& rng, const double u)
{
  double log_sum = ycutils::math::log_zero;
  for (auto log_p : rng)  // find sum
    log_sum = ycutils::log_add(log_sum, log_p);

  return sample_from_log_discrete_range(rng, log_sum, u);
}

/**
 * @brief Sample from a categorical distributions whose weights are in log space and defined by \p rng. The weights need not be normalized.
 * @see ycutils::sample_from_log_discrete_range
 */
template <typename Range>
inline index_type sample_from_log_discrete_range(const Range& rng)
{
  mersenne_generator<std::uniform_real_distribution<> > gen_01(default_random_engine(ycutils::time_seed()), std::uniform_real_distribution<>(0, 1));

  return sample_from_log_discrete_range(rng, gen_01());
}

}  // namespace ycutils

#endif  // YCUTILS_SAMPLING_H_INCLUDED
