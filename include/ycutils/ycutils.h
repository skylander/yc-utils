/// @file
/// @brief Dummy file containing documentation for \p ycutils.
#ifndef YCUTILS_H_INCLUDED
#define YCUTILS_H_INCLUDED

#include <cstddef>

#ifdef DOXYGEN
  /// Use <a href="http://eigen.tuxfamily.org/index.php?title=Main_Page">Eigen</a> linear algebra library.
  /// @note This macro is automatically defined when we detect that the Eigen header files have been included; specifically, it looks for the presence of the \p EIGEN_WORLD_VERSION directive.
  #define USE_EIGEN
#endif

// auto define USE_EIGEN if we know Eigen is in use
#if !defined(USE_EIGEN) && defined(EIGEN_WORLD_VERSION)
  #define USE_EIGEN
#endif

/// @brief Namespace encsapsulating all functions related to the \p ycutils package.
namespace ycutils
{
  /// A convenient global typedef for anything referring to sizes of things.
  using size_type = std::size_t;

  /// Used to represent vocabulary indexes.
  using index_type = std::size_t;
}
#endif  // YCUTILS_H_INCLUDED
