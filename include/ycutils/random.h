/**
 * @file
 * @brief Helper functions for dealing with random number generation.
 */
#ifndef YCUTILS_RANDOM_H_INCLUDED
#define YCUTILS_RANDOM_H_INCLUDED

#include <algorithm>
#include <chrono>
#include <random>

#include <boost/random/variate_generator.hpp>

#include "ycutils.h"
#include "fastmath.h"

namespace ycutils {

/// Handy typedef for defining a \p boost::variate_generator using Mersenne Twister generator.
template <typename Distribution>
using mersenne_generator = boost::random::variate_generator<std::mt19937, Distribution>;

/// Library default random engine.
using default_random_engine = std::mt19937;

/// Alias function for returning the current time for use as random seeds.
inline long int time_seed() { return std::chrono::system_clock::now().time_since_epoch().count(); }

/**
 * @brief Randomly shuffles a sequence.
 * @param seq sequence to shuffle
 * @param rng random number generator; if not specified, we use \p ycutils::default_random_engine.
 */
template <typename Sequence, typename RandomNumberGenerator>
inline void shuffle(Sequence& seq, RandomNumberGenerator& rng)
{
  std::shuffle(std::begin(seq), std::end(seq), rng);
}

/**
 * @brief Randomly shuffles a sequence using \p ycutils::default_random_engine with \p ycutils::time_seed().
 * @param seq sequence to shuffle
 */
template <typename Sequence>
inline void shuffle(Sequence& seq)
{
  std::shuffle(std::begin(seq), std::end(seq), default_random_engine(time_seed()));
}

/**
 * @brief Measures the Kullback-Leibler divergence between two categorical distributions.
 * @details This method uses \p ycutils::fastlog methods.
 * For more details about KL divergence, check out <a href="http://en.wikipedia.org/wiki/Kullback%E2%80%93Leibler_divergence">http://en.wikipedia.org/wiki/Kullback%E2%80%93Leibler_divergence</a>.
 *
 * @param dist1 a categorical distribution
 * @param dist2 a categorical distribution
 * @return KL divergence
 */
template <typename RealVector>
inline double kl_divergence(const RealVector& dist1, const RealVector& dist2)
{
  double kl = 0.0;
  index_type i = 0;
  for (const auto p : dist1)
  {
    kl += p * (fastlog(p) - fastlog(dist2[i]));
    ++i;
  }

  return kl;
}

/**
 * @brief Measures the Kullback-Leibler divergence between two categorical distributions.
 * @details This method uses \p ycutils::fastlog methods.
 * For more details about KL divergence, check out <a href="http://en.wikipedia.org/wiki/Kullback%E2%80%93Leibler_divergence">http://en.wikipedia.org/wiki/Kullback%E2%80%93Leibler_divergence</a>.
 *
 * @param dist1 a categorical distribution
 * @param dist2 a categorical distribution
 * @param n size of support of \p dist1 and \p dist2
 * @return KL divergence
 */
template <typename RealArray>
inline double kl_divergence(const RealArray& dist1, const RealArray& dist2, const size_type n)
{
  double kl = 0.0;
  for (index_type i = 0; i < n; ++i)
  {
    const auto p = dist1[i];
    const auto q = dist2[i];
    kl += p * (fastlog(p) - fastlog(q));
  }

  return kl;
}

/**
 * @brief Measures the Jensen-Shannon divergence between two categorical distributions.
 * @details This method uses \p ycutils::fastlog methods.
 * For more details about JS divergence, check out <a href="http://en.wikipedia.org/wiki/Jensen%E2%80%93Shannon_divergence">http://en.wikipedia.org/wiki/Jensen%E2%80%93Shannon_divergence</a>.
 *
 * @param dist1 a categorical distribution
 * @param dist2 a categorical distribution
 * @return JS divergence
 */
template <typename RealVector>
inline double js_divergence(const RealVector& dist1, const RealVector& dist2)
{
  double js = 0.0;
  index_type i = 0;
  for (const auto p : dist1)
  {
    const auto q = dist2[i];
    const auto m = 0.5 * (p + q);
    js += p * (fastlog(p) - fastlog(m));
    js += q * (fastlog(q) - fastlog(m));
    ++i;
  }

  return 0.5 * js;
}

/**
 * @brief Measures the Jensen-Shannon divergence between two categorical distributions.
 * @details This method uses \p ycutils::fastlog methods.
 * For more details about JS divergence, check out <a href="http://en.wikipedia.org/wiki/Jensen%E2%80%93Shannon_divergence">http://en.wikipedia.org/wiki/Jensen%E2%80%93Shannon_divergence</a>.
 *
 * @param dist1 a categorical distribution
 * @param dist2 a categorical distribution
 * @param n size of support of \p dist1 and \p dist2
 * @return JS divergence
 */
template <typename RealArray>
inline double js_divergence(const RealArray& dist1, const RealArray& dist2, const size_type n)
{
  double js = 0.0;
  for (index_type i = 0; i < n; ++i)
  {
    const auto p = dist1[i];
    const auto q = dist2[i];
    const auto m = 0.5 * (p + q);
    js += p * (fastlog(p) - fastlog(m));
    js += q * (fastlog(q) - fastlog(m));
  }

  return 0.5 * js;
}

/**
 * @brief The Jensen-Shannon \a distance is just the square root of the JS divergence (\p ycutils::js_divergence).
 * @param dist1 a categorical distribution
 * @param dist2 a categorical distribution
 * @return JS divergence
 */
template <typename RealVector>
inline double js_distance(const RealVector& dist1, const RealVector& dist2) { return std::sqrt(js_divergence(dist1, dist2)); }

/**
 * @brief The Jensen-Shannon \a distance is just the square root of the JS divergence (\p ycutils::js_divergence).
 * @param dist1 a categorical distribution
 * @param dist2 a categorical distribution
 * @param n size of support of \p dist1 and \p dist2
 * @return JS divergence
 */
template <typename RealArray>
inline double js_distance(const RealArray& dist1, const RealArray& dist2, const size_type n) { return std::sqrt(js_divergence(dist1, dist2, n)); }

}  // namespace ycutils

#endif  // YCUTILS_RANDOM_H_INCLUDED
