/**
 * @file
 * @brief Contain static definitions of constants of classes in @ref vocabulary.h.
 */
#include <string>
#include <vector>

#include <ycutils/vocabulary.h>

namespace ycutils {

/// Default sorting order of vocabulary when saving it.
const std::vector<std::string> vocabulary::sort_order({"freq", "lex", "none"});

}  // namespace ycutils
